Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS

	drugs_parse:		Processing routines for various drugs data sources (Spain, Australia, Switzerland).
	epc_shim:		Install utility for CentOS management interface (bootstrap tools and conditioning code).
	support_software:	Camera application for support staff (camera application and setup program).



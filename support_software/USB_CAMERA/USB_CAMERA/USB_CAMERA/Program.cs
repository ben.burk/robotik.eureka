﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace USB_CAMERA
{
	static class Program
    	{
    		private static List<CameraData> read_settings;
    		private static bool json_error;
    	
    		static void StartWSettings(string FormTitle, bool lowres)
        	{
        		Application.EnableVisualStyles();
         		Application.SetCompatibleTextRenderingDefault(false);
           		Application.Run(new MainForm(FormTitle, lowres));
        	}
        	static void StartWNoSettings(bool lowres)
        	{
        		Application.EnableVisualStyles();
            		Application.SetCompatibleTextRenderingDefault(false);
         	   	Application.Run(new MainForm("", lowres));
        	}
        	static void JSONSerializeError(object sender, Newtonsoft.Json.Serialization.ErrorEventArgs args)
        	{
        		if (!json_error)
           			json_error = true;
        	
           		args.ErrorContext.Handled = true;
           		MessageBox.Show(args.ErrorContext.Error.Message, "Settings JSON Parse ERROR!");

        	}	
        
        	/// <summary>
        	/// The main entry point for the application.
        	/// </summary>
        	[STAThread]
        	static void Main()
        	{
           		json_error = false;
           		if (System.Diagnostics.Process.GetProcessesByName(System.Diagnostics.Process.GetCurrentProcess().ProcessName).Length > 1)
		   	{
              			MessageBox.Show("Please close all cameras!", "ERROR");
  			   	return;
		   	}
        	
           		if (File.Exists(AppDomain.CurrentDomain.BaseDirectory+"settings.json"))
           		{
           			using (StreamReader file = new StreamReader(AppDomain.CurrentDomain.BaseDirectory+"settings.json"))
           			{
           				JsonSerializer serializer = new JsonSerializer();
           				serializer.Error += JSONSerializeError;
           				
           				try
           				{
           					read_settings = (List<CameraData>)serializer.Deserialize(file, typeof(List<CameraData>));
           					if (json_error)
           					{
           						Thread t = new Thread(() => StartWNoSettings(false));
	           					t.Start();
	           					while (t.IsAlive)
           							t.Join(500);
           						return;
           					}
           				}
           				catch (NullReferenceException nre )
           				{
           					Application.Exit();
           				}

           			}
           			for (int i=0;i<read_settings.Count;i++)
           			{
           				CameraData tmp = read_settings[i];
           				Thread t = new Thread(() => 
           				{
           					StartWSettings(" :: CAMERA #" + tmp.Sequence.ToString(), tmp.UseLowResolution);
           				});
           				t.Start();
           			}
           		}
           		else
           		{
           			Thread t = new Thread(() => StartWNoSettings(false));
           			t.Start();
           		}
        	}
    	}
}

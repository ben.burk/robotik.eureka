﻿using System;

namespace USB_CAMERA
{
	public class CameraData
	{
		public string UUID { get; set; }
		public string URI { get; set; }
		public string Moniker { get; set; }
		public string Name { get; set; }
		public int Sequence { get; set; }
		public double DefaultRot { get; set; }
		public bool UseLowResolution { get; set; }
	}
}

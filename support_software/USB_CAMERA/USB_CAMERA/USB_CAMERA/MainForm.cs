﻿using System;
using System.Windows.Forms;
using Ozeki.Camera;
using Ozeki.Media;

namespace USB_CAMERA
{
	public partial class MainForm : Form
    	{
    		private ICamera _webCamera;
       		private DrawingImageProvider _imageProvider;
        	private MediaConnector _mediaConnector;
        	private CameraURLBuilderWF _myCameraUrlBuilder;
        	private Zoom _cameraZoom;
        	private ImageManipulation _imageManipulation;
       		private OzRotate _ozFilter;
        	private MPEG4Recorder _recorder;
        	private bool _lowResolution;
        	private string _callingData;

        	public MainForm(string FormTitle, bool low_res)
        	{
      			_callingData = FormTitle;
            		_lowResolution = low_res;
           		InitializeComponent();
            		_imageProvider = new DrawingImageProvider();
            		_mediaConnector = new MediaConnector(); 
            		videoViewerWF1.SetImageProvider(_imageProvider);
            		_cameraZoom = new Zoom();
            		_imageManipulation = new ImageManipulation();
            		_ozFilter = new OzRotate();
        	}

        	private void btn_Connect_Click(object sender, EventArgs e)
        	{
        		if (_webCamera != null)
        		{
            			_mediaConnector.Disconnect(_webCamera.VideoChannel, _imageManipulation);
            			_mediaConnector.Disconnect(_imageManipulation, _cameraZoom);
            			_mediaConnector.Disconnect(_cameraZoom, _imageProvider);
            			_webCamera = null;
        		}
            
        		InvokeGuiThread(() =>
        		{
            			btn_Connect.Enabled = false;
            			btn_Compose.Enabled = false;
            			btn_Disconnect.Enabled = true;
            			btn_ZoomIn.Enabled = true;
            			btn_ZoomOut.Enabled = true;
            			btn_PanLeft.Enabled = true;
            			btn_PanRight.Enabled = true;
            			btn_PanUp.Enabled = true;
            			btn_PanDown.Enabled = true;
           	 		btn_RotateImageClockW.Enabled = true;
            			btn_RotateImageCClockW.Enabled = true;
            			btn_CameraHome.Enabled = true;
            			btn_Record.Enabled = true;
           		});
        	
           		_webCamera = new OzekiCamera(_myCameraUrlBuilder.CameraURL);
            		_webCamera.CameraStateChanged += _webCamera_CameraStateChanged;
            		_mediaConnector.Connect(_webCamera.VideoChannel, _imageManipulation);
            		_mediaConnector.Connect(_imageManipulation, _cameraZoom);
            		_mediaConnector.Connect(_cameraZoom, _imageProvider);
            		_webCamera.Start();
            
            		InvokeGuiThread(() =>
            		{
            			videoViewerWF1.Start();
            		});
            
            		_imageManipulation.Start();
            		_imageManipulation.Add(_ozFilter);
            		_cameraZoom.Start();
        	}

        	void _webCamera_CameraStateChanged(object sender, CameraStateEventArgs e)
        	{
            		InvokeGuiThread(() =>
            		{
        	    		if (terminate_thread == false)
        	    		{
                			switch (e.State)
                			{
                    				case CameraState.Streaming:
                					//nothing to do here
                        				break;
                    				case CameraState.Disconnected:
                        				btn_Compose.Enabled = true;
                        				btn_Disconnect.Enabled = false;
                        				btn_ZoomIn.Enabled = false;
                        				btn_ZoomOut.Enabled = false;
                        				btn_Connect.Enabled = true;
                        				btn_PanLeft.Enabled = false;
                        				btn_PanRight.Enabled = false;
                        				btn_PanUp.Enabled = false;
                        				btn_PanDown.Enabled = false;
                        				btn_RotateImageClockW.Enabled = false;
                        				btn_RotateImageCClockW.Enabled = false;
                        				btn_CameraHome.Enabled = false;
                        				btn_Record.Enabled = false;
                        				break;
                			}
        	    		}
        	    		else
        	    		{
        	    			System.Windows.Forms.Application.ExitThread();
        	    		}
            		});
        	}

        	private void InvokeGuiThread(Action action)
        	{
        		BeginInvoke(action);
        	}
        
        	private void btn_Disconnect_Click(object sender, EventArgs e)
        	{
        		if (_webCamera.VideoChannel == null) return;
        		if (is_recording == true)
        		{
        			_mediaConnector.Disconnect(_cameraZoom, _recorder.VideoRecorder);
            			_recorder.Multiplex();
            			InvokeGuiThread(() =>
            			{
            				is_recording = false;
        				btn_Record.Click -= StopCapture_Click;
            				btn_Record.Click += btn_Record_Click;
            				btn_Record.Text = "Record";
            				label2.Text = "";
            				label2.ForeColor = System.Drawing.Color.Black;
            				btn_Record.Font = new System.Drawing.Font(btn_Record.Font.Name, btn_Record.Font.Size, System.Drawing.FontStyle.Regular);
            				btn_Record.BackColor = System.Drawing.Color.LightGray;
            				videoViewerWF1.Stop();			//videoviewer is part of form
           				videoViewerWF1.ClearScreen();	//videoviewer is part of form
            			});
        		}
        	 
            		_cameraZoom.Default();
            		_imageManipulation.Remove(_ozFilter);
            		_webCamera.Stop();
            		_mediaConnector.Disconnect(_webCamera.VideoChannel, _imageManipulation);
            		_mediaConnector.Disconnect(_imageManipulation, _cameraZoom);
            		_mediaConnector.Disconnect(_cameraZoom, _imageProvider);
            		_webCamera = null;
        	}

        	private void btn_Compose_Click(object sender, EventArgs e)
        	{
        		var data = new CameraURLBuilderData { DeviceTypeFilter = DiscoverDeviceType.USB };
            		_myCameraUrlBuilder = new CameraURLBuilderWF(data);
            		var result = _myCameraUrlBuilder.ShowDialog();

            		if(result != DialogResult.OK)
                		return;

            		InvokeGuiThread(() =>
            		{
           			textBox1.Text = _myCameraUrlBuilder.CameraURL;
            			btn_Connect.Enabled = true;
           		});
        	}
        	private void btn_Filter_Click(object sender, EventArgs e)
        	{
        		var button = sender as Button;
        		if (button != null)
        			Rotate_Camera(button.Name);
        	}
        	private void btn_Move_Click(object sender, EventArgs e)
        	{
        		var button = sender as Button;
        		if (button != null)
        			Move_Camera_Focus(button.Name);
        	}
        	private void btn_Record_Click(object sender, EventArgs e)
        	{
        		var button = sender as Button;
        		if (button != null)
        		{
        			if (_webCamera.VideoChannel == null) return;
        			string format = "yyyy-dd-MM-HH-mm";
        			var date = DateTime.Now.ToString(format);
        			if (!System.IO.Directory.Exists(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "capture")))
        				System.IO.Directory.CreateDirectory(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "capture"));
        			var outputdir = System.IO.Path.Combine(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "capture"), "capture-"+date+"_camera"+System.Text.RegularExpressions.Regex.Replace(_callingData, "[^0-9]", "")+".mp4");
        			_recorder = new MPEG4Recorder(outputdir);
        			_recorder.MultiplexFinished += recorder_Multiplex_Finished;
        		
				InvokeGuiThread(() =>
        			{
                			btn_ZoomIn.Enabled = false;
                			btn_ZoomOut.Enabled = false;
                			btn_PanLeft.Enabled = false;
                			btn_PanRight.Enabled = false;
                			btn_PanUp.Enabled = false;
                			btn_PanDown.Enabled = false;
                			btn_RotateImageClockW.Enabled = false;
                			btn_RotateImageCClockW.Enabled = false;
                			btn_CameraHome.Enabled = false;
                			is_recording = true;
                			btn_Record.Text = "Stop";
                			btn_Record.Font = new System.Drawing.Font(btn_Record.Font.Name, btn_Record.Font.Size, System.Drawing.FontStyle.Bold);
                			btn_Record.BackColor = System.Drawing.Color.Red;
                			label2.Text = "Recording...";
                			label2.ForeColor = System.Drawing.Color.Green;
        			});

                		_mediaConnector.Connect(_cameraZoom, _recorder.VideoRecorder);
                		btn_Record.Click -= btn_Record_Click;
                		btn_Record.Click += StopCapture_Click;
        		}
        	}
        	private void StopCapture_Click(object sender, EventArgs e)
        	{
        		if (_webCamera.VideoChannel == null) return;
            		_mediaConnector.Disconnect(_cameraZoom, _recorder.VideoRecorder);
            		_recorder.Multiplex();
            		btn_Record.Click -= StopCapture_Click;
            		btn_Record.Click += btn_Record_Click;
            
			InvokeGuiThread(() =>
            		{
            			is_recording = false;
            			btn_Record.Text = "Record";
            			label2.Text = "";
            			label2.ForeColor = System.Drawing.Color.Black;
            			btn_Record.Font = new System.Drawing.Font(btn_Record.Font.Name, btn_Record.Font.Size, System.Drawing.FontStyle.Regular);
            			btn_Record.BackColor = System.Drawing.Color.LightGray;
            			btn_ZoomIn.Enabled = true;
            			btn_ZoomOut.Enabled = true;
            			btn_PanLeft.Enabled = true;
            			btn_PanRight.Enabled = true;
            			btn_PanUp.Enabled = true;
            			btn_PanDown.Enabled = true;
            			btn_RotateImageClockW.Enabled = true;
            			btn_RotateImageCClockW.Enabled = true;
            			btn_CameraHome.Enabled = true;
            		});
        	}
        	private void recorder_Multiplex_Finished(object sender, VoIPEventArgs<bool> e)
        	{
        		_recorder.MultiplexFinished -= recorder_Multiplex_Finished;
        		_recorder.Dispose();
        	}
        	private void btn_Move_Down(object sender, MouseEventArgs me)
        	{
        		var button = sender as Button;
        		if (button != null)
        			Move_Camera_Focus(button.Name);
        	}
        	private void btn_Move_Up(object sender, MouseEventArgs me)
        	{
        		_cameraZoom.StopMovement();
        	}
        	private void Rotate_Camera(string direction)
        	{
        		if (_webCamera == null) return;
        		switch (direction)
        		{
        			case "btn_RotateImageClockW":
        				_ozFilter.Angle -= 90;
        				if (!_imageManipulation.Filters.Contains(_ozFilter))
        					_imageManipulation.Add(_ozFilter);
        				if (_ozFilter.Angle == (double)360 || _ozFilter.Angle == (double)-360) _ozFilter.Angle = 0;
        				break;
        			case "btn_RotateImageCClockW":
        				_ozFilter.Angle += 90;
        				if (!_imageManipulation.Filters.Contains(_ozFilter))
        					_imageManipulation.Add(_ozFilter);
        				if (_ozFilter.Angle == (double)-360 || _ozFilter.Angle == (double)360) _ozFilter.Angle = 0;
        				break;
        		}
        	}
        	private void Move_Camera_Focus(string direction)
        	{
        		if (_webCamera == null) return;
            		switch (direction)
            		{
                		case "btn_ZoomIn":
            				_cameraZoom.In();
            				break;
            			case "btn_ZoomOut":
            				_cameraZoom.Out();
            				break;
            			case "btn_PanRight":
            				_cameraZoom.ContinouesMove(ZoomDirection.Right, 5);
            				break;
            			case "btn_PanLeft":
            				_cameraZoom.ContinouesMove(ZoomDirection.Left, 5);
            				break;
            			case "btn_PanUp":
            				_cameraZoom.ContinouesMove(ZoomDirection.Up, 5);
            				break;
            			case "btn_PanDown":
            				_cameraZoom.ContinouesMove(ZoomDirection.Bottom, 5);
            				break;
            			case "btn_CameraHome":
            				_cameraZoom.Default();
            				_imageManipulation.Remove(_ozFilter);
            				break;
            		}
        	}
    	}
}

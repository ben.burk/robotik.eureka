﻿namespace USB_CAMERA
{
	partial class MainForm
    	{
        	private System.ComponentModel.IContainer components = null;
        	private System.Windows.Forms.Button btn_Compose;
        	private Ozeki.Media.VideoViewerWF videoViewerWF1;
        	private System.Windows.Forms.GroupBox groupBox1;
        	private System.Windows.Forms.GroupBox groupBox2;
        	private System.Windows.Forms.TextBox textBox1;
        	private System.Windows.Forms.Label label1;
        	private System.Windows.Forms.Label label2;
        	private System.Windows.Forms.Button btn_Disconnect;
        	private System.Windows.Forms.Button btn_Connect;
        	private System.Windows.Forms.Button btn_ZoomIn;
        	private System.Windows.Forms.Button btn_ZoomOut;
        	private System.Windows.Forms.Button btn_PanLeft;
        	private System.Windows.Forms.Button btn_PanRight;
        	private System.Windows.Forms.Button btn_PanDown;
        	private System.Windows.Forms.Button btn_PanUp;
        	private System.Windows.Forms.Button btn_RotateImageClockW;
        	private System.Windows.Forms.Button btn_RotateImageCClockW;
        	private System.Windows.Forms.Button btn_CameraHome;
        	private System.Windows.Forms.Button btn_Record;
        	private bool terminate_thread;
        	private bool is_recording;
        
        	delegate void CloseFormThread(MainForm form);
        
       		protected override void Dispose(bool disposing)
        	{
        		if (disposing)
        		{
        			for (int i=0;i<groupBox1.Controls.Count;i++)
            				groupBox1.Controls[i].Dispose();
            			for (int j=0;j<groupBox2.Controls.Count;j++)
            				groupBox2.Controls[j].Dispose();
            	
            			if (_webCamera != null)
            			{
            				_webCamera.Dispose();
            				_imageManipulation.Dispose();
            				_imageProvider.Dispose();
            				_mediaConnector.Dispose();
            				_cameraZoom.Dispose();
            			}
        		}
            		if (disposing && (components != null))
            		{
                		components.Dispose();
            		}
                
           		base.Dispose(disposing);
        	}
        	private void Form_Closing(object sender, System.Windows.Forms.FormClosingEventArgs fcea)
        	{
        		this.terminate_thread = true;
        		if (this.is_recording == true)
        		{
        			_mediaConnector.Disconnect(_cameraZoom, _recorder.VideoRecorder);
            			_recorder.Multiplex();
        		}
        		if (_webCamera != null)
        		{
        			InvokeGuiThread(() => 
        			{
        				videoViewerWF1.Stop();
            				videoViewerWF1.ClearScreen();
            			});
            			
				_cameraZoom.Default();
            			_imageManipulation.Remove(_ozFilter);
            			_webCamera.Stop();
            			_mediaConnector.Disconnect(_webCamera.VideoChannel, _imageManipulation);
            			_mediaConnector.Disconnect(_imageManipulation, _cameraZoom);
            			_mediaConnector.Disconnect(_cameraZoom, _imageProvider);
            			_webCamera = null;
        		}
        	
        		Dispose(true);
        		InvokeCloseThread(this);
        	}
        	private static void InvokeCloseThread(MainForm form)
        	{
        		if (!form.IsDisposed)
    			{
        			if (form.InvokeRequired)
        			{
            				CloseFormThread method = new CloseFormThread( InvokeCloseThread );
            				form.Invoke(method, new object[] { form }  );
        			}
        			else
        			{
            				form.Close();
                		}
        		}
        	}
        	private void InitializeComponent()
        	{
        		this.terminate_thread = false;
        		this.is_recording = false;
        		this.FormClosing += Form_Closing;
            		this.btn_Compose = new System.Windows.Forms.Button();
            		this.videoViewerWF1 = new Ozeki.Media.VideoViewerWF();
            		this.groupBox1 = new System.Windows.Forms.GroupBox();
            		this.groupBox2 = new System.Windows.Forms.GroupBox();
            		this.label1 = new System.Windows.Forms.Label();
            		this.label2 = new System.Windows.Forms.Label();
            		this.textBox1 = new System.Windows.Forms.TextBox();
            		this.btn_Connect = new System.Windows.Forms.Button();
            		this.btn_Disconnect = new System.Windows.Forms.Button();
            		this.btn_ZoomIn = new System.Windows.Forms.Button();
            		this.btn_ZoomOut = new System.Windows.Forms.Button();
           		this.btn_PanLeft = new System.Windows.Forms.Button();
            		this.btn_PanRight = new System.Windows.Forms.Button();
            		this.btn_PanDown = new System.Windows.Forms.Button();
            		this.btn_PanUp = new System.Windows.Forms.Button();
            		this.btn_RotateImageClockW = new System.Windows.Forms.Button();
            		this.btn_RotateImageCClockW = new System.Windows.Forms.Button();
           	 	this.btn_CameraHome = new System.Windows.Forms.Button();
           		this.btn_Record = new System.Windows.Forms.Button();
            		this.components = new System.ComponentModel.Container();
            		this.groupBox1.SuspendLayout();
            		this.groupBox2.SuspendLayout();
            		this.SuspendLayout();
            		// 
            		// btn_Compose
            		//
           		if (_lowResolution)
            			this.btn_Compose.Location = new System.Drawing.Point(188, 15);
            		else
            			this.btn_Compose.Location = new System.Drawing.Point(238, 15);
            		this.btn_Compose.Name = "btn_Compose";
            		this.btn_Compose.Size = new System.Drawing.Size(75, 23);
            		this.btn_Compose.TabIndex = 1;
            		this.btn_Compose.Text = "Compose";
            		this.btn_Compose.UseVisualStyleBackColor = true;
            		this.btn_Compose.Click += new System.EventHandler(this.btn_Compose_Click);
            		//
            		// btn_PanLeft
           		//
            		if (_lowResolution)
            		{
            			this.btn_PanLeft.Location = new System.Drawing.Point(133, 33);
            			this.btn_PanLeft.Size = new System.Drawing.Size(35, 23);
            			this.btn_PanLeft.Text = "←";
            		}
            		else
            		{
            			this.btn_PanLeft.Location = new System.Drawing.Point(173, 33);
            			this.btn_PanLeft.Size = new System.Drawing.Size(55, 23);
            			this.btn_PanLeft.Text = "Left";
            		}
            
            		this.btn_PanLeft.Enabled = false;
           		this.btn_PanLeft.Name = "btn_PanLeft";
            		this.btn_PanLeft.TabIndex = 1;
            		this.btn_PanLeft.UseVisualStyleBackColor = true;
            		this.btn_PanLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Down);
            		this.btn_PanLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Up);
            		//
            		// btn_PanRight
            		//
            		if (_lowResolution)
            		{
            			this.btn_PanRight.Location = new System.Drawing.Point(170, 33);
            			this.btn_PanRight.Size = new System.Drawing.Size(35, 23);
            			this.btn_PanRight.Text = "→";
            		}
            		else
           		{
            			this.btn_PanRight.Location = new System.Drawing.Point(227, 33);
            			this.btn_PanRight.Size = new System.Drawing.Size(50, 23);
            			this.btn_PanRight.Text = "Right";
            		}
            
            		this.btn_PanRight.Enabled = false;
            		this.btn_PanRight.Name = "btn_PanRight";
            		this.btn_PanRight.TabIndex = 1;
            		this.btn_PanRight.UseVisualStyleBackColor = true;
            		this.btn_PanRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Down);
            		this.btn_PanRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Up);
            		//
            		// btn_PanUp
            		//
            		if (_lowResolution)
            		{
            			this.btn_PanUp.Location = new System.Drawing.Point(150, 8);
            			this.btn_PanUp.Size = new System.Drawing.Size(35, 23);
            			this.btn_PanUp.Text = "↑";
            		}
            		else
            		{
            			this.btn_PanUp.Location = new System.Drawing.Point(200, 8);
            			this.btn_PanUp.Size = new System.Drawing.Size(50, 23);
            			this.btn_PanUp.Text = "Up";
            		}
            
            		this.btn_PanUp.Enabled = false;
            		this.btn_PanUp.Name = "btn_PanUp";
            		this.btn_PanUp.TabIndex = 1;
            		this.btn_PanUp.UseVisualStyleBackColor = true;
            		this.btn_PanUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Down);
            		this.btn_PanUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Up);
            		//
            		// btn_PanDown
            		//
           		if (_lowResolution)
            		{
            			this.btn_PanDown.Location = new System.Drawing.Point(150, 58);
            			this.btn_PanDown.Size = new System.Drawing.Size(35, 23);
            			this.btn_PanDown.Text = "↓";
            		}
            		else
            		{
            			this.btn_PanDown.Location = new System.Drawing.Point(200, 58);
            			this.btn_PanDown.Size = new System.Drawing.Size(50, 23);
            			this.btn_PanDown.Text = "Down";
            		}
            
            		this.btn_PanDown.Enabled = false;
            		this.btn_PanDown.Name = "btn_PanDown";
            		this.btn_PanDown.TabIndex = 1;
            		this.btn_PanDown.UseVisualStyleBackColor = true;
            		this.btn_PanDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Down);
           	 	this.btn_PanDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Up);
            		//
            		// btn_ZoomIn
            		//
            		this.btn_ZoomIn.Enabled = false;
            		this.btn_ZoomIn.Location = new System.Drawing.Point(10, 20);
            		this.btn_ZoomIn.Name = "btn_ZoomIn";
            		if (_lowResolution)
            		{
            			this.btn_ZoomIn.Size = new System.Drawing.Size(35, 23);
            			this.btn_ZoomIn.Text = "+";
            		}
            		else
            		{
            			this.btn_ZoomIn.Size = new System.Drawing.Size(75, 23);
            			this.btn_ZoomIn.Text = "Zoom +";
            		}
            
            		this.btn_ZoomIn.TabIndex = 1;
            		this.btn_ZoomIn.UseVisualStyleBackColor = true;
           		this.btn_ZoomIn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Down);
           	 	this.btn_ZoomIn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Up);
            		//
            		// btn_ZoomOut
            		//
            		this.btn_ZoomOut.Enabled = false;
            		this.btn_ZoomOut.Location = new System.Drawing.Point(10, 50);
            		this.btn_ZoomOut.Name = "btn_ZoomOut";
            		if (_lowResolution)
            		{
            			this.btn_ZoomOut.Size = new System.Drawing.Size(35, 23);
            			this.btn_ZoomOut.Text = "-";
            		}
            		else
            		{
            			this.btn_ZoomOut.Size = new System.Drawing.Size(75, 23);
            			this.btn_ZoomOut.Text = "Zoom -";
            		}
            		this.btn_ZoomOut.TabIndex = 1;
            		this.btn_ZoomOut.UseVisualStyleBackColor = true;
            		this.btn_ZoomOut.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Down);
            		this.btn_ZoomOut.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Move_Up);
           		//
            		// btn_RotateImageClockW
            		//
            		if (_lowResolution)
            		{
            			this.btn_RotateImageClockW.Location = new System.Drawing.Point(195, 7);
            			this.btn_RotateImageClockW.Size = new System.Drawing.Size(25, 23);
            			this.btn_RotateImageClockW.Text = "↻";
            		}
            		else
            		{
            			this.btn_RotateImageClockW.Location = new System.Drawing.Point(280, 20);
            			this.btn_RotateImageClockW.Size = new System.Drawing.Size(85, 23);
            			this.btn_RotateImageClockW.Text = "Rot ClockW";
            		}
           		this.btn_RotateImageClockW.Enabled = false;
            		this.btn_RotateImageClockW.Name = "btn_RotateImageClockW";
            		this.btn_RotateImageClockW.TabIndex = 1;
            		this.btn_RotateImageClockW.UseVisualStyleBackColor = true;
            		this.btn_RotateImageClockW.Click += new System.EventHandler(this.btn_Filter_Click);
            		//
            		// btn_RotateImageCClockW
            		//
            		if (_lowResolution)
            		{
            			this.btn_RotateImageCClockW.Location = new System.Drawing.Point(195, 57);
            			this.btn_RotateImageCClockW.Size = new System.Drawing.Size(25, 23);
            			this.btn_RotateImageCClockW.Text = "↺";
            		}
            		else
            		{
            			this.btn_RotateImageCClockW.Location = new System.Drawing.Point(280, 50);
            			this.btn_RotateImageCClockW.Size = new System.Drawing.Size(91, 23);
            			this.btn_RotateImageCClockW.Text = "Rot CClockW";
            		}
            		this.btn_RotateImageCClockW.Enabled = false;
            		this.btn_RotateImageCClockW.Name = "btn_RotateImageCClockW";
            		this.btn_RotateImageCClockW.TabIndex = 1;
            		this.btn_RotateImageCClockW.UseVisualStyleBackColor = true;
            		this.btn_RotateImageCClockW.Click += new System.EventHandler(this.btn_Filter_Click);
            		//
            		// btn_CameraHome
           		//
            		this.btn_CameraHome.Enabled = false;
            		if (_lowResolution)
            			this.btn_CameraHome.Location = new System.Drawing.Point(55, 20);
            		else
            			this.btn_CameraHome.Location = new System.Drawing.Point(95, 20);
            		this.btn_CameraHome.Name = "btn_CameraHome";
            		this.btn_CameraHome.Size = new System.Drawing.Size(50, 23);
            		this.btn_CameraHome.TabIndex = 1;
            		this.btn_CameraHome.Text = "Home";
            		this.btn_CameraHome.UseVisualStyleBackColor = true;
            		this.btn_CameraHome.Click += new System.EventHandler(this.btn_Move_Click);
            		//
            		// btn_Record
            		//
            		this.btn_Record.Enabled = false;
            		if (_lowResolution)
            			this.btn_Record.Location = new System.Drawing.Point(55, 50);
            		else
            			this.btn_Record.Location = new System.Drawing.Point(95, 50);
            		this.btn_Record.Name = "btn_Record";
            		this.btn_Record.Size = new System.Drawing.Size(75, 23);
           		this.btn_Record.TabIndex = 1;
            		this.btn_Record.Text = "Record";
            		this.btn_Record.UseVisualStyleBackColor = true;
            		this.btn_Record.Click += new System.EventHandler(this.btn_Record_Click);
            		// 
            		// videoViewerWF1
            		// 
            		this.videoViewerWF1.BackColor = System.Drawing.Color.Black;
            		this.videoViewerWF1.FlipMode = Ozeki.Media.FlipMode.None;
            		this.videoViewerWF1.FrameStretch = Ozeki.Media.FrameStretch.Uniform;
            		this.videoViewerWF1.FullScreenEnabled = true;
            		this.videoViewerWF1.Location = new System.Drawing.Point(12, 115);
           		this.videoViewerWF1.Name = "videoViewerWF1";
            		this.videoViewerWF1.RotateAngle = 0;
            		if (_lowResolution)
            			this.videoViewerWF1.Size = new System.Drawing.Size(500, 325);
            		else
            			this.videoViewerWF1.Size = new System.Drawing.Size(700, 525);
            		this.videoViewerWF1.TabIndex = 2;
            		this.videoViewerWF1.Text = "videoViewerWF1";
            		// 
            		// groupBox1
            		// 
            		this.groupBox1.Controls.Add(this.btn_Disconnect);
           		this.groupBox1.Controls.Add(this.btn_Connect);
            		this.groupBox1.Controls.Add(this.textBox1);
            		this.groupBox1.Controls.Add(this.label1);
            		this.groupBox1.Controls.Add(this.btn_Compose);
            		this.groupBox1.Location = new System.Drawing.Point(12, 12);
            		this.groupBox1.Name = "groupBox1";
            		if (_lowResolution)
            			this.groupBox1.Size = new System.Drawing.Size(270, 82);
            		else
            			this.groupBox1.Size = new System.Drawing.Size(320, 82);
            		this.groupBox1.TabIndex = 3;
            		this.groupBox1.TabStop = false;
            		this.groupBox1.Text = "Connect";
            		//
            		// groupBox2
            		//
            		this.groupBox2.Controls.Add(this.btn_ZoomIn);
            		this.groupBox2.Controls.Add(this.btn_ZoomOut);
            		this.groupBox2.Controls.Add(this.btn_PanLeft);
            		this.groupBox2.Controls.Add(this.btn_PanRight);
            		this.groupBox2.Controls.Add(this.btn_PanUp);
            		this.groupBox2.Controls.Add(this.btn_PanDown);
            		this.groupBox2.Controls.Add(this.btn_RotateImageClockW);
            		this.groupBox2.Controls.Add(this.btn_RotateImageCClockW);
            		this.groupBox2.Controls.Add(this.btn_CameraHome);
           		this.groupBox2.Controls.Add(this.btn_Record);
            		if (_lowResolution)
            			this.groupBox2.Location = new System.Drawing.Point(288, 12);
            		else
            			this.groupBox2.Location = new System.Drawing.Point(338, 12);
            		this.groupBox2.Name = "groupBox2";
            		if (_lowResolution)
            			this.groupBox2.Size = new System.Drawing.Size(224, 82);
            		else
            			this.groupBox2.Size = new System.Drawing.Size(374, 82);
           		this.groupBox2.TabIndex = 3;
            		this.groupBox2.TabStop = false;
            		this.groupBox2.Text = "Control";
            		// 
            		// label1
           	 	// 
            		this.label1.AutoSize = true;
            		this.label1.Location = new System.Drawing.Point(5, 20);
            		this.label1.Name = "label1";
            		this.label1.Size = new System.Drawing.Size(71, 13);
            		this.label1.TabIndex = 2;
            		this.label1.Text = "Camera URL:";
            		//
            		// label2
            		//
            		this.label2.AutoSize = true;
            		this.label2.Location = new System.Drawing.Point(515, 650);
            		this.label2.Name = "label2";
            		this.label2.Size = new System.Drawing.Size(71, 13);
            		this.label2.TabIndex = 1;
            		this.label2.Font = new System.Drawing.Font(this.label2.Font.Name, (float)12, System.Drawing.FontStyle.Bold);
            		this.label2.Text = "";
            		// 
            		// textBox1
            		// 
            		this.textBox1.Location = new System.Drawing.Point(73, 17);
            		this.textBox1.Name = "textBox1";
            		if (_lowResolution)
            			this.textBox1.Size = new System.Drawing.Size(109, 20);
            		else
            			this.textBox1.Size = new System.Drawing.Size(159, 20);
            		this.textBox1.TabIndex = 3;
            		// 
            		// btn_Connect
            		// 
			this.btn_Connect.Enabled = false;
			if (_lowResolution)
				this.btn_Connect.Location = new System.Drawing.Point(23, 43);
			else
            			this.btn_Connect.Location = new System.Drawing.Point(73, 43);
            		this.btn_Connect.Name = "btn_Connect";
            		this.btn_Connect.Size = new System.Drawing.Size(75, 23);
            		this.btn_Connect.TabIndex = 4;
            		this.btn_Connect.Text = "Connect";
            		this.btn_Connect.UseVisualStyleBackColor = true;
            		this.btn_Connect.Click += new System.EventHandler(this.btn_Connect_Click);
            		// 
            		// btn_Disconnect
            		//
			this.btn_Disconnect.Enabled = false;
			if (_lowResolution)
				this.btn_Disconnect.Location = new System.Drawing.Point(107, 43);
			else
            			this.btn_Disconnect.Location = new System.Drawing.Point(157, 43);
            		this.btn_Disconnect.Name = "btn_Disconnect";
            		this.btn_Disconnect.Size = new System.Drawing.Size(85, 23);
            		this.btn_Disconnect.TabIndex = 5;
            		this.btn_Disconnect.Text = "Disconnect";
            		this.btn_Disconnect.UseVisualStyleBackColor = true;
            		this.btn_Disconnect.Click += new System.EventHandler(this.btn_Disconnect_Click);
            		// 
            		// MainForm
            		//
            		this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(System.Reflection.Assembly.GetExecutingAssembly().Location);
            		this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            		this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            		if (_lowResolution)
            			this.ClientSize = new System.Drawing.Size(524, 480);
           	 	else
            			this.ClientSize = new System.Drawing.Size(724, 680);
            		this.Controls.Add(this.groupBox1);
            		this.Controls.Add(this.groupBox2);
            		this.Controls.Add(this.videoViewerWF1);
           	 	this.Controls.Add(this.label2);
            		components.Add(this.groupBox1);
            		components.Add(this.groupBox2);
            		components.Add(this.videoViewerWF1);
           	 	components.Add(this.label2);
            		this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            		this.MaximizeBox = false;
            		this.Name = "MainForm";
            		this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            		if (_callingData == "")
            			this.Text = "USB Camera's Live Image";
            		else
            			this.Text = "USB Camera's Live Image" + _callingData;
            		this.groupBox1.ResumeLayout(false);
            		this.groupBox1.PerformLayout();
           		this.groupBox2.ResumeLayout(false);
            		this.groupBox2.PerformLayout();
            		this.ResumeLayout(false);
       		}
    	}
}

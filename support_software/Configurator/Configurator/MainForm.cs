﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Newtonsoft.Json;
using Ozeki.Camera;
using Ozeki.Common;
using Ozeki.Media;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;

namespace Configurator
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			this.ImageProvider = new DrawingImageProvider();
			this.MediaConnector = new MediaConnector();
			
			this.VideoViewer1.SetImageProvider(this.ImageProvider);
			this.ImageManipulation = new ImageManipulation();
			this.OzFilter = new OzRotate();
		}
		private void InvokeGuiThread(Action action)
        	{
            		BeginInvoke(action);
        	}
		private void btn_Camera_Click(object sender, System.EventArgs e)
		{
			if (this.has_discovered == false)
			{
				this.device_Info.Discover();
				this.has_discovered = true;
			}
			
			InvokeGuiThread(() =>
			{
				this.btn_Camera.ContextMenuStrip = this.DDCameraCount;
				this.btn_Camera.ContextMenuStrip.Show(btn_Camera, new System.Drawing.Point(0, this.btn_Camera.Height));             	
			});
		}
		private void btn_Write_Click(object sender, System.EventArgs e)
		{
			//TODO; Make this subroutine operate in own thread.
			if (this.cam_Settings.Count == 0)
				return;
			
			using (System.IO.StreamWriter file = System.IO.File.CreateText(AppDomain.CurrentDomain.BaseDirectory+"settings.json"))
			{
					JsonSerializer serializer = new JsonSerializer();
					serializer.Serialize(file, this.cam_Settings);
			}
			
			InvokeGuiThread(() =>
			{
				this.label2.Text = "config file saved...";
				this.label2.ForeColor = System.Drawing.Color.Green;
			});
		}
		private void camera_Discovered(object sender, DiscoveredDeviceArgs dda)
		{
			InvokeGuiThread(() => 
			{
				if (dda.Camera.ToString().Contains("[USB]"))
					try
					{
						this.DDCameraName.Items.Add(dda.Camera.Host + " :: " + ((WebCamera)dda.Camera).ID);
						this.cam_Found.Add((WebCamera)dda.Camera);
					}
					catch (System.InvalidCastException ice)
					{
						return;
					}
			});
		}
		private void camera_Added(object sender, ToolStripItemEventArgs tea)
		{
			tea.Item.Click += delegate(object sender_i, EventArgs e_i)
			{ this.field_Camera_Name_Click(tea.Item, tea); };
		}
		private void btn_Config_Click(object sender, System.EventArgs e)
		{   
			if (this.DDCameraName.Items.Count == 0)
			{
				if (this.cam_Settings.Count > 0)
				{
					MessageBox.Show("No remaining camera(s) to configure!", "ERROR!");
					return;
				}
				else
				{
					MessageBox.Show("No camera(s) found! Please press RESET to reconfigure!", "ERROR!");
					return;
				}
			}
			
			this.btn_Camera.Enabled = false;
            		InvokeGuiThread(() =>
			{
                		((System.Windows.Forms.Button)sender).ContextMenuStrip = this.DDCameraName;
                		((System.Windows.Forms.Button)sender).ContextMenuStrip.Show((System.Windows.Forms.Button)sender, new System.Drawing.Point(0, ((System.Windows.Forms.Button)sender).Height));            	
			});             
		}
		private void btn_Reset_Click(object sender, System.EventArgs e)
		{
			InvokeGuiThread(() =>
			{
			    this.btn_Camera.Enabled = true;
				this.label1.Text = "No choice";
				this.label2.ForeColor = System.Drawing.Color.Gray;
				this.label2.Text = "";
				foreach (System.Windows.Forms.Button b in this.btns_Choice)
				{
					this.Controls.Remove(b);
					b.Dispose();
				}
				
				if (this.ActiveWebCamera != null)
				{
					this.VideoViewer1.Stop();
					this.VideoViewer1.ClearScreen();
					this.ImageManipulation.Remove(this.OzFilter);
					this.OzFilter.Angle = 0.0;
					this.ActiveWebCamera.Stop();
					this.MediaConnector.Disconnect(this.ActiveWebCamera.VideoChannel, this.ImageManipulation);
					this.MediaConnector.Disconnect(this.ImageManipulation, this.ImageProvider);
					this.ActiveWebCamera = null;
					this.btn_RotC.Enabled = false;
					this.btn_RotCC.Enabled = false;
					this.btn_SetRot.Enabled = false;
					this.label3.Text = "0";
					this.txtb1.Text = string.Empty;
					this.txtb1.Enabled = false;
					this.chkbx1.Checked = false;
					this.chkbx1.Enabled = false;
				}
				
				this.has_discovered = false;
				this.DDCameraName.Items.Clear();
				this.DDCameraName.ResetText();
				if (this.cam_Settings.Count > 0)
					this.cam_Settings.Clear();
				this.btns_Choice.Clear();
			});
		}
		private void btn_Rot_Click(object sender, System.EventArgs e)
		{
			if (this.ActiveWebCamera == null) return;
			var button = (Button)sender;
			switch (button.Name)
			{
				case "btn_RotC":
					this.OzFilter.Angle -= 90; this.label3.Text = Convert.ToString(this.OzFilter.Angle);
        				if (!this.ImageManipulation.Filters.Contains(this.OzFilter))
        					this.ImageManipulation.Add(this.OzFilter);
        				if (this.OzFilter.Angle == (double)360 || this.OzFilter.Angle == (double)-360) { this.OzFilter.Angle = 0; this.label3.Text = Convert.ToString(this.OzFilter.Angle); }
        				break;
        			case "btn_RotCC":
        				this.OzFilter.Angle += 90; this.label3.Text = Convert.ToString(this.OzFilter.Angle);
        				if (!this.ImageManipulation.Filters.Contains(this.OzFilter))
        					this.ImageManipulation.Add(this.OzFilter);
        				if (this.OzFilter.Angle == (double)-360 || this.OzFilter.Angle == (double)360) { this.OzFilter.Angle = 0; this.label3.Text = Convert.ToString(this.OzFilter.Angle); }
        				break;
			}
		}
		private void btn_SetCam_Click(object sender, System.EventArgs e)
		{
			if (this.ActiveWebCamera == null) return;
			string moniker;
			if (this.txtb1.Text == "" || this.txtb1.Text == txtbx1_start || this.txtb1.Text == string.Empty)
				moniker = "";
			else
				moniker = this.txtb1.Text;
			
			this.cam_Settings.Add(new CameraData() { Moniker = moniker, URI = this.ActiveWebCamera.Moniker, UUID = this.ActiveWebCamera.ID, Name = this.ActiveWebCamera.DeviceName, Sequence = this.cam_Settings.Count + 1, DefaultRot = this.OzFilter.Angle, UseLowResolution = this.chkbx1.Checked } );
			
			InvokeGuiThread(() =>
			{
					this.VideoViewer1.Stop();
					this.VideoViewer1.ClearScreen();
					this.ImageManipulation.Remove(this.OzFilter);
					this.OzFilter.Angle = 0.0;
					this.ActiveWebCamera.Stop();
					this.MediaConnector.Disconnect(this.ActiveWebCamera.VideoChannel, this.ImageManipulation);
					this.MediaConnector.Disconnect(this.ImageManipulation, this.ImageProvider);
					this.ActiveWebCamera = null;
					this.btn_RotC.Enabled = false;
					this.btn_RotCC.Enabled = false;
					this.btn_SetRot.Enabled = false;
					this.label3.Text = "0";
					this.txtb1.Text = string.Empty;
					this.txtb1.Enabled = false;
					this.chkbx1.Checked = false;
					this.chkbx1.Enabled = false;
			});
		}
		private void txtBox_Enter(object sender, System.EventArgs e)
		{
			if (this.txtb1.Text == txtbx1_start && this.txtb1.Enabled)
				this.txtb1.Text = "";
		}
		private void txtBox_Leave(object sender, System.EventArgs e)
		{
			if (this.txtb1.Text == "" && this.txtb1.Enabled)
				this.txtb1.Text = txtbx1_start;
		}
		private void field_Camera_Name_Click(object sender, System.EventArgs e)
		{
			string temp = ((ToolStripMenuItem)sender).Text.Split(new string[] { " :: " }, StringSplitOptions.None)[1];
			var device = this.cam_Found.Find(x => x.ID.Equals(temp));
			this.DDCameraName.Items.Remove((ToolStripMenuItem)sender);
			this.DDCameraName.SourceControl.Enabled = false;
			
			try
			{
				this.MediaConnector.Connect(device.VideoChannel, this.ImageManipulation);
				this.MediaConnector.Connect(this.ImageManipulation, this.ImageProvider);
				device.Start();
				this.VideoViewer1.Start();
				this.ImageManipulation.Start();
				this.ImageManipulation.Add(this.OzFilter);
				this.ActiveWebCamera = device;
			}
			catch (Ozeki.Common.MediaException me)
			{
				if (device.Initialized)
				{
					this.VideoViewer1.Stop();
					this.VideoViewer1.ClearScreen();
					this.ImageManipulation.Remove(this.OzFilter);
					device.Stop();
				}
				
				//exception thrown from incompatible camera. disconnect subclasses and show window
				if (this.MediaConnector.IsConnected(device.VideoChannel, this.ImageManipulation))
					this.MediaConnector.Disconnect(device.VideoChannel, this.ImageManipulation);
				
				if (this.MediaConnector.IsConnected(this.ImageManipulation, this.ImageProvider))
					this.MediaConnector.Disconnect(this.ImageManipulation, this.ImageProvider);	
				
				this.ActiveWebCamera = null;
				MessageBox.Show(me.Message + " Please press RESET to reconfigure!", "ERROR!");
				return;
				
			}
			
			InvokeGuiThread(() =>
			{
				this.btn_RotC.Enabled = true;
				this.btn_RotCC.Enabled = true;
				this.btn_SetRot.Enabled = true;
				this.txtb1.Text = txtbx1_start;
				this.txtb1.Enabled = true;
				this.chkbx1.Checked = false;
				this.chkbx1.Enabled = true;
			});
		}
		private void field_Camera_Count_Click(object sender, System.EventArgs e)
		{
			InvokeGuiThread(() =>
			{
			    this.label1.Text = (string)sender + " camera(s) chosen";
				
				//remove all buttons if applicable
				if (this.btns_Choice.Count > 0)
				{
					foreach (System.Windows.Forms.Button b in btns_Choice)
					{
						this.Controls.Remove(b);
						b.Dispose();
					}
					this.btns_Choice.Clear();
				}
				
				int choice = Convert.ToInt32((string)sender);
				int start_y = 50;
				for (int i = 1; i <= choice; i++)
				{
					System.Windows.Forms.Button b = new System.Windows.Forms.Button();
					b.Enabled = true;
					b.Location = new System.Drawing.Point(20, start_y+(i*25));
					b.Size = new System.Drawing.Size(90, 25);
					b.Name = "btn_Choice"+i.ToString();
					b.Text = "Camera "+i.ToString();
					b.Click += delegate(object btn_sender, EventArgs btn_e)
					{ this.btn_Config_Click(b, e); };
					this.btns_Choice.Add(b);
					this.Controls.Add(b);
				}
			});
		}
	}
}

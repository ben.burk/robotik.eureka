#region Using directives
using System;
using System.Reflection;
using System.Runtime.InteropServices;

#endregion
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle ("Configurator")]
[assembly: AssemblyDescription ("Configuration utility for USB_CAMERA")]
[assembly: AssemblyConfiguration ("")]
[assembly: AssemblyCompany ("BURKTECH")]
[assembly: AssemblyProduct ("Configurator")]
[assembly: AssemblyCopyright ("Copyright 2019 BURKTECH")]
[assembly: AssemblyTrademark ("")]
[assembly: AssemblyCulture ("")]
// This sets the default COM visibility of types in the assembly to invisible.
// If you need to expose a type to COM, use [ComVisible(true)] on that type.
[assembly: ComVisible (false)]
// The assembly version has following format :
//
// Major.Minor.Build.Revision
//
// You can specify all the values or you can use the default the Revision and 
// Build Numbers by using the '*' as shown below:
[assembly: AssemblyVersion ("1.1.0.3")]
[assembly: AssemblyFileVersion ("1.1.0.3")]
[assembly: Guid ("7c88f020-2894-4aa6-b668-4da0dd519773")]

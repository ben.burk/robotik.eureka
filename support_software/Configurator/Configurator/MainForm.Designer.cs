﻿using System;
using System.Collections.Generic;
using Ozeki.Camera;
using Ozeki.Media;
	
namespace Configurator
{
	partial class MainForm
	{
		private System.Windows.Forms.ContextMenuStrip DDCameraCount;
		private System.Windows.Forms.ContextMenuStrip DDCameraName;
		private List<Ozeki.Camera.WebCamera> cam_Found;
		private List<System.Windows.Forms.Button> btns_Choice;
		private System.Windows.Forms.Button btn_Camera;
		private System.Windows.Forms.Button btn_Write;
		private System.Windows.Forms.Button btn_Reset;
		private System.Windows.Forms.Button btn_RotC;
		private System.Windows.Forms.Button btn_RotCC;
		private System.Windows.Forms.Button btn_SetRot;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox txtb1;
		private System.Windows.Forms.CheckBox chkbx1;
		private System.ComponentModel.IContainer components = null;
		private List<Configurator.CameraData> cam_Settings;
		private Ozeki.Media.DiscoverCameras device_Info;
		private WebCamera ActiveWebCamera;
		private VideoViewerWF VideoViewer1;
		private MediaConnector MediaConnector;
		private DrawingImageProvider ImageProvider;
		private ImageManipulation ImageManipulation;
		private OzRotate OzFilter;
		private bool has_discovered;
		private const string txtbx1_start = "SET CAMERA NICKNAME";
		
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
            		{
                		components.Dispose();
            		}
            		base.Dispose(disposing);
		}
        	private void Form_Closing(object sender, System.Windows.Forms.FormClosingEventArgs fcea)
        	{
        		if (this.ActiveWebCamera != null)
        		{
				this.VideoViewer1.Stop();
				this.VideoViewer1.ClearScreen();
				this.ImageManipulation.Remove(this.OzFilter);
				this.ActiveWebCamera.Stop();
				this.MediaConnector.Disconnect(this.ActiveWebCamera.VideoChannel, this.ImageManipulation);
				this.MediaConnector.Disconnect(this.ImageManipulation, this.ImageProvider);
				this.ActiveWebCamera = null;
        		}
        		System.Windows.Forms.Application.Exit();
        	}
		private void InitializeComponent()
		{
			this.has_discovered = false;
			this.FormClosing += Form_Closing;
			this.DDCameraCount = new System.Windows.Forms.ContextMenuStrip();
			this.DDCameraName = new System.Windows.Forms.ContextMenuStrip();
			this.cam_Found = new List<WebCamera>();
			this.btns_Choice = new List<System.Windows.Forms.Button>();
			this.btn_Camera = new System.Windows.Forms.Button();
			this.btn_Write = new System.Windows.Forms.Button();
			this.btn_Reset = new System.Windows.Forms.Button();
			this.btn_RotC = new System.Windows.Forms.Button();
			this.btn_RotCC = new System.Windows.Forms.Button();
			this.btn_SetRot = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.txtb1 = new System.Windows.Forms.TextBox();
			this.chkbx1 = new System.Windows.Forms.CheckBox();
			this.device_Info = new Ozeki.Media.DiscoverCameras();
			this.device_Info.Discovered += camera_Discovered;
			this.cam_Settings = new List<Configurator.CameraData>();
			
			this.VideoViewer1 = new VideoViewerWF();
			this.VideoViewer1.BackColor = System.Drawing.Color.Black;
			this.VideoViewer1.FlipMode = Ozeki.Media.FlipMode.None;
            		this.VideoViewer1.FrameStretch = Ozeki.Media.FrameStretch.Uniform;
           		this.VideoViewer1.FullScreenEnabled = false;
            		this.VideoViewer1.Location = new System.Drawing.Point(305, 0);
            		this.VideoViewer1.Name = "videoViewerWF1";
            		this.VideoViewer1.RotateAngle = 0;
           		this.VideoViewer1.Size = new System.Drawing.Size(300, 265);
            		this.VideoViewer1.TabIndex = 2;
            		this.VideoViewer1.Text = "videoViewerWF1";
			
			this.DDCameraCount.Enabled = true;
			this.DDCameraCount.Name = "DDCameraCount";
			for (int i = 1; i <= 4; i++)
				this.DDCameraCount.Items.Add(i.ToString());
			
			foreach (System.Windows.Forms.ToolStripMenuItem mItem in DDCameraCount.Items)
				mItem.Click += delegate(object sender, EventArgs e) { this.field_Camera_Count_Click(mItem.Text, e); };
			
			this.DDCameraName.ItemAdded += camera_Added;
					
			this.btn_Camera.Enabled = true;
			this.btn_Camera.Location = new System.Drawing.Point(20, 20);
			this.btn_Camera.Size = new System.Drawing.Size(90, 25);
			this.btn_Camera.Name = "btn_Camera";
			this.btn_Camera.Text = "Camera count";
			this.btn_Camera.Click += btn_Camera_Click;
			
			this.btn_Write.Enabled = true;
			this.btn_Write.Location = new System.Drawing.Point(185, 225);
			this.btn_Write.Size = new System.Drawing.Size(90, 25);
			this.btn_Write.Name = "btn_Write";
			this.btn_Write.Text = "Save";
			this.btn_Write.Click += btn_Write_Click;
			
			this.btn_Reset.Enabled = true;
			this.btn_Reset.Location = new System.Drawing.Point(185, 195);
			this.btn_Reset.Size = new System.Drawing.Size(90, 25);
			this.btn_Reset.Name = "btn_Reset";
			this.btn_Reset.Text = "Reset";
			this.btn_Reset.Click += btn_Reset_Click;
			
			this.btn_RotC.Enabled = false;
			this.btn_RotC.Location = new System.Drawing.Point(185, 95);
			this.btn_RotC.Size = new System.Drawing.Size(90, 25);
			this.btn_RotC.Name = "btn_RotC";
			this.btn_RotC.Text = "Rot ClockW";
			this.btn_RotC.Click += btn_Rot_Click;
			
			this.btn_RotCC.Enabled = false;
			this.btn_RotCC.Location = new System.Drawing.Point(185, 120);
			this.btn_RotCC.Size = new System.Drawing.Size(90, 25);
			this.btn_RotCC.Name = "btn_RotCC";
			this.btn_RotCC.Text = "Rot CClockW";
			this.btn_RotCC.Click += btn_Rot_Click;
			
			this.btn_SetRot.Enabled = false;
			this.btn_SetRot.Location = new System.Drawing.Point(150, 155);
			this.btn_SetRot.Size = new System.Drawing.Size(90, 25);
			this.btn_SetRot.Name = "btn_SetCam";
			this.btn_SetRot.Text = "Set";
			this.btn_SetRot.Click += btn_SetCam_Click;
			
			this.label1.Enabled = false;
			this.label1.Location = new System.Drawing.Point(150, 27);
			this.label1.Size = new System.Drawing.Size(150, 25);
			this.label1.Name = "label1";
			this.label1.Text = "No choice";
			
			this.label2.Location = new System.Drawing.Point(23, 240);
			this.label2.Size = new System.Drawing.Size(150, 25);
			this.label2.Name = "label2";
			this.label2.Text = "";
			
			this.label3.Enabled = false;
			this.label3.Location = new System.Drawing.Point(245, 160);
			this.label3.Size = new System.Drawing.Size(40, 25);
			this.label3.Name = "label3";
			this.label3.Text = "0";
			
			this.txtb1.Enabled = false;
			this.txtb1.Location = new System.Drawing.Point(145, 65);
			this.txtb1.Size = new System.Drawing.Size(150, 25);
			this.txtb1.Name = "txtb1";
			this.txtb1.Text = "";
			this.txtb1.Enter += txtBox_Enter;
			this.txtb1.Leave += txtBox_Leave;
			
			this.chkbx1.Enabled = false;
			this.chkbx1.Text = "Low Res";
			this.chkbx1.Name = "chkbx1";
			this.chkbx1.Location = new System.Drawing.Point(20, 180);

			this.Icon = System.Drawing.Icon.ExtractAssociatedIcon(System.Reflection.Assembly.GetExecutingAssembly().Location);
			this.Text = "Configurator";
			this.Name = "MainForm";
			this.Size = new System.Drawing.Size(620, 300);
			this.MaximumSize = new System.Drawing.Size(620, 300);
			this.MinimumSize = new System.Drawing.Size(620, 300);
			this.Controls.Add(this.VideoViewer1);
			this.Controls.Add(this.btn_Camera);
			this.Controls.Add(this.btn_Write);
			this.Controls.Add(this.btn_Reset);
			this.Controls.Add(this.btn_RotC);
			this.Controls.Add(this.btn_RotCC);
			this.Controls.Add(this.btn_SetRot);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.txtb1);
			this.Controls.Add(this.chkbx1);
		}
	}
}

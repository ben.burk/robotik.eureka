Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019


	USB_CAMERA:		Camera application for support team. Allows for flexible installation inside robot, as well as simple image manipulation and recording to disk	
	Configurator:		Setup application for support camera. Writes a settings file for USB_CAMERA



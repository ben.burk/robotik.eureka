Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

mims_drug_parse

NOTE: SECTIONS HAVE BEEN REDACTED TO PROTECT TRADEMARKS
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Creates a .sql routine compliant with Eureka DB to load drugs for TGA Australia.

These routines utilize a SQL function, check_add_tga_drug, which will handle error checking (duplicate, bad input, etc), as well as creating unique indices

Procedure:

1. Execute 'bash read_mims_drugs_txt.bash', if eureka_mims_drugs_load.sql needs to be remade. 'eureka_mims_drugs_load.sql' file contains 8 fields as of 04.11.2019.
2. Execute 'bash load_tga_patch_drug.bash'. Executes eureka_tga_patch.sql and eureka_mims_drugs_load.sql routines

*note
This table data is old. Dated 2017.08



/* ******************************************************************
*Func	: drugs.check_add_tga_drug
*Desc	: Support for MIMS (Australian TGA) .
*Date	: 11 APR 2019
*Author : Benjamin Burk
******************************************************************* */
CREATE OR REPLACE FUNCTION drugs.check_add_tga_drug( _drug JSONB)
RETURNS VOID AS
$check_add_tga_drug$
DECLARE
	d JSONB;
	id_pharma		INTEGER = 0;
	id_pkg			INTEGER = 0;
	id_drug			INTEGER = 0;
	id_name			INTEGER = 0;
	id_strength_unit	INTEGER = 0;
	id_form			INTEGER = 0;
	id_sch			INTEGER = 0;
	id_mfg			INTEGER = 0;
	id_color		INTEGER = 0;
	id_shape		INTEGER = 0;
	auth drugs.authority%ROWTYPE;
	barcode	TEXT;
BEGIN
	FOR d in (SELECT jsonb_array_elements(_drug))
	LOOP
		RAISE NOTICE 'TGA Drug %', d->>'code_product';
		IF d->'mfg_name' is NULL or d->>'mfg_name' = '' THEN
			id_mfg = 0;
		ELSE
			SELECT * FROM drugs.check_add_mfg(COALESCE(d->>'mfg_name', NULL)) INTO id_mfg;
		END IF;

		IF d->'schedule' is NULL or d->>'schedule' = '' THEN
			id_sch = 0;
		ELSE
			SELECT * FROM drugs.check_add_sch(COALESCE(d->>'schedule', NULL)) INTO id_sch;
		END IF;

		--authority and authority_pharma
		SELECT * FROM drugs.authority WHERE code_prod = d->>'code_product' INTO auth;
		IF NOT FOUND THEN

			SELECT id FROM drugs.authority_pharma
				WHERE authority_pharma.brand_name_id = id_name
				AND authority_pharma.strength_unit_id = id_strength_unit
				AND authority_pharma.dosage_form_id = id_form
				AND authority_pharma.dea_sch_id = id_sch
			INTO id_pharma;

			IF NOT FOUND THEN
				INSERT INTO drugs.authority_pharma
					(brand_name_id, strength_unit_id, dosage_form_id, dea_sch_id)
				VALUES
					(id_name, id_strength_unit, id_form, id_sch)
				RETURNING id INTO id_pharma;
			END IF;

			INSERT INTO drugs.authority
				(code_prod, code_prod_display, code_virt, authority_type_tag, authority_pharma_id, drug_display)
			VALUES
				(	(d->>'code_product')::varchar(32),
					(d->>'code_product')::varchar(32),
					COALESCE(d->>'code_virt', NULL)::varchar(32),
					'TGA',
					id_pharma,
					(d->>'display_name')::varchar(256)
				)
			RETURNING * INTO auth;
		END IF;

		--authority_pkg
		SELECT id FROM drugs.authority_pkg WHERE code_pkg = d->>'code_product' INTO id_pkg;
		IF NOT FOUND THEN
			barcode = COALESCE(d->>'barcode', NULL);
			IF barcode = '' THEN barcode = NULL; END IF;
			INSERT INTO drugs.authority_pkg
				(authority_id, code_pkg, code_pkg_virt, code_pkg_display, pkg_qty)
			VALUES
				(auth.id,
				d->>'code_product',
				COALESCE(d->>'code_virt', NULL)::varchar(32),
				d->>'code_product',
				(COALESCE((d->>'package_qty')::REAL, 0)))
			RETURNING id INTO id_pkg;
		END IF;
		IF d->>'barcode' IS NOT NULL THEN
			IF (SELECT count(*) FROM drugs.authority_pkg WHERE pkg_barcode = d->>'barcode') = 0 THEN
				UPDATE drugs.authority_pkg
					SET pkg_barcode = d->>'barcode'
				WHERE id = id_pkg;
			END IF;
		END IF;

		--drug
		SELECT id FROM drugs.drug WHERE authority_id = auth.id AND manufacture_id = id_mfg INTO id_drug;
		IF NOT FOUND THEN
			INSERT INTO drugs.drug
				(authority_id, manufacture_id, active)
			VALUES
				(auth.id, id_mfg, FALSE)
			RETURNING id INTO id_drug;
		END IF;
	END LOOP;
END;
$check_add_tga_drug$
LANGUAGE plpgsql VOLATILE SECURITY DEFINER
COST 100;
ALTER FUNCTION drugs.check_add_tga_drug (JSONB) OWNER TO sueureka;



/* ******************************************************************
*Desc 	: To initially set the tga database
*Date	: 11 APR 2019
*Author : Benjamin Burk
******************************************************************* */

--authority_types
INSERT INTO authority_types(tag, description, display, display_code_pkg, display_code_prod, display_code_virt, display_code_pkg_virt, is_default)
VALUES ('TGA'::TEXT, 'Therapeutic Goods Administration of Australia'::TEXT, 'TGA'::TEXT, 'TGA'::TEXT, 'TGA'::TEXT, ''::TEXT, ''::TEXT, FALSE);

--system_settings
UPDATE system.system_settings SET value = 'TGA'
	WHERE tag in ('DRUG_CODE_METHOD','DRUG_AUTHORITY');

--drugs.authority_type
UPDATE drugs.authority_types SET is_default = FALSE;
UPDATE drugs.authority_types SET is_default = TRUE WHERE tag = 'TGA';

--Timing
UPDATE orders.timing
	SET description = 'AM Timing',
		display = 'AM',
		label_text = 'AM'
WHERE tag = 'TIMING_AM';

UPDATE orders.timing
	SET description = 'Noon Timing',
		display = 'NOON',
		label_text = 'Noon'
WHERE tag = 'TIMING_NOON';

UPDATE orders.timing
	SET description = 'PM Timing',
		display = 'PM',
		label_text = 'PM'
WHERE tag = 'TIMING_PM';

UPDATE orders.timing
	SET description = 'Bed Timing',
		display = 'Bed',
		label_text = 'Bed'
WHERE tag = 'TIMING_BED';


--Packages for mediclear
UPDATE packages.package_types SET active = TRUE where tag = 'PKG_TYPE_MULTI_RT_CLASSIC_CLEAR_PORTRAIT';

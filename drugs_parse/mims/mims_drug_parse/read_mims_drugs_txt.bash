#!/bin/bash

#File	: read_mims_drugs_txt.bash
#Desc	: Parses drug csv tables from MIMS into sql load routine
#Date	: 12 MAR 2019
#Author	: Benjamin Burk

out_file="eureka_mims_drugs_load.sql"
proddat_txt_file="./tables/PRODDAT.txt"
formdat_txt_file="./tables/FORMDAT.txt"
packdat_txt_file="./tables/PACKDAT.txt"
cmpdat_txt_file="./tables/CMPDAT.txt"
linkamt_txt_file="./tables/LinkAMT.txt"
amtdesc_txt_file="./tables/AMTdescription.txt"
#lnkabbvfull_txt_file="./tables/lnkAbbreviatedToFullPI.txt"
#maniddat_txt_file="./tables/MANIDDAT.txt"
prodartg_txt_file="./tables/productARTG.txt"

#conversion from WINDOWS newline to UNIX newline, for processing

if [[ "$(file "$proddat_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${proddat_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${proddat_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$proddat_txt_file" "${proddat_txt_file%.txt}_UNIX.txt"
	dos2unix "${proddat_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	proddat_txt_file="${proddat_txt_file%.txt}_UNIX.txt"
fi

if [[ "$(file "$formdat_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${formdat_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${formdat_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$formdat_txt_file" "${formdat_txt_file%.txt}_UNIX.txt"
	dos2unix "${formdat_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	formdat_txt_file="${formdat_txt_file%.txt}_UNIX.txt"
fi

if [[ "$(file "$packdat_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${packdat_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${packdat_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$packdat_txt_file" "${packdat_txt_file%.txt}_UNIX.txt"
	dos2unix "${packdat_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	packdat_txt_file="${packdat_txt_file%.txt}_UNIX.txt"
fi

if [[ "$(file "$cmpdat_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${cmpdat_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${cmpdat_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$cmpdat_txt_file" "${cmpdat_txt_file%.txt}_UNIX.txt"
	dos2unix "${cmpdat_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	cmpdat_txt_file="${cmpdat_txt_file%.txt}_UNIX.txt"
fi

if [[ "$(file "$linkamt_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${linkamt_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${linkamt_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$linkamt_txt_file" "${linkamt_txt_file%.txt}_UNIX.txt"
	dos2unix "${linkamt_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	linkamt_txt_file="${linkamt_txt_file%.txt}_UNIX.txt"
fi

if [[ "$(file "$amtdesc_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${amtdesc_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${amtdesc_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$amtdesc_txt_file" "${amtdesc_txt_file%.txt}_UNIX.txt"
	dos2unix "${amtdesc_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	amtdesc_txt_file="${amtdesc_txt_file%.txt}_UNIX.txt"
fi

#if [[ "$(file "$lnkabbvfull_txt_file")" == *"CRLF"* ]]; then
	#if [ -f "${lnkabbvfull_txt_file%.txt}_UNIX.txt" ]; then
		#rm -f "${lnkabbvfull_txt_file%.txt}_UNIX.txt"
	#fi

	#cp "$lnkabbvfull_txt_file" "${lnkabbvfull_txt_file%.txt}_UNIX.txt"
	#dos2unix "${lnkabbvfull_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	#lnkabbvfull_txt_file="${lnkabbvfull_txt_file%.txt}_UNIX.txt"
#fi

#if [[ "$(file "$maniddat_txt_file")" == *"CRLF"* ]]; then
	#if [ -f "${maniddat_txt_file%.txt}_UNIX.txt" ]; then
		#rm -f "${maniddat_txt_file%.txt}_UNIX.txt"
	#fi
	
	#cp "$maniddat_txt_file" "${maniddat_txt_file%.txt}_UNIX.txt"
	#dos2unix "${maniddat_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	#maniddat_txt_file="${maniddat_txt_file%.txt}_UNIX.txt"
#fi

if [[ "$(file "$prodartg_txt_file")" == *"CRLF"* ]]; then
	if [ -f "${prodartg_txt_file%.txt}_UNIX.txt" ]; then
		rm -f "${prodartg_txt_file%.txt}_UNIX.txt"
	fi
	
	cp "$prodartg_txt_file" "${prodartg_txt_file%.txt}_UNIX.txt"
	dos2unix "${prodartg_txt_file%.txt}_UNIX.txt" >/dev/null 2>&1
	prodartg_txt_file="${prodartg_txt_file%.txt}_UNIX.txt"
fi

packdat_num_entries=$(cat "$packdat_txt_file" | wc -l)

#PRODDAT FORMAT														FORMDAT FORMAT
#prodcode = key of PRODDAT												prodcode = key of PRODDAT
#product = Product name. May contain formatting tags									formcode = key of FORMDAT
#section = value from SUBDAT.section											formsort =  sort order for formulations
#subsection = value from SUBDAT.subsection										form = Formulation type (Tablets, Powder, Liquid)
#pc = ADEC pregnancy categorization											rx = Boolean value indicating whether form is Rx only
#p_text = Boolean value indicating presence of text with pregnancy category						rx_text = Text accompanying Rx only column
#cmpcode = key of CMPDAT												co = Composition
#dis = Note to follow manufacturer name											gf = Boolean value indicating if form is gluten free
#cmi = Boolean value indicating presence of consumer medicine information						brand = Brand name for specific formulation
#use = Uses/indications. May contain formatting tags									da = Dosage administration 
#ci = Contraindications. May contain formatting tags									foodcode = code from FOODDAT 
#pr = Precautions. May contain formatting tags										drowsy = Boolean value indicating drowsiness
#ar = Adverse reactions. May contain formatting tags									CMIcode = CMI code
#ir = Drug interactions. May contain formatting tags									GenericList = List of generic ingredients w/ formatting
#wa = Other warnings.													tfGenericList = List of generic ingredients non-format
#manpage = REMOVED
#xref = REMOVED
#sportcode = key of SPORTDAT
#s11 = Boolean value indicating if product is S11 in Victoria
#prodsort = Used for ASCII sort for products in book. May contain formatting tags
#mancode = Related MIMS full prescribing information (MIMS Annual) primary key
#deleted = Shows the month when a deleted product was withdrawn
#tfproduct = Tag-free product name. Use when non-formatted text is required


#PACKDAT FORMAT
#prodcode = key of PRODDAT
#formcode = key of FORMDAT
#packcode = key of PACKDAT
#packsort = Sort order for packs.
#active = Numeral of active ingredient
#active_units = Unit of active ingredient
#per_volume = Volume or mass size containing the amount of active ingredient (active, active_units)
#per_vol_units = Units for containing mass size
#unit_volume = Volume or mass size in each pack (liquids)
#unit_vol_units = Units for liquids
#solvent = Solvent information
#misc = Other pack information
#units_per_pack = Number of units (i.e. tablets) in each pack
#no_of_packs = Number of packs to be dispensed together
#pbs_code = PBS code (Schedule of Pharmaceutical Benefits)
#pbs = PBS status
#rp = Number of repeats allowed by the PBS
#equiv = Boolean value indicating whether brand substitution is permitted
#authcode = key of INDDAT
#restcode = key of INDDAT
#pbs_price = PBS price
#bpp = Brand price premium
#spec_contrib = Additional amount patient must pay as special contribution
#price = Price to patient if not purchased under PBS
#disp_fee = Boolean value indicating whether price includes a dispensing fee
#dd_fee = Boolean value indicating whether price includes a dangerous drug fee
#s3r_fee = Boolean value indicating whether price includes a S3 recordable fee
#tgp = Therapeutic Group Premium price - additional amount needed to pay as a premium
#tgp_group = Code for the Therapeutic Group
#sec100code = key of INDDAT
#MANcode = Manufacturer Code from the PBS data
#DispFeeCode = PBS Dispensing Fee Code

#PRODDAT FILE FORMAT
#prodcode|product|section|subsection|pc|p_text|cmpcode|dis|cmi|use|ci|pr|ar|ir|wa|sportcode|s11|prodsort|mancode|deleted|tfproduct

#FORMDAT FILE FORMAT
#prodcode|formcode|formsort|form|rx|rx_text|co|gf|brand|da|foodcode|drowsy|CMIcode|GenericList|tfGenericList

#PACKDAT FILE FORMAT
#prodcode|formcode|packcode|packsort|active|active_units|per_volume|per_vol_units|unit_volume|unit_vol_units|solvent|misc|units_per_pack|no_of_packs|pbs_code|pbs|rp|equiv|authcode|restcode|pbs_price|bpp|spec_contrib|price|disp_fee|dd_fee|s3r_fee|tgp|tgp_group|sec100code|MANcode|DispFeeCode


#MIMS DATA FORMAT EXAMPLE:
#	1 drug name (prodcode)
#	MULTIPLE DRUG FORMS
#	MULTIPLE DRUG PACKAGES, FOR MULTIPLE FORMS


#define lookup tables, as I have no header row to read from
#if these column positions change, this table must too

declare -A proddat_aarr
declare -A formdat_aarr
declare -A packdat_aarr
declare -A cmpdat_aarr
declare -A linkamt_aarr
declare -A amtdesc_aarr
#declare -A lnkabbvfull_aarr
#declare -A maniddat_aarr
declare -A prodartg_aarr
proddat_aarr+=( ["prodcode"]=1 ["product"]=2 ["section"]=3 ["subsection"]=4 ["pc"]=5 ["p_text"]=6 ["cmpcode"]=7 ["dis"]=8 ["cmi"]=9 ["use"]=10 ["ci"]=11 ["pr"]=12 ["ar"]=13 ["ir"]=14 ["wa"]=15 ["manpage"]=16 ["xref"]=17 ["sportcode"]=18 ["s11"]=19 ["prodsort"]=20 ["mancode"]=21 ["deleted"]=22 ["tfproduct"]=23 )
formdat_aarr+=( ["prodcode"]=1 ["formcode"]=2 ["formsort"]=3 ["form"]=4 ["rx"]=5 ["rx_text"]=6 ["co"]=7 ["gf"]=8 ["brand"]=9 ["da"]=10 ["foodcode"]=11 ["drowsy"]=12 ["CMIcode"]=13 ["GenericList"]=14 ["tfGenericList"]=15 )
packdat_aarr+=( ["prodcode"]=1 ["formcode"]=2 ["packcode"]=3 ["packsort"]=4 ["active"]=5 ["active_units"]=6 ["per_volume"]=7 ["per_vol_units"]=8 ["unit_volume"]=9 ["unit_vol_units"]=10 ["solvent"]=11 ["misc"]=12 ["units_per_pack"]=13 ["no_of_packs"]=14 ["pbs_code"]=15 ["pbs"]=16 ["rp"]=17 ["equiv"]=18 ["authcode"]=19 ["restcode"]=20 ["pbs_price"]=21 ["bpp"]=22 ["spec_contrib"]=23 ["price"]=24 ["disp_fee"]=25 ["dd_fee"]=26 ["s3r_fee"]=27 ["tgp"]=28 ["tgp_group"]=29 ["sec100code"]=30 ["MANcode"]=31 ["DispFeeCode"]=32 )
cmpdat_aarr+=( ["cmpcode"]=1 ["company"]=2 ["cmpabbrev"]=3 ["address1"]=4 ["address2"]=5 ["address3"]=6 ["postcode"]=7 ["phone"]=8 ["fax"]=9 ["toll_free"]=10 ["email"]=11 ["web_site"]=12 ["mail_order"]=13 ["subtitle"]=14 )
linkamt_aarr+=( ["prodcode"]=1 ["formcode"]=2 ["packcode"]=3 ["itemcode"]=4 ["gencode"]=5 ["amtcode"]=6 ["amttype"]=7 ["mimsterm"]=8 ["node"]=9 ["comment"]=10 )
amtdesc_aarr+=( ["amtcode"]=1 ["amtpreferredterm"]=2 ["amttype"]=3 ["fobsolete"]=4 )
#lnkabbvfull_aarr+=( ["AbbreviatedProductCode"]=1 ["FullProductCode"]=2 )
#maniddat_aarr+=( ["mancode"]=1 ["grid"]=2 ["description"]=3 ["colour"]=4 ["alpha"]=5 ["image"]=6 )
prodartg_aarr+=( ["prodcode"]=1 ["formcode"]=2 ["packcode"]=3 ["tga_code"]=4 )

#define csv columns to locate
proddat_used_col=("prodcode" "cmpcode" "tfproduct" "mancode")	#I use cmpcode as an anchor for cmpdat. I use tfproduct if amtpreferredterm falls through. I use mancode as an anchor for lnkabbvfull, and by relation maniddat
formdat_used_col=("prodcode" "formcode" "form")
packdat_used_col=("prodcode" "formcode" "packcode" "active" "active_units" "per_volume" "per_vol_units" "unit_volume" "unit_vol_units" "misc" "pbs_code" "units_per_pack")
cmpdat_used_col=("cmpcode" "company")	#invoked during proddat loop
linkamt_used_col=("amtcode" "amttype")		#I use amtcode as an anchor for amtdesc
amtdesc_used_col=("amtcode" "amtpreferredterm")
#lnkabbvfull_used_col=("AbbreviatedProductCode" "FullProductCode")		#I use FullProductCode as an anchor for maniddat
#maniddat_used_col=("mancode" "colour" "image")
prodartg_used_col=("tga_code")

#list of positions of columns defined above. defined dynamically. values match keys of *_used_col
proddat_del_pos=()
formdat_del_pos=()
packdat_del_pos=()
cmpdat_del_pos=()
linkamt_del_pos=()
amtdesc_del_pos=()
#lnkabbvfull_del_pos=()
#maniddat_del_pos=()
prodartg_del_pos=()

#fill positions if found. if a key is not found in the provided list, exit with error message
for (( i=0;i<${#proddat_used_col[@]};i++ )); do
	for key in ${!proddat_aarr[@]}; do
		if [[ "${key}" == "${proddat_used_col[${i}]}" ]]; then
			proddat_del_pos+=( ${proddat_aarr[${key}]} )
			break
		fi
	done
	if [ ${#proddat_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find PRODDAT key \""${proddat_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done
for (( i=0;i<${#formdat_used_col[@]};i++ )); do
	for key in ${!formdat_aarr[@]}; do
		if [[ "${key}" == "${formdat_used_col[${i}]}" ]]; then
			formdat_del_pos+=( ${formdat_aarr[${key}]} )
			break
		fi
	done
	if [ ${#formdat_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find FORMDAT key \""${formdat_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done
for (( i=0;i<${#packdat_used_col[@]};i++ )); do
	for key in ${!packdat_aarr[@]}; do
		if [[ "${key}" == "${packdat_used_col[${i}]}" ]]; then
			packdat_del_pos+=( ${packdat_aarr[${key}]} )
			break
		fi
	done
	if [ ${#packdat_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find PACKDAT key \""${packdat_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done
for (( i=0;i<${#cmpdat_used_col[@]};i++ )); do
	for key in ${!cmpdat_aarr[@]}; do
		if [[ "${key}" == "${cmpdat_used_col[${i}]}" ]]; then
			cmpdat_del_pos+=( ${cmpdat_aarr[${key}]} )
			break
		fi
	done
	if [ ${#cmpdat_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find CMPDAT key \""${cmpdat_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done
for (( i=0;i<${#linkamt_used_col[@]};i++ )); do
	for key in ${!linkamt_aarr[@]}; do
		if [[ "${key}" == "${linkamt_used_col[${i}]}" ]]; then
			linkamt_del_pos+=( ${linkamt_aarr[${key}]} )
			break
		fi
	done
	if [ ${#linkamt_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find LINKAMT key \""${linkamt_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done
for (( i=0;i<${#amtdesc_used_col[@]};i++ )); do
	for key in ${!amtdesc_aarr[@]}; do
		if [[ "${key}" == "${amtdesc_used_col[${i}]}" ]]; then
			amtdesc_del_pos+=( ${amtdesc_aarr[${key}]} )
			break
		fi
	done
	if [ ${#amtdesc_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find AMTDESCRIPTION key \""${amtdesc_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done
#for (( i=0;i<${#lnkabbvfull_used_col[@]};i++ )); do
	#for key in ${!lnkabbvfull_aarr[@]}; do
		#if [[ "${key}" == "${lnkabbvfull_used_col[${i}]}" ]]; then
			#lnkabbvfull_del_pos+=( ${lnkabbvfull_aarr[${key}]} )
			#break
		#fi
	#done
	#if [ ${#lnkabbvfull_del_pos[@]} -ne $((i+1)) ]; then
		#echo -e "\033[31;1mERROR! Could not find LNKABBVFULL key \""${lnkabbvfull_used_col[${i}]}"\"!\033[0m"
		#exit 1
	#fi
#done
#for (( i=0;i<${#maniddat_used_col[@]};i++ )); do
	#for key in ${!maniddat_aarr[@]}; do
		#if [[ "${key}" == "${maniddat_used_col[${i}]}" ]]; then
			#maniddat_del_pos+=( ${maniddat_aarr[${key}]} )
			#break
		#fi
	#done
	#if [ ${#maniddat_del_pos[@]} -ne $((i+1)) ]; then
		#echo -e "\033[31;1mERROR! Could not find MANIDDAT key \""${maniddat_used_col[${i}]}"\"!\033[0m"
		#exit 1
	#fi
#done
for (( i=0;i<${#prodartg_used_col[@]};i++ )); do
	for key in ${!prodartg_aarr[@]}; do
		if [[ "${key}" == "${prodartg_used_col[${i}]}" ]]; then
			prodartg_del_pos+=( ${prodartg_aarr[${key}]} )
			break
		fi
	done
	if [ ${#prodartg_del_pos[@]} -ne $((i+1)) ]; then
		echo -e "\033[31;1mERROR! Could not find PRODARTG key \""${prodartg_used_col[${i}]}"\"!\033[0m"
		exit 1
	fi
done

#list of fields to write. defined dynamically
line_fields=()

#check for 'prodcode, formcode, and packcode' dependency in PACKDAT file
packdat_id_prod=-1
packdat_id_form=-1
packdat_id_pack=-1
for (( j=0;j<${#packdat_used_col[@]};j++ )); do
	if [[ "${packdat_used_col[${j}]}" == "prodcode" ]]; then
		packdat_id_prod=$((packdat_del_pos[${j}]-1))
	fi
	if [[ "${packdat_used_col[${j}]}" == "formcode" ]]; then
		packdat_id_form=$((packdat_del_pos[${j}]-1))
	fi
	if [[ "${packdat_used_col[${j}]}" == "packcode" ]]; then
		packdat_id_pack=$((packdat_del_pos[${j}]-1))
	fi
done
		
if [ $packdat_id_prod -lt 0 ] || [ $packdat_id_form -lt 0 ] || [ $packdat_id_pack -lt 0 ]; then
	echo -e "\033[31;1mERROR! Missing {prodcode, formcode, packcode} key combination in MIMS table PACKDAT!\033[0m"
	echo -e "\033[31;1mPlease ensure all of the following are present in the packdat_used_col variable :	prodcode, formcode, packcode\033[0m"
	exit 1
fi

#check for 'cmpcode' dependency in PRODDAT file
proddat_id_cmp=-1
for (( k=0;k<${#proddat_used_col[@]};k++ )); do
	if [[ "${proddat_used_col[${k}]}" == "cmpcode" ]]; then
		proddat_id_cmp=$((proddat_del_pos[${k}]-1))
		break
	fi
done

if [ $proddat_id_cmp -lt 0 ]; then
	echo -e "\033[31;1mERROR! Missing 'cmpcode' key in MIMS table PRODDAT!\033[0m"
	echo -e "\033[31;1mPlease ensure all of the following are present in the proddat_used_col variable :	cmpcode\033[0m"
	exit 1
fi

linkamt_id_code=-1
linkamt_id_type=-1
for (( l=0;l<${#linkamt_used_col[@]};l++ )); do
	if [[ "${linkamt_used_col[${l}]}" == "amtcode" ]]; then
		linkamt_id_code=${linkamt_del_pos[${l}]}
	fi
	if [[ "${linkamt_used_col[${l}]}" == "amttype" ]]; then
		linkamt_id_type=${linkamt_del_pos[${l}]}
	fi
done

if [ $linkamt_id_code -lt 0 ] || [ $linkamt_id_type -lt 0 ]; then
	echo -e "\033[31;1mERROR! Missing {amtcode, amttype} key combination in MIMS table LINKAMT!\033[0m"
	echo -e "\033[31;1mPlease ensure all of the following are present in the linkamt_used_col variable:	amtcode, amttype\033[0m"
	exit 1
fi

amtdesc_id_term=-1
for (( m=0;m<${#amtdesc_used_col[@]};m++ )); do
	if [[ "${amtdesc_used_col[${m}]}" == "amtpreferredterm" ]]; then
		amtdesc_id_term=${amtdesc_del_pos[${m}]}
		break
	fi
done

if [ $amtdesc_id_term -lt 0 ]; then
	echo -e "\033[31;1mERROR! Missing 'amtpreferredterm' key in MIMS table AMTDESCRIPTION!\033[0m"
	echo -e "\033[31;1mPlease ensure all of the following are present in the amtdesc_used_col variable:	amtpreferredterm\033[0m"
	exit 1
fi

#lnkabbvfull_id_prod=-1
#lnkabbvfull_id_man=-1
#for (( n=0;n<${#lnkabbvfull_used_col[@]};n++ )); do
	#if [[ "${lnkabbvfull_used_col[${n}]}" == "AbbreviatedProductCode" ]]; then
		#lnkabbvfull_id_prod=${lnkabbvfull_del_pos[${n}]}
	#fi
	#if [[ "${lnkabbvfull_used_col[${n}]}" == "FullProductCode" ]]; then
		#lnkabbvfull_id_man=${lnkabbvfull_del_pos[${n}]}
	#fi
#done

#if [ $lnkabbvfull_id_prod -lt 0 ] || [ $lnkabbvfull_id_man -lt 0 ]; then
	#echo -e "\033[31;1mERROR! Missing {AbbreviatedProductCode, FullProductCode} key combination in MIMS table LNKABBVFULL!\033[0m"
	#echo -e "\033[31;1mPlease ensure all of the following are present in the lnkabbvfull_used_col variable:		AbbreviatedProductCode, FullProductCode\033[0m"
	#exit 1
#fi

#maniddat_id_man=-1
#for (( o=0;o<${#maniddat_used_col[@]};o++ )); do
	#if [[ "${maniddat_used_col[${o}]}" == "mancode" ]]; then
		#maniddat_id_man=${maniddat_del_pos[${o}]}
		#break
	#fi
#done

#if [ $maniddat_id_man -lt 0 ]; then
	#echo -e "\033[31;1mERROR! Missing 'mancode' key in MIMS table MANIDDAT!\033[0m"
	#echo -e "\033[31;1mPlease ensure all of the following are present in the maniddat_used_col variable:		mancode\033[0m"
	#exit 1
#fi

prodartg_id_artg=-1
for (( p=0;p<${#prodartg_used_col[@]};p++ ));do
	if [[ "${prodartg_used_col[${p}]}" == "tga_code" ]]; then
		prodartg_id_artg=${prodartg_del_pos[${p}]}
		break
	fi
done

if [ $prodartg_id_artg -lt 0 ]; then
	echo -e "\033[31;1mERROR! Missing 'tga_code' key in MIMS table PRODARTG!\033[0m"
	echo -e "\033[31;1mPlease ensure all of the following are present in the prodartg_used_col variable:		tga_code\033[0m"
	exit 1
fi

#write header portion
echo "/*****************************************************" > $out_file
echo "File		: eureka_mims_drugs_load.sql" >> $out_file
echo "Desc		: Output from integration with MIMS tables" >> $out_file
echo "Date		: 12 MAR 2019" >> $out_file
echo "Date Compiled 	: $(date)" >> $out_file
echo "Author		: Benjamin Burk" >> $out_file
echo "" >> $out_file
echo "*****************************************************/" >> $out_file
echo "" >> $out_file
echo "--tga_authority" >> $out_file
echo "SET session_replication_role = replica;" >> $out_file
echo "SELECT * FROM drugs.check_add_tga_drug('" >> $out_file
printf " [" >> $out_file

line_no=0
#delimiter for MIMS tables is the '|', break character
while [ -s "$packdat_txt_file" ]; do

	ind=0
	line_fields=()
	packdat_queue=()
	formdat_list=()
	declare -A formdat_current_defs ; formdat_current_defs=()
	filter_packdat="$(cat "$packdat_txt_file" | sed "1q;d")"	#always grab first line, routines below to remove all from queue
	filter_formdat="$(echo "$filter_packdat" | cut -d\| -f${packdat_del_pos[${packdat_id_prod}]})"
	filter_proddat="$(cat "$proddat_txt_file" | grep -w "^$filter_formdat")"

	queue_packdatfilter="$(cat "$packdat_txt_file" | grep -wn "^$filter_formdat" | cut -f1 -d: | tr '\n' ',' | sed 's/.$//')"
	list_formdatfilter="$(cat "$formdat_txt_file" | grep -wn "^$filter_formdat" | cut -f1 -d: | tr '\n' ',' | sed 's/.$//')"

	packdat_queue=(${queue_packdatfilter//,/ })
	formdat_list=(${list_formdatfilter//,/ })
	for (( a=0;a<${#formdat_list[@]};a++ )); do		#build dictionary for formdat reference. associative array, i.e. ["formcode"]=line_number
		formdat_current_defs+=( ["$(cat "$formdat_txt_file" | sed "${formdat_list[${a}]}q;d" | cut -d\| -f${packdat_del_pos[${packdat_id_form}]})"]=${formdat_list[${a}]} )
	done

	for (( b=0;b<${#packdat_queue[@]};b++ )); do
		ind=0
		line_no=$((line_no+1))
		line_fields=()
		linkamt_list=()
		c_prod="$(cat "$packdat_txt_file" | sed "${packdat_queue[${b}]}q;d" | cut -d\| -f${packdat_del_pos[${packdat_id_prod}]})"
		c_form="$(cat "$packdat_txt_file" | sed "${packdat_queue[${b}]}q;d" | cut -d\| -f${packdat_del_pos[${packdat_id_form}]})"
		c_pack="$(cat "$packdat_txt_file" | sed "${packdat_queue[${b}]}q;d" | cut -d\| -f${packdat_del_pos[${packdat_id_pack}]})"
		printf 'INFO: processing PACKDAT queue item %s out of %s : prodcode %s, formcode %s, packcode %s\n' "$((b+1))" "${#packdat_queue[@]}" "$c_prod" "$c_form" "$c_pack"
		line_fields+=( "$c_prod"'-'"$c_form"'-'"$c_pack" )										#ADD MIMS CODE
		tga_code="$(cat "$prodartg_txt_file" | grep -w '^'"$c_prod"'|'"$c_form"'|'"$c_pack" | cut -d\| -f${prodartg_id_artg})"
		if [[ $(echo "$tga_code" | wc -l) -eq 1 ]] && [[ ! -z "$tga_code" ]]; then
			if [[ "$tga_code" == *[,.\(\)]* ]]; then
				continue
			else
				line_fields+=( "$(echo "$tga_code" | tr -d "[:space:]")" )								#ADD TGA CODE
			fi
		else
			continue	#skip to next drug, as I will not support drugs with multiple TGA codes
		fi

		for (( c=0;c<${#packdat_del_pos[@]};c++ )); do
			if [[ "${packdat_used_col[${c}]}" == "prodcode" ]]; then
				for (( x=0;x<${#proddat_del_pos[@]};x++ )); do
					if [[ "${proddat_used_col[${x}]}" == "cmpcode" ]]; then							#ADD CMPDAT FIELDS
						for (( y=0;y<${#cmpdat_del_pos[@]};y++ )); do
							if [[ "${cmpdat_used_col[${y}]}" != "cmpcode" ]]; then
								line_fields+=( "$(cat "$cmpdat_txt_file" | grep -w "^$(echo "$filter_proddat" | cut -d\| -f${proddat_del_pos[${x}]})" | cut -d\| -f${cmpdat_del_pos[${y}]} | sed 's/[\x27`]/\x27"\x27"\x27/g')" )	#replace ' and ` with shell-safe expression '"'"'
							fi
						done
					elif [[ "${proddat_used_col[${x}]}" == "tfproduct" ]]; then						#ADD PRODDAT FIELDS
						list_linkamtfilter="$(cat "$linkamt_txt_file" | grep -wn '^'"$c_prod"'|'"$c_form"'|'"$c_pack" | cut -f1 -d: | tr '\n' ',' | sed 's/.$//')"
						if [[ -z "$list_linkamtfilter" ]]; then
							line_fields+=( "$(echo "$filter_proddat" | cut -d\| -f${proddat_del_pos[${x}]} | sed 's/[\x27`]/\x27"\x27"\x27/g')" )		#add tfproduct, as I couldn't find AMT data
							break
						else
							linkamt_list=(${list_linkamtfilter//,/ })
							for (( z=0;z<${#linkamt_list[@]};z++ )); do
								c_linkamt_type="$(cat "$linkamt_txt_file" | sed "${linkamt_list[${z}]}q;d" | cut -d\| -f$linkamt_id_type)"
								if [[ "$c_linkamt_type" == "TPP" ]]; then
									c_linkamt_code="$(cat "$linkamt_txt_file" | sed "${linkamt_list[${z}]}q;d" | cut -d\| -f$linkamt_id_code)"
									line_fields+=( "$(cat "$amtdesc_txt_file" | grep -w "$c_linkamt_code" | cut -d\| -f$amtdesc_id_term | sed 's/[\x27`]/\x27"\x27"\x27/g' | sed 's/\(.*\),.*/\1/')" )	#add AMT data, as it exists. Remove everything after the last comma
									break	#exit loop, so I don't add both amtpreferredterm and tfproduct
								fi
								if [ $((z+1)) -eq ${#linkamt_list[@]} ]; then
									line_fields+=( "$(echo "$filter_proddat" | cut -d\| -f${proddat_del_pos[${x}]} | sed 's/[\x27`]/\x27"\x27"\x27/g')" )
									break	#exit loop. I couldn't find TPP AMTTYPE, so exit after adding tfproduct data
								fi
							done
						fi
					#elif [[ "${proddat_used_col[${x}]}" == "mancode" ]]; then
						#c_lnkabbvfull_man="$(cat "$lnkabbvfull_txt_file" | grep -w "^$c_prod" | cut -d\| -f$lnkabbvfull_id_man)"
						#if [[ -z "$c_lnkabbvfull_man" ]]; then
							#line_fields+=( "" )
							#break
						#fi
						#for (( l=0;l<${#maniddat_del_pos[@]};l++ )); do
							#if [[ "${maniddat_used_col[${l}]}" != "mancode" ]]; then
								#if [[ -z "$(cat "$maniddat_txt_file" | grep -w "^$c_lnkabbvfull_man" | cut -d\| -f${maniddat_del_pos[${l}]})" ]]; then
									#line_fields+=( "" )
								#else
									#line_fields+=( "$(cat "$maniddat_txt_file" | grep -w "^$c_lnkabbvfull_man" | cut -d\| -f${maniddat_del_pos[${l}]})" )
								#fi
							#fi
						#done
					fi
				done
			fi
			if [[ "${packdat_used_col[${c}]}" == "formcode" ]]; then									#ADD FORMDAT FIELDS
				for (( y=0;y<${#formdat_del_pos[@]};y++ )); do
					if [[ "${formdat_used_col[${y}]}" != "prodcode" ]] && [[ "${formdat_used_col[${y}]}" != "formcode" ]]; then
						current_formcode="$(cat "$packdat_txt_file" | sed "${packdat_queue[${b}]}q;d" | cut -d\| -f${packdat_del_pos[${packdat_id_form}]})"
						for key in ${!formdat_current_defs[@]}; do
							if [[ "${key}" == "$current_formcode" ]]; then
								line_fields+=( "$(cat "$formdat_txt_file" | sed "${formdat_current_defs[${key}]}q;d" | cut -d\| -f${formdat_del_pos[${y}]})" )
							fi
						done
					fi
				done
			fi
			if [[ "${packdat_used_col[${c}]}" != "prodcode" ]] && [[ "${packdat_used_col[${c}]}" != "formcode" ]]; then			#ADD PACKDAT FIELDS
				if [[ "${packdat_used_col[${c}]}" != "packcode" ]]; then
					line_fields+=( "$(cat "$packdat_txt_file" | sed "${packdat_queue[${b}]}q;d" | cut -d\| -f${packdat_del_pos[${c}]})" )
				fi
			fi
		done
	
		#loops constructing sql routine line
		if [ $line_no -eq 1 ]; then										#start by writing MIMS code
			printf '{\"mims_code\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
		else
			printf '  {\"mims_code\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
		fi
		ind=$((ind+1))

		printf '\"code_product\":\"%s\",' "${line_fields[${ind}]}" >> $out_file					#write TGA code
		ind=$((ind+1))

		for (( j=0;j<${#proddat_used_col[@]};j++ )); do
			if [[ "${proddat_used_col[${j}]}" == "cmpcode" ]]; then
				for (( h=0;h<${#cmpdat_used_col[@]};h++ )); do
					if [[ "${cmpdat_used_col[${h}]}" == "company" ]]; then
						printf '\"mfg_name\":\"%s\",' "$(echo "${line_fields[${ind}]}" | sed 's/\x27"\x27"\x27/\x27\x27/g' | sed "s/[\]/\\\&/g")" >> $out_file	#replace shell-safe expression '"'"' with '', for SQL compliancy
						ind=$((ind+1))
					fi
				done
				printf '\"display_name\":\"%s\",' "$(echo "${line_fields[${ind}]}" | sed 's/\x27"\x27"\x27/\x27\x27/g' | sed "s/[\]/\\\&/g")" >> $out_file
				ind=$((ind+1))
				#for (( w=0;w<${#maniddat_used_col[@]};w++ )); do
					#if [[ "${maniddat_used_col[${w}]}" == "colour" ]]; then
						#printf '\"color\":\"%s\",' "$(echo "${line_fields[${ind}]}")" >> $out_file
						#ind=$((ind+1))
					#fi
					#if [[ "${maniddat_used_col[${w}]}" == "image" ]]; then
						#printf '\"image\":\"%s\",' "$(echo "${line_fields[${ind}]}")" >> $out_file
						#ind=$((ind+1))
					#fi
				#done
			fi
		done

		for (( k=0;k<${#formdat_used_col[@]};k++ )); do
			if [[ "${formdat_used_col[${k}]}" == "form" ]]; then
				printf '\"form\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
				ind=$((ind+1))
			fi
		done
		dosage_units_null=0
		for (( l=0;l<${#packdat_used_col[@]};l++ )); do
			if [[ "${packdat_used_col[${l}]}" == "active" ]]; then
				if [[ -z "${line_fields[${ind}]}" ]]; then
					printf '\"strength_unit\":\"' >> $out_file
					dosage_units_null=$((dosage_units_null+1))
				else
					if [[ "${line_fields[${ind}]}" == *"<SUP>"* ]]; then		#expansion of units, if necessary
						exponent=$(echo "${line_fields[${ind}]}" | sed 's/<\/SUP>.*//' | sed 's/.*<SUP>//')
						base=$(echo "${line_fields[${ind}]}" | cut -d'<' -f1 | sed 's/.* //')
						common=$(echo "${line_fields[${ind}]}" |  cut -d' ' -f1)

						if [ $exponent -gt 2 ] && [ $exponent -lt 6 ] && [ $base -eq 10 ]; then					#thousand
							printf '\"strength_unit\":\"%s' "$common thousand" >> $out_file
						elif [ $exponent -gt 5 ] && [ $exponent -lt 9 ] && [ $base -eq 10 ]; then				#million
							printf '\"strength_unit\":\"%s' "$common million" >> $out_file
						else												#last resort, expand value
							printf '\"strength_unit\":\"%s' "$(echo "out=0;exponent=$exponent;base=$base;common=$common;out=common*(base^exponent);out/=1;out" | bc)" >> $out_file
						fi
					else
						printf '\"strength_unit\":\"%s' "${line_fields[${ind}]}" >> $out_file
					fi
				fi
				ind=$((ind+1))
			fi
			if [[ "${packdat_used_col[${l}]}" == "active_units" ]]; then
				if [[ -z "${line_fields[${ind}]}" ]]; then
					dosage_units_null=$((dosage_units_null+1))
					ind=$((ind+1))
					continue
				else
					if [[ "${line_fields[${ind}]}" == "%" ]]; then
						printf '%s' "$(echo "${line_fields[${ind}]}" | sed "s/[\]/\\\&/g")" >> $out_file
					else
						printf ' %s' "$(echo "${line_fields[${ind}]}" | sed "s/[\]/\\\&/g")"  >> $out_file
					fi
					ind=$((ind+1))
				fi
			fi
			if [[ "${packdat_used_col[${l}]}" == "per_volume" ]]; then
				if [[ -z "${line_fields[${ind}]}" ]]; then
					dosage_units_null=$((dosage_units_null+1))
					ind=$((ind+1))
					continue
				else
					printf '/%s' "${line_fields[${ind}]}" >> $out_file
					ind=$((ind+1))
				fi
			fi
			if [[ "${packdat_used_col[${l}]}" == "per_vol_units" ]]; then
				if [[ -z "${line_fields[${ind}]}" ]]; then
					dosage_units_null=$((dosage_units_null+1))
					ind=$((ind+1))
					continue
				else
					printf ' %s' "$(echo "${line_fields[${ind}]}" | sed "s/[\]/\\\&/g")" >> $out_file
					ind=$((ind+1))
				fi
			fi
			if [[ "${packdat_used_col[${l}]}" == "unit_volume" ]]; then
				if [[ -z "${line_fields[${ind}]}" ]]; then
					dosage_units_null=$((dosage_units_null+1))
					ind=$((ind+1))
					continue
				else
					if [ $dosage_units_null -eq 4 ]; then
						printf '%s' "${line_fields[${ind}]}" >> $out_file				
					else
						printf ' %s' "${line_fields[${ind}]}" >> $out_file
					fi
					ind=$((ind+1))
				fi
			fi
			if [[ "${packdat_used_col[${l}]}" == "unit_vol_units" ]]; then
				if [[ -z "${line_fields[${ind}]}" ]]; then
					dosage_units_null=$((dosage_units_null+1))
					ind=$((ind+1))
					continue
				else
					printf ' %s' "$(echo "${line_fields[${ind}]}" | sed "s/[\]/\\\&/g")" >> $out_file
					ind=$((ind+1))
				fi
			fi
			if [[ "${packdat_used_col[${l}]}" == "misc" ]]; then
				if [ $dosage_units_null -eq 6 ]; then						#only if I found no units, show misc data
					if [[ "${line_fields[${ind}]}" == *"<SUP>"* ]]; then		#expansion of units, if necessary
						exponent=$(echo "${line_fields[${ind}]}" | sed 's/<\/SUP>.*//' | sed 's/.*<SUP>//')
													#convert <SUB></SUB> expression to unicode exponent
						if [ $exponent -eq 2 ]; then
							printf '%s\",' "$(echo -e "${line_fields[${ind}]}" | sed 's/<SUP>'"$exponent"'<\/SUP>/\xc2\xb2/g' | sed "s/[\]/\\\&/g" | sed 's/\x27/\x27\x27/g')" >> $out_file	#hex value here is UTF-8, may need to be changed
						elif [ $exponent -eq 3 ]; then
							printf '%s\",' "$(echo -e "${line_fields[${ind}]}" | sed 's/<SUP>'"$exponent"'<\/SUP>/\xc2\xb3/g' | sed "s/[\]/\\\&/g" | sed 's/\x27/\x27\x27/g')" >> $out_file	#hex value here is UTF-8, may need to be changed
						elif [ $exponent -gt 3 ] && [ $exponent -lt 10 ]; then
							printf '%s\",' "$(echo -e "${line_fields[${ind}]}" | sed 's/<SUP>'"$exponent"'<\/SUP>/\xe2\x81\xb'"$exponent"'/g' | sed "s/[\]/\\\&/g" | sed 's/\x27/\x27\x27/g')" >> $out_file	#hex value here is UTF-8, may need to be changed
						fi
						ind=$((ind+1))

					elif [[ ! -z "${line_fields[${ind}]}" ]]; then
						printf '%s\",' "$(echo "${line_fields[${ind}]}" | sed "s/[\]/\\\&/g" | sed 's/\x27/\x27\x27/g')" >> $out_file
						ind=$((ind+1))
					else
						printf '\",' >> $out_file
						ind=$((ind+1))
					fi
				else
					printf '\",' >> $out_file		#write end of field
					ind=$((ind+1))
				fi
			fi		
			if [[ "${packdat_used_col[${l}]}" == "pbs_code" ]]; then				#write PBS code
				printf '\"pbs_code\":\"%s\",' "${line_fields[${ind}]}" >> $out_file 
				ind=$((ind+1))
			fi
			if [[ "${packdat_used_col[${l}]}" == "units_per_pack" ]]; then				#write package quantity
				if [[ -z "${line_fields[${ind}]}" ]]; then
					printf '\"pkg_qty\":\"0\"' >> $out_file
				else
					printf '\"pkg_qty\":\"%s\"' "${line_fields[${ind}]}" >> $out_file
				fi
				ind=$((ind+1))
			fi
		done

		if [ $line_no -eq 1 ] || [ $line_no -ne $packdat_num_entries ]; then
			printf '},\n' >> $out_file
		else
			printf '}]\n' >> $out_file
		fi
	done

	sed -i "/^\b$filter_formdat\b/d" "$packdat_txt_file"			#remove all lines that start with current prodcode
done

#write end lines
echo "'::JSONB);" >> $out_file
echo "SET session_replication_role = DEFAULT;" >> $out_file
echo "SELECT * FROM drugs.init_drug_package_volume();" >> $out_file
rm -f ../sed*									#remove all sed temporary files

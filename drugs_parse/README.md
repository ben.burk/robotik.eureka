Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS


	drugs_parse:		Processing routines for various drugs data sources (Spain, Australia, Switzerland).

		aemps:		SPANISH DRUG DATA ROUTINES
		mims: 		AUSTRALIAN DRUG DATA ROUTINES
		swiss_fr:	SWITZERLAND DRUG DATA ROUTINES (w/ stub project to reconstruct data from working unit. 'eureka_swiss_fr_drug_parse').



Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS

	swiss_fr_drug_parse:			SWITZERLAND DRUG DATA ROUTINES
	eureka_swiss_fr_drug_parse:		RECONSTRUCTION UTILITY. GEARED TOWARDS SWISS DATA. EASILY ADAPTED TO OTHER EUROPEAN SITES


Please read the 'README' files in each subdirectory

**NOTE**
Any .sql routines included in this archive have been generated ahead of time. All files, other than 'make_swiss_fr.sql' and 'character.sql', contain a timestamp of when they were created.

Ensure the information that will be changed is what is desired before running these.



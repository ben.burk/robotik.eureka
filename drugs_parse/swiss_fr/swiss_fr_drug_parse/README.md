Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

swiss_fr_drug_parse

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------


This project should only be used as needed. Currently supports "code_product", "display_name", "barcode" fields for Eureka DB.
Creates a .sql routine compliant with Eureka DB to load drugs. Takes a single CSV file as input.

Ideally, this should be used to load more drugs after the initial set.
These routines utilize a SQL function, check_add_swiss_fr_drug, which will handle error checking (duplicate, bad input, etc), as well as creating unique indices

Procedure:

1. Execute 'bash make_swiss_fr.bash'. Sets up db for SWISS drugs
2. Execute 'bash read_swiss_fr_csv.bash', if swiss_fr_drugs_load.sql needs to be remade. 'swiss_fr_drugs_load.sql' file contains 3 fields as of 02.14.2019.
3. Execute 'bash load_drug.bash'. Executes swiss_fr_drugs_load.sql routine

(optional)
4. Execute 'bash load_character.bash'. Populates drug characteristics tables with template data



**robotik_swiss.csv is a combination of 'Robotik liste *.xls'



#!/bin/bash

#File	: read_swiss_csv.bash
#Desc	: Constructs sql routine to load swiss_fr drugs
#Date	: 14 FEB 2019
#Author	: Benjamin Burk

#Reads csv file. Expects SWISS authority drugs with only 3 fields (SWISS id, authority display, barcode)

csv_file="robotik_swiss.csv"
out_file="swiss_fr_drugs_load.sql"
num_entries=$(cat "$csv_file" | wc -l)

#write header portion
echo "/*****************************************************" > $out_file
echo "File		: swiss_fr_drugs_load.sql" >> $out_file
echo "Desc		: Output file from csv incorporation" >> $out_file
echo "Date Compiled 	: $(date)" >> $out_file
echo "Author		: Benjamin Burk" >> $out_file
echo "" >> $out_file
echo "*****************************************************/" >> $out_file
echo "" >> $out_file
echo "--swiss_fr_authority" >> $out_file
echo "SET session_replication_role = replica;" >> $out_file
echo "SELECT * FROM drugs.check_add_swiss_fr_drug('" >> $out_file
printf " [" >> $out_file

for (( i=2;i<$num_entries;i++ )); do
	#grab current line, and grab fields
	filter="$(cat "$csv_file" | sed "$((i))q;d" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	swiss_id="$(echo "$filter" | cut -d, -f1 | sed 's/\"//g')"
	auth_dsply="$(echo "$filter" | cut -d, -f2 | sed "s/'/\'\'/g" | sed 's/\"//g')"
	barcode="$(echo "$filter" | cut -d, -f3 | sed 's/\"//g')"

	#experimental, not tested
	#schedule="$(echo "$filter" | cut -d, -f4 | sed 's/\"//g')"
	#drug_name="$(echo "$auth_dsply" | sed 's/[0-9].*//' | sed -e 's/[[:space:]]*$//')"
	#drug_dosage="$(echo "$auth_dsply" | sed 's/^[^0-9]*//g')"

	#change empty barcode format from '0' to empty string
	if [[ "$barcode" == "0" ]]; then
		barcode=""
	fi

	#serialize to entry
	if [ $i -eq 2 ]; then
		echo "{\"code_product\":\"$swiss_id\",\"display_name\":\"$auth_dsply\",\"barcode\":\"$barcode\"}," >> $out_file
	elif [ $i -eq $((num_entries-1)) ]; then
		echo "  {\"code_product\":\"$swiss_id\",\"display_name\":\"$auth_dsply\",\"barcode\":\"$barcode\"}]" >> $out_file
	else
		echo "  {\"code_product\":\"$swiss_id\",\"display_name\":\"$auth_dsply\",\"barcode\":\"$barcode\"}," >> $out_file
	fi
done

#write end lines
echo "'::JSONB);" >> $out_file
echo "SET session_replication_role = DEFAULT;" >> $out_file
echo "SELECT * FROM drugs.init_drug_package_volume();" >> $out_file

/*****************************************************
File	: character.sql
Desc	: Populates swiss_fr drug tables with working template data
Date	: 14 FEB 2019
Author	: Benjamin Burk

*****************************************************/

--drug_color

SELECT drugs.check_add_color('BRUN'::text);
SELECT drugs.check_add_color('BLANC'::text);
SELECT drugs.check_add_color('TRANSPARENT'::text);
SELECT drugs.check_add_color('ROUGE'::text);
SELECT drugs.check_add_color('JAUNE'::text);
SELECT drugs.check_add_color('JAUNE / BLANC'::text);
SELECT drugs.check_add_color('BLEU'::text);
SELECT drugs.check_add_color('ROSE'::text);
SELECT drugs.check_add_color('BLEU/BLANC'::text);
SELECT drugs.check_add_color('BLEU PÂLE'::text);
SELECT drugs.check_add_color('ROSE / BLANC'::text);
SELECT drugs.check_add_color('BEIGE'::text);
SELECT drugs.check_add_color('JAUNE-ORANGE'::text);
SELECT drugs.check_add_color('ORANGE'::text);

--drug_shape

SELECT drugs.check_add_shape('GÉLULE'::text);
SELECT drugs.check_add_shape('CPR'::text);
SELECT drugs.check_add_shape('CRÈME'::text);
SELECT drugs.check_add_shape('LIQUIDE'::text);
SELECT drugs.check_add_shape('POUDRE'::text);
SELECT drugs.check_add_shape('EMPLÂTRE'::text);
SELECT drugs.check_add_shape('COMPRESSES'::text);
SELECT drugs.check_add_shape('PANSEMENT'::text);
SELECT drugs.check_add_shape('GOUTTES'::text);
SELECT drugs.check_add_shape('DRAGÉE'::text);
SELECT drugs.check_add_shape('SOLUTION INJ.'::text);
SELECT drugs.check_add_shape('SUSPENSION'::text);

--drug_form

SELECT drugs.check_add_form('CRÈME'::text);
SELECT drugs.check_add_form('LIQUIDE'::text);
SELECT drugs.check_add_form('CPR'::text);

--drug_manufacture

SELECT drugs.check_add_mfg('SANOFI'::text);
SELECT drugs.check_add_mfg('WELTI'::text);
SELECT drugs.check_add_mfg('PREPARATION MAGISTRALE'::text);
SELECT drugs.check_add_mfg('3M'::text);
SELECT drugs.check_add_mfg('FRESENIUS'::text);
SELECT drugs.check_add_mfg('NESTLE HEALTH SCIENCE'::text);
SELECT drugs.check_add_mfg('3M (SCHWEIZ) GMBH'::text);
SELECT drugs.check_add_mfg('B. BRAUN MEDICAL AG'::text);
SELECT drugs.check_add_mfg('AMEDIS UE SA'::text);
SELECT drugs.check_add_mfg('PFIZER AG'::text);
SELECT drugs.check_add_mfg('DR. WILD & CO. AG'::text);
SELECT drugs.check_add_mfg('SILCIUM ESPANA'::text);
SELECT drugs.check_add_mfg('INTERDELTA SA'::text);
SELECT drugs.check_add_mfg('F. UHLMANN-EYRAUD SA'::text);
SELECT drugs.check_add_mfg('ALLERGAN AG'::text);
SELECT drugs.check_add_mfg('MEPHA'::text);
SELECT drugs.check_add_mfg('SANDOZ'::text);
SELECT drugs.check_add_mfg('VIFOR'::text);
SELECT drugs.check_add_mfg('ROCHE POSAY'::text);
SELECT drugs.check_add_mfg('GSK CONSUMER HEALTHCARE'::text);
SELECT drugs.check_add_mfg('ARKO DIFFUSION SA'::text);
SELECT drugs.check_add_mfg('ASTRAZENECA'::text);
SELECT drugs.check_add_mfg('STREULI'::text);
SELECT drugs.check_add_mfg('ZENTIVA'::text);
SELECT drugs.check_add_mfg('TEVA PHARMA AG'::text);
SELECT drugs.check_add_mfg('THEA'::text);
SELECT drugs.check_add_mfg('EUROMEDEX'::text);
SELECT drugs.check_add_mfg('BAYER'::text);
SELECT drugs.check_add_mfg('SPIRIG'::text);
SELECT drugs.check_add_mfg('DR.WILD & CO.AG'::text);
SELECT drugs.check_add_mfg('ABBOTT AG'::text);
SELECT drugs.check_add_mfg('ZAMBON'::text);
SELECT drugs.check_add_mfg('NOVO NORDISK PHARMA AG'::text);
SELECT drugs.check_add_mfg('BAUSCH'::text);
SELECT drugs.check_add_mfg('HORUS PHARMA'::text);
SELECT drugs.check_add_mfg('SMITH & NEPHEW'::text);
SELECT drugs.check_add_mfg('MOELNLYCKE HELATH CARE'::text);
SELECT drugs.check_add_mfg('BOIRON SA'::text);
SELECT drugs.check_add_mfg('RECORDATI AG'::text);
SELECT drugs.check_add_mfg('MERCK AG'::text);
SELECT drugs.check_add_mfg('MEDA PHARMA GMBH'::text);
SELECT drugs.check_add_mfg('EFFIK SA'::text);
SELECT drugs.check_add_mfg('BURGERSTEIN'::text);
SELECT drugs.check_add_mfg('ROCHE PHARMA'::text);
SELECT drugs.check_add_mfg('LEO PHARMACEUTICAL'::text);
SELECT drugs.check_add_mfg('MUNDIPHARMA MEDICAL'::text);
SELECT drugs.check_add_mfg('MUNDIPHA'::text);
SELECT drugs.check_add_mfg('MUNDIPHARM MEDICAL'::text);
SELECT drugs.check_add_mfg('UCB-PHARMA SA'::text);
SELECT drugs.check_add_mfg('IVF HARTMANN AG'::text);
SELECT drugs.check_add_mfg('ALCINA AG'::text);
SELECT drugs.check_add_mfg('SMEDICO AG'::text);
SELECT drugs.check_add_mfg('BGP PRODUCTS'::text);
SELECT drugs.check_add_mfg('SERVIER'::text);
SELECT drugs.check_add_mfg('ELI'::text);
SELECT drugs.check_add_mfg('ELI LILLY'::text);
SELECT drugs.check_add_mfg('PIERRE FAVRE'::text);
SELECT drugs.check_add_mfg('GALDERMA'::text);
SELECT drugs.check_add_mfg('PHARMA MEDICA AG'::text);
SELECT drugs.check_add_mfg('TAKEDA'::text);
SELECT drugs.check_add_mfg('SMITH & NEPHEW SCHWEIZ'::text);
SELECT drugs.check_add_mfg('FLAWA'::text);
SELECT drugs.check_add_mfg('KCI'::text);
SELECT drugs.check_add_mfg('CURADEN AG'::text);
SELECT drugs.check_add_mfg('JUVISÉ'::text);
SELECT drugs.check_add_mfg('BRISTOL'::text);
SELECT drugs.check_add_mfg('JANSSEN'::text);

--drug_strength_unit

SELECT drugs.check_add_strength_unit('MG'::text);
SELECT drugs.check_add_strength_unit('ML'::text);

--drug_schedule

SELECT drugs.check_add_sch('CI'::text);
SELECT drugs.check_add_sch('CII'::text);
SELECT drugs.check_add_sch('CIII'::text);
SELECT drugs.check_add_sch('CIV'::text);
SELECT drugs.check_add_sch('CV'::text);
SELECT drugs.check_add_sch('Rx'::text);
SELECT drugs.check_add_sch('OTC'::text);
SELECT drugs.check_add_sch('A'::text);
SELECT drugs.check_add_sch('B'::text);
SELECT drugs.check_add_sch('C'::text);
SELECT drugs.check_add_sch('D'::text);
SELECT drugs.check_add_sch('E'::text);

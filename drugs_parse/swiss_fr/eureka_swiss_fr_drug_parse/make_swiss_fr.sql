/* ******************************************************************
File    : make_swiss_fr.sql
Desc    : To initially set the Swiss_fr databse
Date    : 3 DEC 2017
Author  : Brian Broussard

    © 2016-2018 . all rights reserved
******************************************************************* */

-- system_settings
UPDATE system.system_settings SET value = 'SWISS'
    WHERE tag in ('DRUG_CODE_METHOD','DRUG_AUTHORITY');

--drugs.authority_type
UPDATE drugs.authority_types SET is_default = FALSE;
UPDATE drugs.authority_types SET is_default = TRUE WHERE tag = 'SWISS';

-- Timing
UPDATE orders.timing
    SET description = 'Dosage du matin',
        display = 'Matin',
        label_text = 'Matin'
WHERE tag = 'TIMING_AM';

UPDATE orders.timing
    SET description = 'Dosage de midi',
        display = 'Midi',
        label_text = 'Midi'
WHERE tag = 'TIMING_NOON';

UPDATE orders.timing
    SET description = 'Dosage de soir',
        display = 'Soir',
        label_text = 'Soir'
WHERE tag = 'TIMING_PM';

UPDATE orders.timing
    SET description = 'Dosage de coucher',
        display = 'Coucher',
        label_text = 'Coucher'
WHERE tag = 'TIMING_BED';


-- Packages for mediclear
UPDATE packages.package_types SET active = TRUE where tag ='PKG_TYPE_MULTI_RT_CLASSIC_CLEAR_LANDSCAPE';

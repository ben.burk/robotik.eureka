#!/bin/bash

#File	: read_eureka_swiss_drugs_csv.bash
#Desc	: Parses drug csv table dumps from Eureka into sql load routine
#Date	: 21 FEB 2019
#Author	: Benjamin Burk

out_file="eureka_swiss_fr_drugs_load.sql"
auth_csv_file="drug_tables/authority.csv"
auth_pkg_csv_file="drug_tables/authority_pkg.csv"
auth_num_entries=$(cat "$auth_csv_file" | wc -l)

#define csv columns to locate
auth_used_col=("code_prod" "drug_display")
auth_pkg_used_col=("code_pkg" "pkg_qty" "pkg_barcode")

#list of positions of columns defined above. defined dynamically. indexes match *_used_col
auth_del_pos=()
auth_pkg_del_pos=()

#list of fields to write. defined dynamically
line_fields=()

#count commas in header line. number of columns in csv file is this number, plus 1
auth_head_com_count=$(cat "$auth_csv_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | tr -cd , | wc -c)
auth_pkg_head_com_count=$(cat "$auth_pkg_csv_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | tr -cd , | wc -c)

#iterate through authority file header and find selected column locations
for (( i=1;i<$((auth_head_com_count+2));i++ )); do
	col_n="$(cat "$auth_csv_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | cut -d, -f$((i)))"
	for (( j=0;j<${#auth_used_col[@]};j++ )); do
		if [[ "$col_n" == "${auth_used_col[${j}]}" ]]; then
			auth_del_pos+=( $i )
			break
		fi
	done
done

if [ ${#auth_used_col[@]} -ne ${#auth_del_pos[@]} ]; then
	echo -e "\033[31;1mERROR! CSV file "$auth_csv_file" invalid format!\033[0m"
	exit 1
fi

#iterate through authority_pkg file header and find selected column locations
for (( i=1;i<$((auth_pkg_head_com_count+2));i++ )); do
	col_n="$(cat "$auth_pkg_csv_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | cut -d, -f$((i)))"
	for (( j=0;j<${#auth_pkg_used_col[@]};j++ )); do
		if [[ "$col_n" == "${auth_pkg_used_col[${j}]}" ]]; then
			auth_pkg_del_pos+=( $i )
			break
		fi
	done
done

if [ ${#auth_pkg_used_col[@]} -ne ${#auth_pkg_del_pos[@]} ]; then
	echo -e "\033[31;1mERROR! CSV file "$auth_pkg_csv_file" invalid format!\033[0m"
	exit 1
fi

#find code_pkg index
code_pkg_id=-1
for (( j=0;j<${#auth_pkg_used_col[@]};j++ )); do
	if [[ "${auth_pkg_used_col[${j}]}" == "code_pkg" ]]; then
		code_pkg_id=$j
		break
	fi
done
	
#find code_prod index
code_prod_id=-1
for (( k=0;k<${#auth_used_col[@]};k++ )); do
	if [[ "${auth_used_col[${k}]}" == "code_prod" ]]; then
		code_prod_id=$k
		break
	fi
done
		
if [ $code_pkg_id -lt 0 ] || [ $code_prod_id -lt 0 ]; then
	echo -e "\033[31;1mERROR! Could not find code_prod or code_pkg in CSV!\033[0m"
	exit 1
fi
		

#write header portion
echo "/*****************************************************" > $out_file
echo "File		: eureka_swiss_fr_drugs_load.sql" >> $out_file
echo "Desc		: Output from Eureka db reconstruction" >> $out_file
echo "Date		: 22 FEB 2019" >> $out_file
echo "Date Compiled 	: $(date)" >> $out_file
echo "Author		: Benjamin Burk" >> $out_file
echo "" >> $out_file
echo "*****************************************************/" >> $out_file
echo "" >> $out_file
echo "--swiss_fr_authority" >> $out_file
echo "SET session_replication_role = replica;" >> $out_file
echo "SELECT * FROM drugs.check_add_swiss_fr_drug('" >> $out_file
printf " [" >> $out_file

for (( i=2;i<$auth_num_entries;i++ )); do
	#grab current line, and grab fields
	filter_auth="$(cat "$auth_csv_file" | sed "$((i))q;d" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"

	#add auth fields
	for (( j=0;j<${#auth_del_pos[@]};j++ )); do
		line_fields+=( "$(echo "$filter_auth" | cut -d, -f${auth_del_pos[${j}]} | sed "s/'/\'\'/g" | sed 's/\"//g')" )
	done

	#add auth_pkg fields
	filter_match="$(cat "$auth_pkg_csv_file" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	m_line_no=$(echo "$filter_match" | cut -d, -f${auth_pkg_del_pos[${code_pkg_id}]} | sed "s/'/\'\'/g" | sed 's/\"//g' | grep -n "${line_fields[${code_pkg_id}]}" | cut -f1 -d:)	
	filter_auth_pkg="$(cat "$auth_pkg_csv_file" | sed "$((m_line_no))q;d" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	
	for (( m=0;m<${#auth_pkg_used_col[@]};m++ )); do
		if [[ "${auth_pkg_used_col[${m}]}" != "code_pkg" ]]; then
			#handle empty string
			if [[ "$(echo "$filter_auth_pkg" | cut -d, -f${auth_pkg_del_pos[${m}]})" == "" ]]; then
				line_fields+=( 0 )
			else
				line_fields+=( "$(echo "$filter_auth_pkg" | cut -d, -f${auth_pkg_del_pos[${m}]} | sed "s/'/\'\'/g" | sed 's/\"//g')" )
			fi
		fi
	done
	
	#loops constructing sql routine line
	ind=0
	for (( n=0;n<${#auth_used_col[@]};n++ )); do
		if [[ "${auth_used_col[${n}]}" == "code_prod" ]]; then
			if [ $i -eq 2 ]; then
				printf '{\"code_product\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			else
				printf '  {\"code_product\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			fi
			ind=$((ind+1))
		fi
		if [[ "${auth_used_col[${n}]}" == "drug_display" ]]; then
			printf '\"display_name\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			ind=$((ind+1))
		fi
	done
	for (( o=0;o<${#auth_pkg_used_col[@]};o++ )); do
		if [[ "${auth_pkg_used_col[${o}]}" == "pkg_qty" ]]; then
			printf '\"package_qty\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			ind=$((ind+1))
		fi
		if [[ "${auth_pkg_used_col[${o}]}" == "pkg_barcode" ]]; then
			if [ $i -eq 2 ] || [ $i -ne $((auth_num_entries-1)) ]; then
				if [ ${line_fields[${ind}]} == 0 ]; then
					line_fields[${ind}]=""
				fi
				printf '\"barcode\":\"%s\"},\n' "${line_fields[${ind}]}" >> $out_file
			else
				if [ ${line_fields[${ind}]} == 0 ]; then
					line_fields[${ind}]=""
				fi
				printf '\"barcode\":\"%s\"}]\n' "${line_fields[${ind}]}" >> $out_file
			fi
		fi
	done

	line_fields=()
	ind=0
done

#write end lines
echo "'::JSONB);" >> $out_file
echo "SET session_replication_role = DEFAULT;" >> $out_file
echo "SELECT * FROM drugs.init_drug_package_volume();" >> $out_file

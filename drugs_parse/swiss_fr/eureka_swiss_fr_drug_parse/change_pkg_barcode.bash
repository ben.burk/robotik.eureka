#!/bin/bash

#File	: change_pkg_barcode.bash
#Desc	: Parses drug csv table dumps from Eureka into sql load routine. Routine focuses on updating package barcode for existing drugs.
#Date	: 26 FEB 2019
#Author	: Benjamin Burk

out_file="change_pkg_barcode.sql"
auth_pkg_csv_file="drug_tables/authority_pkg.csv"
auth_pkg_num_entries=$(cat "$auth_pkg_csv_file" | wc -l)

#define csv columns to locate
auth_pkg_used_col=("code_pkg" "pkg_barcode")

#list of positions of columns defined above. defined dynamically.
auth_pkg_del_pos=()

#list of fields to write. defined dynamically
line_fields=()

#count commas in header line. number of columns in csv file is this number, plus 1
auth_pkg_head_com_count=$(cat "$auth_pkg_csv_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | tr -cd , | wc -c)

#iterate through authority_pkg file header and find selected column locations
for (( i=1;i<$((auth_pkg_head_com_count+2));i++ )); do
	col_n="$(cat "$auth_pkg_csv_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | cut -d, -f$((i)))"
	for (( j=0;j<${#auth_pkg_used_col[@]};j++ )); do
		if [[ "$col_n" == "${auth_pkg_used_col[${j}]}" ]]; then
			auth_pkg_del_pos+=( $i )
			break
		fi
	done
done

if [ ${#auth_pkg_used_col[@]} -ne ${#auth_pkg_del_pos[@]} ]; then
	echo -e "\033[31;1mERROR! CSV file "$auth_pkg_csv_file" invalid format!\033[0m"
	exit 1
fi

#find code_pkg index
code_pkg_id=-1
for (( j=0;j<${#auth_pkg_used_col[@]};j++ )); do
	if [[ "${auth_pkg_used_col[${j}]}" == "code_pkg" ]]; then
		code_pkg_id=$j
		break
	fi
done
		
if [ $code_pkg_id -lt 0 ]; then
	echo -e "\033[31;1mERROR! Could not find code_pkg in CSV!\033[0m"
	exit 1
fi

#write header portion
echo "/*****************************************************" > $out_file
echo "File		: change_pkg_barcode.sql" >> $out_file
echo "Desc		: Output from Eureka db reconstruction. Focuses on updating package barcode for existing drugs" >> $out_file
echo "Date		: 26 FEB 2019" >> $out_file
echo "Date Compiled 	: $(date)" >> $out_file
echo "Author		: Benjamin Burk" >> $out_file
echo "" >> $out_file
echo "*****************************************************/" >> $out_file
echo "" >> $out_file		
echo "--authority_pkg.pkg_barcode" >> $out_file
echo "" >> $out_file

for (( i=2;i<$auth_pkg_num_entries;i++ )); do

	#add auth_pkg fields
	filter_="$(cat "$auth_pkg_csv_file" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	filter_auth_pkg="$(cat "$auth_pkg_csv_file" | sed "$((i))q;d" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	
	for (( m=0;m<${#auth_pkg_used_col[@]};m++ )); do
		line_fields+=( "$(echo "$filter_auth_pkg" | cut -d, -f${auth_pkg_del_pos[${m}]} | sed "s/'/\'\'/g" | sed 's/\"//g')" )
	done
	
	if [ ${#line_fields[1]} -gt 1 ] && [ ${#line_fields[0]} -gt 1 ]; then
		echo "UPDATE authority_pkg SET pkg_barcode = '${line_fields[1]}' WHERE code_pkg = '${line_fields[0]}';" >> $out_file	
	fi
	
	line_fields=()
done

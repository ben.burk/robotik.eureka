Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

eureka_swiss_fr_drug_parse

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS
----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------


Creates a .sql routine compliant with Eureka DB to load drugs. Uses Eureka table dumps (as CSV) as input.
Executing the 'read_eureka_swiss_fr_csv.bash' routine may take nearly 1 hour to complete, depending on table dump size.

Ideally, this should be used to reconstruct a drug db from a working site's CSV table dumps.
These routines utilize a SQL function, check_add_swiss_fr_drug, which will handle error checking (duplicate, bad input, etc), as well as creating unique indices

Procedure:

1. Execute 'bash make_swiss_fr.bash'. Sets up db for SWISS drugs
2. Execute 'bash read_eureka_swiss_fr_csv.bash', if 'eureka_swiss_fr_drugs_load.sql' needs to be remade.
3. Execute 'bash load_drug.bash'. Executes 'eureka_swiss_fr_drugs_load.sql' routine

(optional)
4. Execute 'bash load_character.bash'. Populates drug characteristics tables with template data


table dumps are inside of 'lausanne_drug_tables21.02.19.zip'


(optional, update scripts)
Execute 'bash change_pkg_qty.bash', if 'change_pkg_qty.sql' needs to be remade.
Execute 'bash update_qty.bash'. Executes 'change_pkg_qty.sql' routine. Updates package quantity values for included drugs by code_pkg
Execute 'bash change_pkg_barcode.bash', if 'change_pkg_barcode.sql' needs to be remade.
Execute 'bash update_barcode.bash'. Executes 'change_pkg_barcode.sql' routine. Updates package barcode values for included drugs by code_pkg



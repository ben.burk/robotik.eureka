Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019


	aemps_drug_parse:		SPANISH DRUG DATA ROUTINES


Please read the 'README.md' files in each subdirectory

**NOTE**
Any .sql routines included in this archive have been generated ahead of time. All files, other than 'eureka_aemps_patch.sql' and 'aemps_character.sql', contain a timestamp of when they were created.

Ensure the information that will be changed is what is desired before running these.



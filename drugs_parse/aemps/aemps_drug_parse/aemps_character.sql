/*****************************************************
File	: cima_character.sql
Desc	: Populates cima drug tables with working template data
Date	: 11 MAR 2019
Author	: Benjamin Burk

*****************************************************/

--drug_color

SELECT drugs.check_add_color('MARRÓN'::text);
SELECT drugs.check_add_color('BLANCO'::text);
SELECT drugs.check_add_color('TRANSPARENTE'::text);
SELECT drugs.check_add_color('ROJO'::text);
SELECT drugs.check_add_color('AMARILLO'::text);
SELECT drugs.check_add_color('AMARILLO / BLANCO'::text);
SELECT drugs.check_add_color('AZUL'::text);
SELECT drugs.check_add_color('ROSADO'::text);
SELECT drugs.check_add_color('AZUL / BLANCO'::text);
SELECT drugs.check_add_color('AZUL PÁLIDO'::text);
SELECT drugs.check_add_color('ROSADO / BLANCO'::text);
SELECT drugs.check_add_color('BEIGE'::text);
SELECT drugs.check_add_color('AMARILLO / NARANJA'::text);
SELECT drugs.check_add_color('NARANJA'::text);

--drug_shape

SELECT drugs.check_add_shape('CÁPSULA'::text);
SELECT drugs.check_add_shape('PÍLDORA'::text);
SELECT drugs.check_add_shape('CREMA'::text);
SELECT drugs.check_add_shape('LÍQUIDO'::text);
SELECT drugs.check_add_shape('POUDRE'::text);
SELECT drugs.check_add_shape('PARCHE'::text);
SELECT drugs.check_add_shape('COMPRESAS'::text);
SELECT drugs.check_add_shape('VESTIDOR'::text);
SELECT drugs.check_add_shape('GOTAS'::text);
SELECT drugs.check_add_shape('INY. DE SOLUCION'::text);
SELECT drugs.check_add_shape('SUSPENSIÓN'::text);

--drug_form

SELECT drugs.check_add_form('CREMA'::text);
SELECT drugs.check_add_form('LÍQUIDO'::text);
SELECT drugs.check_add_form('PÍLDORA'::text);

--drug_manufacture

--drug_strength_unit

SELECT drugs.check_add_strength_unit('MG'::text);
SELECT drugs.check_add_strength_unit('ML'::text);

--drug_schedule
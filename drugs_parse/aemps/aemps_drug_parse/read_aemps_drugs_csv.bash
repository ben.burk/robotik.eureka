#!/bin/bash

#File	: read_aemps_drugs_csv.bash
#Desc	: Parses drug csv from CIMA into sql load routine
#Date	: 10 MAR 2019
#Author	: Benjamin Burk

out_file="eureka_aemps_drugs_load.sql"
csv1_file="cima-medicamentos_1.csv"
csv2_file="cima-medicamentos_2.csv"
csv1_num_entries=$(cat "$csv1_file" | wc -l)

#define csv columns to locate
csv1_used_col=("NUMERO DE REGISTRO" "MEDICAMENTO" "LABORATORIO TITULAR")
csv2_used_col=("NUMERO DE REGISTRO")

#list of positions of columns defined above. defined dynamically. indexes match *_used_col
csv1_del_pos=()
csv2_del_pos=()

#list of fields to write. defined dynamically
line_fields=()

#count commas in header line. number of columns in csv file is this number, plus 1
csv1_head_com_count=$(cat "$csv1_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | tr -cd ^ | wc -c)
csv2_head_com_count=$(cat "$csv2_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | tr -cd ^ | wc -c)

#iterate through authority file header and find selected column locations
for (( i=1;i<$((csv1_head_com_count+2));i++ )); do
	col_n="$(cat "$csv1_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | cut -d^ -f$((i)))"
	for (( j=0;j<${#csv1_used_col[@]};j++ )); do
		if [[ "$col_n" == "${csv1_used_col[${j}]}" ]]; then
			csv1_del_pos+=( $i )
			break
		fi
	done
done

if [ ${#csv1_used_col[@]} -ne ${#csv1_del_pos[@]} ]; then
	echo -e "\033[31;1mERROR! CSV file "$csv1_file" invalid format!\033[0m"
	exit 1
fi

#iterate through authority_pkg file header and find selected column locations
for (( i=1;i<$((csv2_head_com_count+2));i++ )); do
	col_n="$(cat "$csv2_file" | sed '1q;d' | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/' | cut -d^ -f$((i)))"
	for (( j=0;j<${#csv2_used_col[@]};j++ )); do
		if [[ "$col_n" == "${csv2_used_col[${j}]}" ]]; then
			csv2_del_pos+=( $i )
			break
		fi
	done
done

if [ ${#csv2_used_col[@]} -ne ${#csv2_del_pos[@]} ]; then
	echo -e "\033[31;1mERROR! CSV file "$csv2_file" invalid format!\033[0m"
	exit 1
fi

#find code_pkg index
nat_id_1=-1
for (( j=0;j<${#csv2_used_col[@]};j++ )); do
	if [[ "${csv2_used_col[${j}]}" == "NUMERO DE REGISTRO" ]]; then
		nat_id_1=$j
		break
	fi
done
	
#find code_prod index
nat_id_2=-1
for (( k=0;k<${#csv1_used_col[@]};k++ )); do
	if [[ "${csv1_used_col[${k}]}" == "NUMERO DE REGISTRO" ]]; then
		nat_id_2=$k
		break
	fi
done
		
if [ $nat_id_1 -lt 0 ] || [ $nat_id_2 -lt 0 ]; then
	echo -e "\033[31;1mERROR! Could not find NUMERO DE REGISTRO in CSV!\033[0m"
	exit 1
fi
		

#write header portion
echo "/*****************************************************" > $out_file
echo "File		: eureka_cima_drugs_load.sql" >> $out_file
echo "Desc		: Output from drug csv integration with CIMA" >> $out_file
echo "Date		: 10 MAR 2019" >> $out_file
echo "Date Compiled 	: $(date)" >> $out_file
echo "Author		: Benjamin Burk" >> $out_file
echo "" >> $out_file
echo "*****************************************************/" >> $out_file
echo "" >> $out_file
echo "--aemps_authority" >> $out_file
echo "SET session_replication_role = replica;" >> $out_file
echo "SELECT * FROM drugs.check_add_aemps_drug('" >> $out_file
printf " [" >> $out_file

for (( i=2;i<$csv1_num_entries;i++ )); do
	#grab current line, and grab fields
	filter_csv1="$(cat "$csv1_file" | sed "$((i))q;d" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"

	#add auth fields
	for (( j=0;j<${#csv1_del_pos[@]};j++ )); do
		line_fields+=( "$(echo "$filter_csv1" | cut -d^ -f${csv1_del_pos[${j}]} | sed "s/'/\'\'/g" | sed 's/\"//g')" )
	done

	#add auth_pkg fields
	filter_match="$(cat "$csv2_file" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	m_line_no=$(echo "$filter_match" | cut -d^ -f${csv2_del_pos[${nat_id_1}]} | sed "s/'/\'\'/g" | sed 's/\"//g' | grep -n "${line_fields[${nat_id_1}]}" | cut -f1 -d:)	
	filter_csv2="$(cat "$csv2_file" | sed "$((m_line_no))q;d" | sed 's/\(.*\".*\),\(.*\".*\)/\1\2/')"
	
	for (( m=0;m<${#csv2_used_col[@]};m++ )); do
		if [[ "${csv2_used_col[${m}]}" != "NUMERO DE REGISTRO" ]]; then
			#handle empty string
			if [[ "$(echo "$filter_csv2" | cut -d^ -f${csv2_del_pos[${m}]})" == "" ]]; then
				line_fields+=( 0 )
			else
				line_fields+=( "$(echo "$filter_csv2" | cut -d^ -f${csv2_del_pos[${m}]} | sed "s/'/\'\'/g" | sed 's/\"//g')" )
			fi
		fi
	done
	
	#loops constructing sql routine line
	ind=0
	for (( n=0;n<${#csv1_used_col[@]};n++ )); do
		if [[ "${csv1_used_col[${n}]}" == "NUMERO DE REGISTRO" ]]; then
			if [ $i -eq 2 ]; then
				printf '{\"code_product\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			else
				printf '  {\"code_product\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			fi
			ind=$((ind+1))
		fi
		if [[ "${csv1_used_col[${n}]}" == "MEDICAMENTO" ]]; then
			printf '\"display_name\":\"%s\",' "${line_fields[${ind}]}" >> $out_file
			ind=$((ind+1))
		fi
		if [[ "${csv1_used_col[${n}]}" == "LABORATORIO TITULAR" ]]; then
			printf '\"mfg_name\":\"%s\"' "${line_fields[${ind}]}" >> $out_file
			ind=$((ind+1))
		fi			

		#printf ',' >> $out_file

	done
	for (( o=0;o<${#csv2_used_col[@]};o++ )); do
		if [[ "${csv2_used_col[${o}]}" == "DOSIS" ]]; then
			if [ $i -eq 2 ] || [ $i -ne $((csv1_num_entries-1)) ]; then
				printf '\"package_qty\":\"%s\"' "${line_fields[${ind}]}" >> $out_file
			else
				printf '\"package_qty\":\"%s\"' "${line_fields[${ind}]}" >> $out_file
			fi

			ind=$((ind+1))
		fi
		#if [[ "${csv2_used_col[${o}]}" == "pkg_barcode" ]]; then
			#if [ $i -eq 2 ] || [ $i -ne $((csv1_num_entries-1)) ]; then
				#if [ ${line_fields[${ind}]} == 0 ]; then
					#line_fields[${ind}]=""
				#fi
				#printf '\"barcode\":\"%s\"},\n' "${line_fields[${ind}]}" >> $out_file
			#else
				#if [ ${line_fields[${ind}]} == 0 ]; then
					#line_fields[${ind}]=""
				#fi
				#printf '\"barcode\":\"%s\"}]\n' "${line_fields[${ind}]}" >> $out_file
			#fi
		#fi

		#printf ',' >> $out_file
	done

	if [ $i -eq 2 ] || [ $i -ne $((csv1_num_entries-1)) ]; then
		printf '},\n' >> $out_file
	else
		printf '}]\n' >> $out_file
	fi

	line_fields=()
	ind=0
done

#write end lines
echo "'::JSONB);" >> $out_file
echo "SET session_replication_role = DEFAULT;" >> $out_file
echo "SELECT * FROM drugs.init_drug_package_volume();" >> $out_file

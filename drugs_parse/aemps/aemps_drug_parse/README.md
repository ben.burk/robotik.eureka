Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

aemps_drug_parse

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

This project should only be used as needed. Currently supports "code_product", "display_name", "mfg_name", "package_qty" fields for Eureka DB.
Creates a .sql routine compliant with Eureka DB to load drugs. Takes 2 CSV files as input.

Ideally, this should be used to load more drugs after the initial set.
These routines utilize a SQL function, check_add_aemps_drug, which will handle error checking (duplicate, bad input, etc), as well as creating unique indices

Procedure:

1. Execute 'bash read_aemps_drugs_csv.bash', if eureka_aemps_drugs_load.sql needs to be remade. 'eureka_aemps_drugs_load.sql' file contains 4 fields as of 03.11.2019.
2. Execute 'bash load_aemps_patch_drug.bash'. Executes eureka_aemps_patch.sql and eureka_aemps_drugs_load.sql routines

(optional)
4. Execute 'bash load_character.bash'. Populates drug characteristics tables with template data


**cima-medicamentos_*.csv are individual sheets of 'cima-medicamentos.xlsx'
**I use the character '^' to delimit columns in each CSV file.



Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019


	mksquashfs.sh:		prepares a squashfs of DEBIAN LIVE, from chroot directory (needs to be tweaked before initial use).
	mkiso.sh:		prepares a new iso9660 file. uses stub BIOS/EFI bootloader, writes grub menu from timestamp, etc).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------

squashfs location:	image/live/filesystem.squashfs

image/initrd:		initial ramdisk for debian bootstrap

image/vmlinuz:		kernel for debian bootstrap

image/DEBIAN_CUSTOM:	filesystem anchor (used if future development to boot from prepared USB).


source : https://willhaley.com/blog/custom-debian-live-environment/
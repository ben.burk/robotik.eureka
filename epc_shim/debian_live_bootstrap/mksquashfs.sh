#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root..."
	exit 1
fi

mkdir -p image/live

DEV_CHROOT="~/shim"
mksquashfs "$DEV_CHROOT" image/live/filesystem.squashfs -e boot

Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019


	scratch/:		subdirectory of all grub bootloader and template files

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

'grub.cfg.iso.template' is a template file, modified by '../mkiso.sh'

'grub.cfg.iso.template' is moved to 'scratch/grub.cfg' each time '../mkiso.sh' is called

DATE in format YMDHM, YEAR,MONTH,DAY,HOUR,MINUTE is inserted into 'grub.cfg' as the label to search for in grub. This is later used to set the filename and label of the iso9660 filesystem. After creation of iso9660 filesystem, the script '../mkiso.sh' performs cleanup, removing the grub.cfg file copied and modified from the template.

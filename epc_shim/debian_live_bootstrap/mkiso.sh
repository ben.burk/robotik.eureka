#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root..."
	exit 1
fi

mkdir -p isos

DATE=`date '+%y%m%d%H%M'`
cp scratch/grub.cfg.iso.template scratch/grub.cfg
sed -i "s/XXXX/EPC_X86_64_CENTOS_${DATE}/" scratch/grub.cfg
sed -i "s/YYYY/EPC CENTOS Setup v. ${DATE}/" scratch/grub.cfg

echo "COMPILATION OF LINUX (LIVE) custom..."
echo "Are you sure you want to proceed?... (bootloader files will be recreated)"
read -r -p "Continue (y/n)" c
if [[ $c =~ ^[Yy] ]] || [[ -z $c ]]; then
	#begin
	grub-mkstandalone --format=x86_64-efi \
		--output=scratch/bootx64.efi \
		--locales="" \
		--fonts="" \
		"boot/grub/grub.cfg=scratch/grub.cfg"

	(cd scratch && \
		dd if=/dev/zero of=efiboot.img bs=1M count=10 && \
		mkfs.vfat efiboot.img && \
		mmd -i efiboot.img efi efi/boot && \
		mcopy -i efiboot.img ./bootx64.efi ::efi/boot/
	)

	grub-mkstandalone --format=i386-pc \
		--output=scratch/core.img \
		--install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
		--modules="linux normal iso9660 biosdisk search" \
		--locales="" \
		--fonts="" \
		"boot/grub/grub.cfg=scratch/grub.cfg"

	cat /usr/lib/grub/i386-pc/cdboot.img \
		scratch/core.img \
		> scratch/bios.img

	xorriso -as mkisofs -iso-level 3 -full-iso9660-filenames \
		-volid "EPC_X86_64_CENTOS_${DATE}" \
		-eltorito-boot \
		boot/grub/bios.img \
		-no-emul-boot \
		-boot-load-size 4 \
		-boot-info-table \
		--eltorito-catalog boot/grub/boot.cat \
		--grub2-boot-info \
		--grub2-mbr /usr/lib/grub/i386-pc/boot_hybrid.img \
		-eltorito-alt-boot \
		-e EFI/efiboot.img \
		-no-emul-boot \
		-append_partition 2 0xef scratch/efiboot.img \
		-output "isos/epc_x86_64_centos_${DATE}.iso" \
		-graft-points \
		image \
		/boot/grub/bios.img=scratch/bios.img \
		/EFI/efiboot.img=scratch/efiboot.img

		rm -f scratch/grub.cfg
else
	echo "Exiting..."
	exit 0
fi


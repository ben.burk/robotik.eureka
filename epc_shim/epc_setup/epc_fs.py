
#author : Benjamin Burk
#details : EPC Setup 'epc_fs.py' routine. MOUNTS CREATED FILESYSTEMS, DUMPS FS TARBALL, CALLS CHROOT ENTRY SCRIPT
#date : Updated 13 MAR 2019
#copyright : BURKTECH 2019

import sys
import json
import os
import re
import shlex
import string
import shutil
from subprocess import Popen, PIPE, STDOUT

try:
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

#tarfile, string name of tarball to dump. full path
#db_dict, dictionary of all pertinent information for disks and successfully configured mdarrays
def deploy_tarball(tarfile, db_dict):

    #PRELIMINARY ERROR CHECK FOR TARFILE
    if not os.path.isfile(tarfile):
        print("\033[31;1mERROR! {0} : file does not exist\033[0m".format(tarfile))

    #CONSTANTS FOR FS MOUNT HANDLES. IF CHANGING THESE, BE CAREFUL TO READ THE BELOW PROCEDURES
    #THESE PROCEDURES DO NOT SUPPORT SEPARATING MOUNTPOINTS FROM THE SAME TREE, i.e. root ---> '/mnt', esp ---> '/media'. AN ERROR WILL BE THROWN IF THIS IS ATTEMPTED
    #IF THESE FS HANDLES ARE CHANGED, DO NOT ADD A TRAILING FORWARD SLASH
    efi_mnt = "/mnt/boot/efi"
    boot_mnt = "/mnt/boot"
    root_mnt = "/mnt"

    #PRELIMINARY ERROR CHECKING. THROWS ERROR IF ROOT_MNT or EFI_MNT BEING USED
    #NO NEED TO ADD CHECK FOR BOOT_MNT, AS ALL IMMEDIATE SUBDIRECTORIES OF ROOT_MNT ARE CHECKED FOR MOUNT POINT STATUS.
    if os.path.isdir(root_mnt):
        #quick check to see if root_mnt handle is being used, if so exit with error
        if os.path.ismount(root_mnt):
            print("\033[31;1mERROR! {0} : directory is being used as a mountpoint\033[0m".format(root_mnt))
            sys.exit(1)
        #quick check to see if root_mnt subdirectories are mounted, if so exit with error. will check direct child directories of efi_mnt
        for x in [d for d in os.listdir(root_mnt) if os.path.isdir(os.path.join(root_mnt, d))]:
            if os.path.ismount(x):
                print("\033[31;1mERROR! {0} : directory is being used as a mountpoint\033[0m".format(x))
                sys.exit(1)
    if os.path.isdir(efi_mnt):
        #quick check to see if efi_mnt handle is being used, if so exit with error. will check direct child directories of efi_mnt
        if os.path.ismount(efi_mnt):
            print("\033[31;1mERROR! {0} : directory is being used as a mountpoint\033[0m".format(efi_mnt))
            sys.exit(1)
        #quick check to see if efi_mnt subdirectories are mounted, if so exit with error
        for x in [d for d in os.listdir(efi_mnt) if os.path.isdir(os.path.join(efi_mnt, d))]:
            if os.path.ismount(x):
                print("\033[31;1mERROR! {0} : directory is being used as a mountpoint\033[0m".format(x))
                sys.exit(1)

    #CREATION OF FS HANDLES, IF THEY DO NOT EXIST. EFI_MNT IS DEEPER INTO THE /MNT TREE, SO USE 'MKDIR -P' ON THE CHILD DIRECTORY. WILL CREATE ALL PARENT DIRECTORIES AS WELL
    if not os.path.isdir(efi_mnt):
        proc = Popen(["mkdir", "-p", efi_mnt], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
        mkd_o, mkd_er = proc.communicate()
        #wait for return before continue
        while proc.returncode is None:
            proc.poll()

        #checking return code for spawned process, if error, print customized error message
        if proc.returncode == 1:
            print("\033[31;1mERROR! {0}\033[0m".format(mkd_o.decode('utf-8')))
            if os.geteuid() != 0:
                print("\033[31;1mERROR! mkdir needs root privileges!\033[0m")
                sys.exit(1)
            sys.exit(1)

    print("CONFIGURING FS HANDLES...")
    #PREPARATION OF FS HANDLES, CONFIGURING ROOT_MNT, EFI_MNT. RECURSES THROUGH TWO LAYERS FROM /MNT. PERFORMS CHECKS TO ENSURE EFI_MNT IS AT THE BOTTOM OF THIS MINI-TREE. EXITS IF NOT
    if os.path.join(root_mnt, efi_mnt.rsplit('/', 2)[1], efi_mnt.rsplit('/', 1)[1]) == efi_mnt:                    #efi_mnt is a subdirectory of root_mnt
        for item in os.listdir(root_mnt):
            for item2 in os.listdir(os.path.join(root_mnt, item)):                                                 #boot subdirectory will remain, but items will be wiped
                if os.path.isdir(os.path.join(os.path.join(root_mnt, item), item2)) and os.path.join(os.path.join(root_mnt, item), item2) != efi_mnt and item != "boot":    #found directory to remove
                    shutil.rmtree(os.path.join(os.path.join(root_mnt, item), item2), ignore_errors=True)
                if os.path.isfile(os.path.join(os.path.join(root_mnt, item), item2)):                              #found file to remove
                    os.remove(os.path.join(os.path.join(root_mnt, item), item2))
            if os.path.isdir(os.path.join(root_mnt, item)):
                    shutil.rmtree(os.path.join(root_mnt, item), ignore_errors=True)
            if os.path.isfile(os.path.join(root_mnt, item)):
                    os.remove(os.path.join(root_mnt, item))
    else:
        print("\033[31;1mERROR! {0} is not a child directory of {1}. exiting...\033[0m".format(efi_mnt, root_mnt))
        sys.exit(1)

    #MOUNT FS

    print("MOUNTING FS...")
    #ITERATE THROUGH TO FIND ROOTFS. NEEDS TO BE MOUNTED FIRST, TO ALLOW NESTED LOCATIONS TO BE MOUNTED. i.e. root ---> /mnt, boot ---> /mnt/boot, esp/efi ---> /mnt/boot/efi
    for k0 in db_dict["MDARRAY"].keys():
        if db_dict["MDARRAY"][k0]["NAME"] == "ROOT" and not os.path.ismount(root_mnt):
            proc = Popen(["mount", "-t", "xfs", k0, root_mnt], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            mnt_o, mnt_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:        #multiple error codes possible
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mnt_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mount needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
            print("\033[32;1mrootfs {0} mounted on {1}\033[0m".format(k0, root_mnt))
            #CREATION OF 'boot' SUBDIRECTORY IN ROOTFS. IF THE SUBDIRECTORY EXISTS, NO ERROR WILL BE THROWN. IF NOT, ESP WILL NOT MOUNT LATER
            proc = Popen(["mkdir", boot_mnt], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
            mkd_o, mkd_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error, print customized error message
            if proc.returncode == 1:
                print("\033[31;1mERROR! {0}\033[0m".format(mkd_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mkdir needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
    
    for k1 in db_dict["DISK"].keys():
        for k2 in db_dict["DISK"][k1]["PARTITION"].keys():
            if db_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "ROOT" and not os.path.ismount(root_mnt):
                proc = Popen(["mount", "-t", "xfs", k2, root_mnt], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                mnt_o, mnt_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error, print customized error message
                if proc.returncode != 0:        #multiple error codes possible
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mnt_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mount needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
                print("\033[32;1mrootfs {0} mounted on {1}\033[0m".format(k2, root_mnt))
                #CREATION OF 'boot' SUBDIRECTORY IN ROOTFS. IF THE SUBDIRECTORY EXISTS, NO ERROR WILL BE THROWN. IF NOT, ESP WILL NOT MOUNT LATER
                proc = Popen(["mkdir", boot_mnt], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
                mkd_o, mkd_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error, print customized error message
                if proc.returncode == 1:
                    print("\033[31;1mERROR! {0}\033[0m".format(mkd_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mkdir needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
    
    #ITERATE THROUGH TO FIND BOOTFS. NEEDS TO BE MOUNTED AFTER ROOTFS
    for k0 in db_dict["MDARRAY"].keys():
        if db_dict["MDARRAY"][k0]["NAME"] == "BOOT" and not os.path.ismount(boot_mnt):
            proc = Popen(["mount", "-t", "xfs", k0, boot_mnt], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            mnt_o, mnt_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error print customized error message
            if proc.returncode != 0:
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mnt_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mount needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
            print("\033[32;1mbootfs {0} mounted on {1}\033[0m".format(k0, boot_mnt))
            #CREATION of 'efi' SUBDIRECTORY IN BOOTFS. IF THE SUBDIRECTORY EXISTS, NO ERROR WILL BE THROWN, IF NOT, ESP WILL NOT MOUNT LATER
            proc = Popen(["mkdir", efi_mnt], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
            mkd_o, mkd_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error, print customized error message
            if proc.returncode == 1:
                print("\033[31;1mERROR! {0}\033[0m".format(mkd_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mkdir needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)

    for k1 in db_dict["DISK"].keys():
        for k2 in db_dict["DISK"][k1]["PARTITION"].keys():
            if db_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "BOOT" and not os.path.ismount(boot_mnt):
                proc = Popen(["mount", "-t", "xfs", k2, boot_mnt], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                mnt_o, mnt_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error print customized error message
                if proc.returncode != 0:
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mnt_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mount needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
                print("\033[32;1mbootfs {0} mounted on {1}\033[0m".format(k2, boot_mnt))
                #CREATION of 'efi' SUBDIRECTORY IN BOOTFS. IF THE SUBDIRECTORY EXISTS, NO ERROR WILL BE THROWN. IF NOT, ESP WILL NOT MOUNT LATER
                proc = Popen(["mkdir", efi_mnt], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
                mkd_o, mkd_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error, print customized error message
                if proc.returncode == 1:
                    print("\033[31;1mERROR! {0}\033[0m".format(mkd_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mkdir needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)

    #PROCESSING ESP/EFI PARTITIONS, MOUNTING DEPENDING ON CONFIGURATION
    for k0 in db_dict["MDARRAY"].keys():
        if (db_dict["MDARRAY"][k0]["NAME"] == "ESP" or db_dict["MDARRAY"][k0]["NAME"] == "EFI") and not os.path.ismount(efi_mnt):
            proc = Popen(["mount", "-t", "vfat", k0, efi_mnt], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            mnt_o, mnt_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error print customized error message
            if proc.returncode != 0:        #multiple error codes possible
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mnt_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mount needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
            print("\033[32;1mespfs  {0} mounted on {1}\033[0m".format(k0, efi_mnt))

    for k1 in db_dict["DISK"].keys():
        for k2 in db_dict["DISK"][k1]["PARTITION"].keys():
            if (db_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "ESP" or db_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "EFI") and not os.path.ismount(efi_mnt):
                proc = Popen(["mount", "-t", "vfat", k2, efi_mnt], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                mnt_o, mnt_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error print customized error message
                if proc.returncode != 0:        #multiple error codes possible
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mnt_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mount needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
                print("\033[32;1mespfs  {0} mounted on {1}\033[0m".format(k2, efi_mnt))

    #SECOND CHECK TO VERIFY FS HANDLES ARE MOUNTED
    if not os.path.ismount(root_mnt):
        print("\033[31;1mERROR! {0} not mounted!\033[0m".format(root_mnt))
        sys.exit(1)
    if not os.path.ismount(boot_mnt):
        print("\033[31;1mERROR! {0} not mounted!\033[0m".format(boot_mnt))
        sys.exit(1)
    if not os.path.ismount(efi_mnt):
        print("\033[31;1mERROR! {0} not mounted!\033[0m".format(efi_mnt))
        sys.exit(1)

    #DUMP TARBALLS
    print("DUMPING TARBALL...")
    proc = Popen(["tar", "-xzpf", tarfile, "-C", root_mnt], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
    tar_o, tar_er = proc.communicate()
    #wait for return before continue
    while proc.returncode is None:
        proc.poll()

    #checking return code for spawned process, if error, print customized error message
    if proc.returncode == 1:
        print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, tar_o.decode('utf-8')))
        if os.geteuid() != 0:
            print("\033[31;1mERROR! tar needs root privileges!\033[0m")
            sys.exit(1)
        sys.exit(1)
    print("\033[32;1mtarball {0} deployed\033[0m".format(tarfile))

    #serialization for finalization command
    swp_added = 0
    c_args = "bash /root/epc_setup/enter_chroot.bash "
    for k in db_dict["MDARRAY"].keys():
        if db_dict["MDARRAY"][k]["NAME"] == "SWAP":
            swp_added = 1
        c_args+="{0} {1} ".format(db_dict["MDARRAY"][k]["NAME"].lower(), k)				#adding mdarray to argument. i.e. /dev/mdX
    for l in db_dict["DISK"].keys():
        for m in db_dict["DISK"][l]["PARTITION"].keys():
            if swp_added != 1 and db_dict["DISK"][l]["PARTITION"][m]["NAME"] == "SWAP" and db_dict["INFO"]["SWAP_EN"] == m:
                c_args+="{0} {1} ".format(db_dict["DISK"][l]["PARTITION"][m]["NAME"].lower(), m)	#adding swap device partition (not mdarray) to argument
            for n in range(0, len(db_dict["INFO"]["IND_PLIST"])):
                if m == db_dict["INFO"]["IND_PLIST"][n]:
                    c_args+="{0} {1} ".format(db_dict["DISK"][l]["PARTITION"][m]["NAME"].lower(), m)	#adding other relevant partitions to argument

    c_args = c_args.rstrip()	#removal of trailing whitespace
    #END OF PYTHON ROUTINE. CHROOT SHELL SCRIPT IS CALLED HERE, CONFIGURING FSTAB, PERSISTENT ACTIVATION OF SWAP, AND GRUB INSTALLATION
    os.execvp("bash", shlex.split(c_args))

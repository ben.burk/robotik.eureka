Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019


	chroot.bash:			chroot script. writes fstab, mdadm, configures swap, installs grub, rebuilds all kernels.
	enter_chroot.bash:		chroot prep script. catalogs all created filesystems to db, builds mdadm config, sets up and enters chroot.
	epc_entry_point.bash:		entry point for setup. a premade alias for the word 'install' triggers this script. stops all mdarrays and enters setup.
	epc_setup.py:			shows setup menu, handles error checking for input, prevents erroneous raid, calls disk and fs routines.
	epc_disk.py:			wipes all file signatures, partitions drives, handles fake raid, formats devices, configures and enables swap, writes info to json db.
	epc_fs.py:			mounts created filesystems, dumps fs tarball, calls chroot entry script.
	config.json:			settings file for installation. includes ability to set filesystem sizes (in MiB) and specify raid type for partition.	

	epc_splash, robotik_logo, warning, warning_2:		setup splash screens



	*room was left to include installation options for other software suites in config.json. i.e:		EPC	--->	OCULUS	--->	EPC_OCULUS_RAID1_ALL

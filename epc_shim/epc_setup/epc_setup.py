
#author : Benjamin Burk
#details : EPC Setup 'epc_setup.py' (main) routine. SHOWS MENU, HANDLES ERROR CHECKING FOR INPUT, INCLUDING ERRONEOUS RAID. 
#date : Updated 13 MAR 2019
#copyright : BURKTECH 2019

import sys
import json
import os
import re
import string
import copy
from epc_disk import setup_disks
from epc_fs import deploy_tarball
import pyudev

if os.name == 'nt':
    print("\033[31;1mERROR! EPC Setup not compatible with WINDOWS NT!\033[0m")
    sys.exit(1)

if not os.path.isdir("/sys/firmware/efi"):
    print("\033[31;1mERROR! EPC Setup will not install under BIOS. BIOS IS DEPRECATED. Please boot with UEFI (EFI)!\033[0m")
    sys.exit(1)

#prints verify dialogue based on list 'input', returning user input in true or false
def verif_dialogue(input_l):
    if len(input_l) == 0 or input_l[0] == '':
        print("\033[33;1mNO OPTIONS SELECTED!\033[0m")
        return 0
    input_l = sorted(input_l)
    for i in range(0, len(input_l)):
        print("\033[33;1mOPTION : " + input_l[i] + "\033[0m")

    vc = input("Correct? [Y/n] : ")
    if (vc == "y") or (vc == "yes") or (vc == ""):
        return 1
    else:
        return 0

#prints dictionary in custom format, returning list of child dictionary keys
#method used to print settings in 'config.json'
def o_menu_print(data):
    child_dict_list = []
    for i, k0 in enumerate(sorted(data.keys())):
        if isinstance(data[k0], dict):
            child_dict_list.append(k0)
            print("{0}:\t{1}".format(i+1, k0))
    return child_dict_list

#prints dictionary in custom format, returning list of child dictionary keys
#method used to print found drives
def drive_print(data):
    drive_list = []
    ind = 1
    for k, v in sorted(data.items()):
        if isinstance(v, dict):
            drive_list.append(k)
            print('\n')
            print("{0}: {1}  ".format(ind, k))
            ind+=1
            drive_print(v)
        else:
            print("\t{0} : {1}".format(k, v))

    return drive_list

with open('config.json') as f:
    db_data = json.load(f)
f.close()

with open('epc_splash') as h:
    print(h.read())
h.close()

#PYUDEV HOOK. GRAB LIST OF PHYSICAL DRIVES BY ID_SERIAL
#USING PYUDEV AND SYSFS TO LIST INFORMATION OF DISKS
drive_dict = {}                                                 #DEVICE DICTIONARY (MASTER). FILTERS OUT DM, LVM, LOOP, AND NETWORK BLOCK DEVICES
context = pyudev.Context()
for device in context.list_devices(subsystem='block', DEVTYPE='disk'):
    if device.get('MAJOR') == "8":      #currently only supports scsi disks, either physical or virtual. physical raid device support should be added
        drive_dict[device.get('DEVNAME')] = {}      #ID_SERIAL cannot be used. the device may be virtual and have no serial
        drive_dict[device.get('DEVNAME')]['MODEL'] = device.get('ID_MODEL')
        drive_dict[device.get('DEVNAME')]['L_SECTOR_COUNT'] = int(open('/sys/block/'+device.get('DEVNAME').replace("/dev/","")+'/size'.format(**locals())).read())
        drive_dict[device.get('DEVNAME')]['P_SECTOR_SIZE'] = int(open('/sys/block/'+device.get('DEVNAME').replace("/dev/","")+'/queue/physical_block_size'.format(**locals())).read())
        drive_dict[device.get('DEVNAME')]['L_SECTOR_SIZE'] = int(open('/sys/block/'+device.get('DEVNAME').replace("/dev/", "") + '/queue/logical_block_size'.format(**locals())).read())
        drive_dict[device.get('DEVNAME')]['GIBI_SIZE'] = str(int(((drive_dict[device.get('DEVNAME')]['L_SECTOR_SIZE']*drive_dict[device.get('DEVNAME')]['L_SECTOR_COUNT'])/(1024*1024*1024))))+' GiB'
        drive_dict[device.get('DEVNAME')]['P_SECTOR_COUNT'] = int((int(drive_dict[device.get('DEVNAME')]['GIBI_SIZE'].replace(" GiB", ""))*(1024*1024*1024))/drive_dict[device.get('DEVNAME')]['P_SECTOR_SIZE'])

#write to file
with open('drives.json', 'w') as i:
    json.dump(drive_dict, i)
i.close()

c_dname_l = []                                                  #CHOSEN DEVICE NAMES. ALLOCATED LIST OF CHOSEN DEVICE NAMES i.e /dev/sdX
#MAIN MENU. DRIVE SELECTION
print("\nDRIVES DETECTED:\t")
data_drive_l = drive_print(drive_dict)                          #SUBDICTIONARY LIST OF 'DRIVE_DICT'. RETURN VALUE FROM PRINTING
print("\n")
print("SELECT DRIVE NUMBERS, SEPARATED BY {,;(space)(tab)}")
c1 = ""

while (c1 == ""):
    c1 = input("\n#? : ")
    c1 = re.sub('[' + re.sub(string.digits, '', string.printable) + ']', ',', c1)               #BOILING DOWN STRING... Removal of all non-digit characters, replacing with commas
    delimtd = c1.split(',')                                                                     #delimiting string by commas
    nargs = list(set([ch for ch in delimtd if ch != '']))                                       #split string into list of strings, excluding all commas, set() removes any duplicates
    if (verif_dialogue(nargs)):                 #if user accepts, continue
        e_count = 0
        if len(nargs) < 1:
            print("\033[31;1;4mNOT ENOUGH DRIVES SELECTED!\033[0m")
            c1 = ""
            continue
        for i in range(0, len(nargs)):
            try:
                if (int(nargs[i]) <= len(data_drive_l) and int(nargs[i]) > 0):
                    c_dname_l.append(data_drive_l[int(nargs[i])-1])                        #adding /dev/sdX list 'C_DNAME' for setup_disks()
                else:
                    c_dname_l.clear()       #signal restart of loop
                    c1 = ""
                    e_count += 1
                    if e_count > 1:                                                                             #only print 'INVALID OPTION' once
                        continue
                    print("\033[31;1;4mINVALID OPTION(s)!\033[0m")
            except ValueError:
                c_dname_l.clear()           #signal restart of loop
                c1 = ""
                e_count += 1
                if e_count > 1:                                                                                 #only print 'INVALID OPTION' once
                    continue
                print("\033[31;1;4mINVALID OPTION(s)!\033[0m")
    else:
        c1 = ""                                                       #continue loop

if len(c_dname_l) > 0:                                               #printing selected drive serial numbers
    for i in range(0, len(c_dname_l)):
        print("\033[32;1mOPTION : " + c_dname_l[i] + " CHOSEN!\033[0m")

print('\n')

#MAIN MENU. INSTALLATION
print("INSTALLATION OPTIONS")
c2 = ""
c_raidoption = -1
dict_install = {}   #contains keys representing tree of selected install type
while (c2 == "" and c_raidoption == -1):
    print("\033[32;1m\nMENU:\tROOT{}\033[0m")
    db_l1 = o_menu_print(db_data)
    c2 = input("\n#? : ")
    c2 = re.sub("\D", "", c2)
    nargs1 = [c2]
    if (verif_dialogue(nargs1)):
        e_count = 0
        c3 = ""
        try:
            if int(nargs1[0]) <= len(db_l1) and int(nargs1[0]) > 0:
                while (c3 == "" and c_raidoption == -1):
                    print("\033[32;1m\nMENU:\tROOT[{0}]\033[0m".format(db_l1[int(nargs1[0])-1]))
                    for k0 in sorted(db_data[db_l1[int(nargs1[0])-1]].keys()):
                        print("\033[32;1m{0} = [{1}]\033[0m".format(k0, ', '.join(sorted(db_data[db_l1[int(nargs1[0])-1]][k0]))))
                    print("\n0:\tRETURN TO PREVIOUS MENU")
                    db_l2 = o_menu_print(db_data[db_l1[int(nargs1[0])-1]])
                    c3 = input("\n#? : ")
                    c3 = re.sub("\D", "", c3)
                    nargs2 = [c3]
                    if (verif_dialogue(nargs2)):
                        e_count = 0
                        c4 = ""
                        try:
                            if int(nargs2[0]) == 0:
                                break
                            if int(nargs2[0]) <= len(db_l2) and int(nargs2[0]) > 0:
                                while (c4 == "" and c_raidoption == -1):
                                    print("\033[32;1m\nMENU:\tROOT[{0}][{1}]\033[0m".format(db_l1[int(nargs1[0])-1], db_l2[int(nargs2[0])-1]))
                                    print("0:\tRETURN TO PREVIOUS MENU")
                                    db_l3 = o_menu_print(db_data[db_l1[int(nargs1[0])-1]][db_l2[int(nargs2[0])-1]])
                                    c4 = input("\n#? : ")
                                    c4 = re.sub("\D", "", c4)
                                    nargs3 = [c4]
                                    if (verif_dialogue(nargs3)):
                                        e_count = 0
                                        try:
                                            if int(nargs3[0]) == 0:
                                                break
                                            if int(nargs3[0]) <= len(db_l3) and int(nargs3[0]) > 0:
                                                c_raidoption = db_l3[int(nargs3[0])-1]
                                                print("\033[32;1mOPTION : {0} CHOSEN!\033[0m\n".format(c_raidoption))
                                                dict_install["1"] = db_l1[int(nargs1[0])-1]                                     #'config.json' has three main levels... MODE, TYPE, SETTING
                                                dict_install["2"] = db_l2[int(nargs2[0])-1]                                     #'EPC_VANILLA', 'EPC_OCULUS' ARE MODES
                                                dict_install["3"] = db_l3[int(nargs3[0])-1]                                     #'BASIC', 'EXTRA' ARE TYPES
                                            else:                                                                               #'EPC_VANILLA_RAID1_ALL', 'EPC_OCULUS_RAID5_ROOT' ARE SETTINGS
                                                c4 = ""                                                                         #if this tree is changed, this entire nested loop, and the construction of 'dict_install' must be changed
                                                e_count+=1
                                                if e_count > 1:
                                                    continue
                                                print("\033[31;1;4mINVALID OPTION!\033[0m")
                                        except ValueError:
                                            c4 = ""
                                            e_count+=1
                                            if e_count > 1:
                                                continue
                                            print("\033[31;1;4mINVALID OPTION!\033[0m")
                                    else:
                                        c4 = ""
                                    c4 = ""
                            else:
                                c3 = ""
                                e_count+=1
                                if e_count > 1:
                                    continue
                                print("\033[31;1;4mINVALID OPTION!\033[0m")
                        except ValueError:
                            c3 = ""
                            e_count+=1
                            if e_count > 1:
                                continue
                            print("\033[31;1;4mINVALID OPTION!\033[0m")
                    else:
                        c3 = ""
                    c3 = ""
            else:
                c2 = ""
                e_count+=1
                if e_count > 1:
                    continue
                print("\033[31;1;4mINVALID OPTION!\033[0m")
        except ValueError:
            c2 = ""
            e_count+=1
            if e_count > 1:
                continue
            print("\033[31;1;4mINVALID OPTION!\033[0m")
    else:
        c2 = ""
    c2 = ""

#CRITICAL CHECK. ENSURE USER CANNOT SELECT RAID HIGHER THAN RAID 1 'MIRROR' IF TWO DRIVES SPECIFIED. ENSURE USER CANNOT SELECT RAID LOWER THAN RAID 5 'STRIPING WITH PARITY' IF THREE DRIVES SPECIFIED. OTHER RAID TYPES MAY NEED TO BE ADDED TO FULLY SUPPORT INITIALIZATION

for k0 in db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["RAID_PART_OPT"].keys():
    if int(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["RAID_PART_OPT"][k0]) == -1 and len(c_dname_l) != 1:
        print("\033[31;1mERROR! more than 1 drive specified for single disk installation!\033[0m")
        sys.exit(1)
    if int(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["RAID_PART_OPT"][k0]) == 1 and len(c_dname_l) < 2:
        print("\033[31;1mERROR! cannot create {0} (raid1) with {1} drives!\033[0m".format(dict_install["3"], len(c_dname_l)))
        sys.exit(1)
    if int(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["RAID_PART_OPT"][k0]) == 5 and len(c_dname_l) < 3:
        print("\033[31;1mERROR! cannot create {0} (raid5) with {1} drives!\033[0m".format(dict_install["3"], len(c_dname_l)))
        sys.exit(1)
    if int(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["RAID_PART_OPT"][k0]) == 6 and len(c_dname_l) < 4:
        print("\033[31;1mERROR! cannot create {0} (raid6) with {1} drives!\033[0m".format(dict_install["3"], len(c_dname_l)))
        sys.exit(1)
    if int(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["RAID_PART_OPT"][k0]) == 10 and len(c_dname_l) < 2:     #really should be 4, but linux supports
        print("\033[31;1mERROR! cannot create {0} (raid10) with {1} drives!\033[0m".format(dict_install["3"], len(c_dname_l)))
        sys.exit(1)

#CRITICAL CHECK. THE SMALLEST OS CONFIGURATION, IS MADE IN PART BY CHOSEN CONFIGURATION AND A MINIMUM OF 10GiB ROOT.
#IF THIS AGGREGATE IS LARGER THAN ANY OF THE DRIVES SPECIFIED, EXIT WARNING USER THAT DRIVE IS TOO SMALL
crit_space_fail = 0
for i in range(0, len(c_dname_l)):
    smallest_logical = ((db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["SWAP"]*1024*1024)+(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["ESP"]*1024*1024)+(10*1024*1024*1024)+(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["BOOT"]*1024*1024))/drive_dict[c_dname_l[i]]["P_SECTOR_SIZE"]

    if smallest_logical > drive_dict[c_dname_l[i]]["P_SECTOR_COUNT"]:
        print("\033[31;1mERROR! drive {0}: {1} is too small for requested configuration: {2}MiB swap {3}MiB esp {4}MiB boot with min 10GiB root = {5}GiB\033[0m".format(c_dname_l[i], drive_dict[c_dname_l[i]]["GIBI_SIZE"], db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["SWAP"], db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["ESP"], db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]]["BOOT"], ((smallest_logical*drive_dict[c_dname_l[i]]['P_SECTOR_SIZE'])/(1024*1024*1024))))
        crit_space_fail = 1

    if i == len(c_dname_l)-1 and crit_space_fail:       #if the end of the list and a fail has occurred, exit with error
        sys.exit(1)

    #warn user about limited disk size to use. if this step evaluates to true, and the user continues, I assume a virtual machine is being built
    if int(drive_dict[c_dname_l[i]]["GIBI_SIZE"].replace(" GiB", "")) < 500:
        print("\033[33;1mWARNING! drive {0}: {1} is too small for production use. Continuing with installation...\033[0m".format(c_dname_l[i], drive_dict[c_dname_l[i]]["GIBI_SIZE"]))
    if "NORAID" in dict_install["3"]:
        print("\033[33;1mWARNING! {0} option chosen. Installing on a single disk is not recommended for production. Continuing with installation...\033[0m".format(dict_install["3"]))

with open('warning') as g:
    print(g.read())
g.close()

c3 = input("\033[31mCONTINUE? [Y/n] :\033[0m ")
if (c3 == "y") or (c3 == "yes") or (c3 == ""):
    with open('warning_2') as a:
        print(a.read())
    a.close()
else:
    print("\033[32m\nUSER TERMINATION!\033[0m")
    sys.exit(0)

for i in range(0, len(c_dname_l)):
    print("\033[32;1;4mCHOSEN DRIVE: {0}\033[0m".format(c_dname_l[i]))
print("\033[32;1;4mCHOSEN RAID: {0}\033[0m".format(c_raidoption))

c3 = input("\033[31;1;4mCONTINUE? [Y/n] :\033[0m")
if (c3 == "y") or (c3 == "yes") or (c3 == ""):
    #EPC function calls
    c_install_subdict = copy.deepcopy(db_data[dict_install["1"]][dict_install["2"]][dict_install["3"]])     #copy configuration details from selected installation setting, will be passed to later methods
    setup_disks(c_install_subdict, c_dname_l)
    tarball = '/root/epc_setup/rootfs.tgz'
    with open('master.json') as z:
        disk_dict = json.load(z)		#reload modified master db into memory. created in 'epc_disk.py'
    f.close()
    deploy_tarball(tarball, disk_dict)
else:
    print("\033[32m\nUSER TERMINATION!\033[0m")
    sys.exit(0)


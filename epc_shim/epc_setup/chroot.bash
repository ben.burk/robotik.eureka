#!/bin/bash

#author : Benjamin Burk
#details : EPC Setup CHROOT script. WRITES FSTAB, MDADM, CONFIGURES SWAP, INSTALLS GRUB, REBUILDS ALL KERNELS
#date : 13 MARCH 2019
#NOTE : Development must be continued and tested, if EPC OS is to be moved to Debian
#copyright : BURKTECH 2019

#FINAL STEP IN EPC INSTALLATION
#THIS SCRIPT IS CALLED FROM INSIDE THE FINAL CHROOT, ROOT_MNT, BY DEFAULT IS /MNT

if ! [[ $(id -u) = 0 ]]; then											#check for effective user
	echo -e "\033[1;31mERROR! finalization script must be called as root\033[0m"
	exit 1
fi
if [[ "$(stat -c %d:%i /)" == "$(stat -c %d:%i /proc/1/root/.)" ]]; then					#check for chroot
	echo -e "\033[1;31mERROR! finalization script must be called from inside a chroot\033[0m"
	exit 1
fi

#CONSTRUCTION OF ARGUMENTS FROM 'PARTUUID.JSON'
args_cmd=()
args_nm=()
args_dev=()
args_ele=0

for (( i=0;i<$(cat partuuid.json | jq '. | length');i++ )); do
	args_cmd[${args_ele}]="$(cat partuuid.json | jq ".p$((i+1)).UUID" | sed 's/\"//g')"
	args_nm[${args_ele}]="$(cat partuuid.json | jq ".p$((i+1)).NAME" | sed 's/\"//g')"
	args_dev[${args_ele}]="$(cat partuuid.json | jq ".p$((i+1)).DEVICE" | sed 's/\"//g')"
	args_ele=$((args_ele+1))
done

#CONSTRUCTION OF ARGUMENTS FROM 'MASTER.JSON'
grub_disks=()
grub_q=""
for (( k=0;k<$args_ele;k++ )); do
	if [[ "$(echo "${args_nm[${k}]}" | awk '{print toupper($0)}')" == "ESP" ]] || [[ "$(echo "${args_nm[${k}]}" | awk '{print toupper($0)}')" == "EFI" ]]; then		#ESP/EFI is the search token
		if [[ "$(echo "${args_dev[${k}]}" | sed 's/^\(\/dev\/\)*//')" == *"md"* ]]; then	#if device is mdarray, grab list of member disks 
			grub_q="$(cat master.json | jq ".MDARRAY.\""${args_dev[${k}]}"\".DEVLIST" | sed 's/\"//g' | sed 's/[][]//g' | sed 's/[0-9]//g' | tr -d '[:space:]')"
		else
			grub_q="$(cat partuuid.json | jq ".p1.DEVICE" | sed 's/\"//g' | sed 's/[0-9]//g' | tr -d '[:space:]')"	#if device is individual, grab disk
		fi
	fi
done
#construct array. each element will be used to install grub to
if [[ "$grub_q" != "" ]]; then
	set -f
	grub_disks=(${grub_q//,/ })
	set +f

	echo "GRUB WILL BE INSTALLED TO THE FOLLOWING DISKS:"
	for (( a=0;a<${#grub_disks[@]};a++ )); do
		echo "${grub_disks[${a}]}"
	done
fi

echo "BUILDING FSTAB..."
#WRITING DEFAULT HEADER TO NEW FSTAB
echo "# /etc/fstab: static file system information." > fstab_file
echo "#" >> fstab_file
echo "# Use 'blkid' to print the universally unique identifier for a" >> fstab_file
echo "# device; this may be used with UUID= as a more robust way to name devices" >> fstab_file
echo "# that works even if disks are added and removed. See fstab(5)." >> fstab_file
echo "#" >> fstab_file
echo "# <file system> <mount point>   <type>  <options>       <dump> <pass>" >> fstab_file

#CONSTRUCTION OF FSTAB FILE. ROOT, BOOT, SWAP
for (( j=0;j<$args_ele;j++ )); do

	grep "(${args_nm[${j}]})" fstab_file >/dev/null		#prevents more than one root, boot, or swap being added to fstab
	is_entry="$?"

	if [[ $(echo "${args_nm[${j}]}" | awk '{print toupper($0)}') = "ROOT" ]] && [[ "$is_entry" = "1" ]]; then
		echo "# / was on "${args_dev[${j}]}" (root) during installation" >> fstab_file
		echo "UUID=${args_cmd[${j}]} /		xfs	defaults		0	1" >> fstab_file
	fi
	if [[ $(echo "${args_nm[${j}]}" | awk '{print toupper($0)}') = "BOOT" ]] && [[ "$is_entry" = "1" ]]; then
		echo "# /boot was on "${args_dev[${j}]}" (boot) during installation" >> fstab_file
		echo "UUID=${args_cmd[${j}]} /boot	xfs	defaults		0	0" >> fstab_file
	fi
	if [[ $(echo "${args_nm[${j}]}" | awk '{print toupper($0)}') = "EFI" || $(echo "${args_nm[${j}]}" | awk '{print toupper($0)}') = "ESP" ]] && [[ "$is_entry" = "1" ]]; then
		echo "# /boot/efi was on "${args_dev[${j}]}" (esp) during installation" >> fstab_file
		echo "UUID=${args_cmd[${j}]} /boot/efi		vfat	umask=0077,shortname=winnt		0	0" >> fstab_file	#is_entry variable prevents multiple addition
	fi
	if [[ $(echo "${args_nm[${j}]}" | awk '{print toupper($0)}') = "SWAP" ]] && [[ "$is_entry" = "1" ]]; then
		echo "# swap was on "${args_dev[${j}]}" (swap) during installation" >> fstab_file
		echo "UUID=${args_cmd[${j}]} none		swap	sw			0	0" >> fstab_file			#is_entry variable prevents multiple addition
	fi
done


#FSTAB FILE FINAL CHECK. IF FILE DOES NOT HAVE ENTRIES FOR ROOT, BOOT, AND SWAP, then exit
for (( k=0;k<$args_ele;k++ )); do
	grep "${args_nm[${k}]}" fstab_file >/dev/null
	fstab_chk="$?"

	if [[ "$fstab_chk" = "1" ]]; then
		echo -e "\033[1;31mERROR! finalization script could not detect a $(echo ${args_nm[${k}]} | awk '{print toupper($0)}') entry from input arguments\033[0m"
		#rm -f fstab_file
		exit 1
	fi
done

#IF FSTAB FILE EXISTS, FROM THE TARBALL THAT WAS DUMPED, REMOVE AND REPLACE
#IF NOT, JUST COPY TO ETC
if [ -e /etc/fstab ]; then
	rm -f /etc/fstab
fi
mv fstab_file /etc/fstab

echo "INSTALLING MDADM CONFIGURATION..."
#SOME DISTRIBUTIONS OF LINUX HAVE THE CONFIGURATION FILE IN DIFFERENT PLACES
#I.E. DEBIAN IS IN '/ETC/MDADM/MDADM.CONF'. CENTOS '/ETC/MDADM.CONF'
os="$(cat /etc/os-release | grep -v "VERSION_ID" | grep -v "ID_LIKE" | grep "ID" | sed 's/.*=//' | awk '{print toupper($0)}' | sed 's/\"//g')"
if [[ "$os" = "DEBIAN" ]]; then
	if [ -e /etc/mdadm/mdadm.conf ]; then
		rm -f /etc/mdadm/mdadm.conf
	fi
	mv mdadm_file /etc/mdadm/mdadm.conf
fi
if [[ "$os" = "CENTOS" ]]; then
	if [ -e /etc/mdadm.conf ]; then
		rm -f /etc/mdadm.conf
	fi
	mv mdadm_file /etc/mdadm.conf
fi

#DESTRUCTION OF GRUB.CFG, OLD FILE DUMPED FROM TARBALL. CHECK ALL POSSIBLE LOCATIONS
if [ -f /boot/grub/grub.cfg ]; then
	rm -f /boot/grub/grub.cfg		#not sure where grub symlink exists on debian
fi
if [ -f /boot/grub2/grub.cfg ]; then
	rm -f /boot/grub2/grub.cfg		#remove config file
	if [ -L /etc/grub2.cfg ]; then
		unlink /etc/grub2.cfg
	fi					#remove symlink
fi
if [[ "$os" = "CENTOS" ]] && [ -f "/boot/efi/EFI/$(echo $os | awk '{print tolower($0)}')/grub.cfg" ]; then
	rm -f "/boot/efi/EFI/$(echo $os | awk '{print tolower($0)}')/grub.cfg"					#removal of efi grub config file. symlink kept for recreation
fi
if [[ "$os" = "DEBIAN" ]] && [ -f "/boot/efi/EFI/$(echo $os | awk '{print tolower($0)}')/grub.cfg" ]; then
	rm -f "/boot/efi/EFI/$(echo $os | awk '{print tolower($0)}')/grub.cfg"					#removal of efi grub config file. symlink kept for recreation
fi

#INSTALLATION OF GRUB
echo "INSTALLING GRUB..."
if [[ "$os" = "CENTOS" ]]; then
	for (( y=0;y<${#grub_disks[@]};y++ )); do
		if [ ${#grub_disks[@]} -eq 1 ]; then										#let system find EFI and install
			grub2-install --recheck >/dev/null 2>&1
			if [[ "$?" = "0" ]]; then
				echo -e "\033[1;32mGRUB2-INSTALL: successfully installed to ${grub_disks[${y}]}\033[0m"
			else
				echo -e "\033[1;31mGRUB2-INSTALL: grub2-install failed on ${grub_disks[${y}]}\033[0m"
				exit 1
			fi
		else
			grub2-install --bootloader-id=GRUB --recheck "${grub_disks[${y}]}" >/dev/null 2>&1			#install grub to disks (CENTOS)
			if [[ "$?" = "0" ]]; then
				echo -e "\033[1;32mGRUB2-INSTALL: successfully installed to ${grub_disks[${y}]}\033[0m"
			else
				echo -e "\033[1;31mGRUB2-INSTALL: grub2-install failed on ${grub_disks[${y}]}\033[0m"
				exit 1
			fi
		fi
	done
fi
if [[ "$os" = "DEBIAN" ]]; then
	for (( z=0;z<${#grub_disks[@]};z++ )); do
		if [ ${#grub_disks[@]} -eq 1 ]; then										#let system find EFI and install
			grub-install --recheck >/dev/null 2>&1
			if [[ "$?" = "0" ]]; then
				echo -e "\033[1;32mGRUB-INSTALL: successfully installed to ${grub_disks[${z}]}\033[0m"
			else
				echo -e "\033[1;31mGRUB-INSTALL: grub-install failed on ${grub_disks[${z}]}\033[0m"
				exit 1
			fi
		else
			grub-install --bootloader-id=GRUB --recheck "${grub_disks[${z}]}" >/dev/null 2>&1			#install grub to disks (DEBIAN)
			if [[ "$?" = "0" ]]; then
				echo -e "\033[1;32mGRUB-INSTALL: successfully installed to ${grub_disks[${z}]}\033[0m"
			else
				echo -e "\033[1;31mGRUB-INSTALL: grub-install failed on ${grub_disks[${z}]}\033[0m"
				exit 1
			fi
		fi
	done
fi

#CONFIGURE PERSISTENT SETTINGS FOR SWAP
echo "FINAL CONFIGURATION OF SWAP..."
for (( l=0;l<$args_ele;l++ )); do
	if [[ $(echo "${args_nm[${l}]}" | awk '{print toupper($0)}') = "SWAP" ]]; then					#if swap partition specified in arguments, and command exists for found partition, proceed
															#adding swap uuid to grub defaults, if it isn't already configured
		if [[ "$os" == "DEBIAN" ]]; then
			initramfs_swap_default="RESUME=UUID=${args_cmd[${l}]}"
			if [[ -d /etc/initramfs-tools ]] || [[ ! -d /etc/initramfs-tools/conf.d ]]; then
				mkdir -p /etc/initramfs-tools/conf.d
			fi
			if [[ -f /etc/initramfs-tools/conf.d/resume ]]; then
				rm -f /etc/initramfs-tools/conf.d/resume
			fi
			echo "$initramfs_swap_default" > /etc/initramfs-tools/conf.d/resume
			echo -e "\033[1;32minitramfs default (Debian) : set RESUME=UUID=${args_cmd[${l}]}\033[0m"
		fi


		cat /etc/default/grub | grep -w "GRUB_PRELOAD_MODULES" >/dev/null 2>&1
		if [[ "$?" = "0" ]]; then											#check if line exists in config
			if [[ "$(cat /etc/default/grub | grep -w "GRUB_PRELOAD_MODULES")" != *"xfs mdraid1x mdraid09"* ]] && [[ "$(cat /etc/default/grub | grep -w "GRUB_PRELOAD_MODULES")" != "GRUB_PRELOAD_MODULES=\"\"" ]]; then	#addition of xfs, raid fields to end of line
				grub_mod_default="$(cat /etc/default/grub | grep -w "GRUB_PRELOAD_MODULES" | sed 's^\"*$^^' | sed 's^$^ xfs mdraid1x mdraid09\"^')"
				sed -i 's^GRUB_PRELOAD_MODULES=.*^'"$grub_mod_default"'^' /etc/default/grub
				echo -e "\033[1;32mgrub default : updated modules to : xfs mdraid1x mdraid09\033[0m"
			elif [[ "$(cat /etc/default/grub | grep -w "GRUB_PRELOAD_MODULES")" == "GRUB_PRELOAD_MODULES=\"\"" ]]; then
				grub_mod_default="$(cat /etc/default/grub | grep -w "GRUB_PRELOAD_MODULES" | sed 's^\"*$^^' | sed 's^$^\"xfs mdraid1x mdraid09\"^')"
				sed -i 's^GRUB_PRELOAD_MODULES=.*^'"$grub_mod_default"'^' /etc/default/grub
				echo -e "\033[1;32mgrub default : updated modules to : xfs mdraid1x mdraid09\033[0m"

			fi	#if xfs mdraid1x mdraid09 exists in line, no update needed
		else
			grub_mod_default="GRUB_PRELOAD_MODULES=\"xfs mdraid1x mdraid09\""						#addition of newline, as line did not exist
			echo "$grub_mod_default" >> /etc/default/grub
			echo -e "\033[1;32mgrub default : added modules : xfs mdraid1x mdraid09\033[0m"
		fi
		
		cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX" >/dev/null 2>&1
		if [[ "$?" = "0" ]]; then											#check if line exists in config
			if [[ "$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX")" != *"rd.auto rd.auto=1"* ]] && [[ "$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX")" != "GRUB_CMDLINE_LINUX=\"\"" ]]; then	#addition of raid auto assembly field to end of line
				grub_assemb_default="$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX" | sed 's^\"*$^^' | sed 's^$^ rd.auto rd.auto=1\"^')"
				sed -i 's^GRUB_CMDLINE_LINUX=.*^'"$grub_assemb_default"'^' /etc/default/grub
				echo -e "\033[1;32mgrub default : updated raid auto assembly to : rd.auto rd.auto=1\033[0m"
			elif [[ "$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX")" == "GRUB_CMDLINE_LINUX=\"\"" ]]; then
				grub_assemb_default="$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX" | sed 's^\"*$^^' | sed 's^$^\"rd.auto rd.auto=1\"^')"
				sed -i 's^GRUB_CMDLINE_LINUX=.*^'"$grub_assemb_default"'^' /etc/default/grub
				echo -e "\033[1;32mgrub default : updated raid auto assembly to : rd.auto rd.auto=1\033[0m"
			fi	#if rd.auto rd.auto=1 exists in line, no update needed
		else
			grub_assemb_default="GRUB_CMDLINE_LINUX=\"rd.auto rd.auto=1\""						#addition of newline, as line did not exist
			echo "$grub_assemb_default" >> /etc/default/grub
			echo -e "\033[1;32mgrub default : added raid auto assembly : rd.auto rd.auto=1\033[0m"
		fi

		cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT" >/dev/null 2>&1
		if [[ "$?" = "0" ]]; then											#check if line exists in config
			if [[ "$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT")" != *"resume=UUID="* ]] && [[ "$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT")" != "GRUB_CMDLINE_LINUX_DEFAULT=\"\"" ]]; then	#adding 'resume=UUID=' entry to existing line in /etc/default/grub
				grub_swap_default="$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT" | sed 's^\"*$^^' | sed 's^$^ resume=UUID='"${args_cmd[${l}]}"'\"^')"
				sed -i 's^GRUB_CMDLINE_LINUX_DEFAULT=.*^'"$grub_swap_default"'^' /etc/default/grub
				echo -e "\033[1;32mgrub default : added field : resume=UUID=${args_cmd[${l}]}\033[0m"
				break
			elif [[ "$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT")" == "GRUB_CMDLINE_LINUX_DEFAULT=\"\"" ]]; then #adding resume to empty
				grub_swap_default="$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT" | sed 's^\"*$^^' | sed 's^$^\"resume=UUID='"${args_cmd[${l}]}"'\"^')"
				sed -i 's^GRUB_CMDLINE_LINUX_DEFAULT=.*^'"$grub_swap_default"'^' /etc/default/grub
				echo -e "\033[1;32mgrub default : added field : resume=UUID=${args_cmd[${l}]}\033[0m"
				break
			else			#replace 'resume=UUID=' entry with updated one, computed as a line, in place inside the file
				grub_swap_default="$(cat /etc/default/grub | grep -w "GRUB_CMDLINE_LINUX_DEFAULT" | sed 's_^[^\"]*\"__' | sed 's_\"__g' | sed 's_resume=UUID=[^ ]*_resume=UUID='"$grub_swap_default"'_')"
				sed -i 's~GRUB_CMDLINE_LINUX_DEFAULT=.*~GRUB_CMDLINE_LINUX_DEFAULT=\"'"$grub_swap_default"'\"~' /etc/default/grub
				echo -e "\033[1;32mgrub default : replaced resume field with : resume=UUID=${args_cmd[${l}]}\033[0m"
				break
			fi
		else
			grub_swap_default="GRUB_CMDLINE_LINUX_DEFAULT=\"resume=UUID=${args_cmd[${l}]}\""			#adding new line 'GRUB_CMDLINE_LINUX_DEFAULT' with field 'resume=UUID='
			echo "$grub_swap_default" >> /etc/default/grub
			echo -e "\033[1;32mgrub default : added new line : $grub_swap_default\033[0m"
			break
		fi
	fi
done

#END OF SCRIPT, UPDATE INITRAMFS AND GRUB. REBOOT WITH SUCCESS MESSAGE
if [[ "$os" = "DEBIAN" ]]; then											#update initramfs and grub on debian system
	update-initramfs -u											#this section will need further development, if debian used
	if [[ "$?" = "0" ]]; then
		echo -e "\033[1;32mregenerated initramfs image\033[0m"
	else
		echo -e "\033[1;31mregeneration of initramfs failed\033[0m"
		exit 1
	fi
	update-grub
	if [[ "$?" = "0" ]]; then
		echo -e "\033[1;32mreconfigured grub bootloader\033[0m"
	else
		echo -e "\033[1;31mreconfiguring of grub bootloader failed\033[0m"
		exit 1
	fi
fi
if [[ "$os" = "CENTOS" ]]; then
	#update initramfs and grub on centos system
	k_vers="$(ls -Art /boot/initramfs* | grep -v "kdump" | grep -v "rescue" | sed 's/^[^-]*-//g')"  			#find all relevant kernels, and build against each, one at a time. will not build kdump or rescue kernel here. kdump is requested disabled. rescue kernel generated below.
	k_vers_ln="$(echo "$k_vers" | wc -l)"
	l_kern=""
	for (( i=0;i<$k_vers_ln;i++ )); do									#filesystem modules below will throw errors sometimes, as some installed initramfs images do not support all filesystems
		dracut --mdadmconf --fstab --add="mdraid resume" --filesystems "vfat xfs ext3 ext4 devpts sysfs proc" --add-drivers="xfs raid1 raid10 raid456" --force "/boot/initramfs-$(echo "$k_vers" | head -$((i+1)) | tail -1)" "$(echo "$k_vers" | head -$((i+1)) | tail -1 | sed 's/\.[^.]*$//')" >/dev/null 2>&1
		
		if [[ "$?" = "0" ]]; then
			echo -e "\033[1;32mregenerated /boot/initramfs-$(echo "$k_vers" | head -$((i+1)) | tail -1)\033[0m"
			l_kern="$(echo "$k_vers" | head -$((i+1)) | tail -1)"		#store variable as reference. kernels are reverse-iterated by timestamp. rescue kernel will be made from newest kernel detected below.
		else
			echo -e "\033[1;31mregeneration of /boot/initramfs-$(echo "$k_vers" | head -$((i+1)) | tail -1) failed\033[0m"
			exit 1
		fi
	done

	#regeneration of rescue kernel, from newest kernel detected above
	if [ -f /etc/kernel/postinst.d/51-dracut-rescue-postinst.sh ]; then
		rm -f /boot/vmlinuz-0-rescue-* /boot/initramfs-0-rescue-*.img												#removal of rescue kernel
		/etc/kernel/postinst.d/51-dracut-rescue-postinst.sh "$(echo "$l_kern" | sed 's/\.[^.]*$//')" "/boot/vmlinuz-$(echo "$l_kern" | sed 's/\.[^.]*$//')"	#regenerate rescue kernel
		if [[ "$?" = "0" ]]; then
			echo -e "\033[1;32mregenerated rescue kernel\033[0m"
		else
			echo -e "\033[1;33mWARNING! regeneration of rescue kernel failed! Recommend regeneration of rescue kernel on next boot\033[0m"
		fi
	else
		echo -e "\033[1;33mWARNING! 51-dracut-rescue-postinst.sh script not found! Cannot regenerate rescue kernel. Recommend regeneration of rescue kernel on next boot\033[0m"
	fi
	
	grub2-mkconfig -o "$(readlink /etc/grub2-efi.cfg)" >/dev/null 2>&1					#grub was installed previously, so use symlink that was setup
	if [[ "$?" = "0" ]]; then
		echo -e "\033[1;32mreconfigured EFI grub bootloader\033[0m"
	else
		echo -e "\033[1;31mreconfiguring of EFI grub bootloader failed\033[0m"
		exit 1
	fi

	#mark system selinux relabel. required for centos, or system will not allow login
	touch /.autorelabel
fi

tar -czf /root/epc.tgz master.json partuuid.json *.table								#copy as backup. placed in /root, inside chroot
rm -f master.json partuuid.json chroot.bash *.table									#chroot archival of data, cleanup
exit &&
umount "$root_mnt/tmp" &&
umount "$root_mnt/sys" &&
umount "$root_mnt/proc" &&
umount "$root_mnt/dev/pts" &&
umount "$root_mnt/dev" &&
exit 0

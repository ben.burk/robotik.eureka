#!/bin/bash
#
# EPC_SETUP ENTRY POINT. DEBIAN SYSTEM WILL AUTOLOGIN AS ROOT. THIS SCRIPT WILL BE RUN WHEN THE USER TYPES 'install'
#

#patch before execution. stop all md devices
md_dev="$(ls -l /dev/md* 2>/dev/null | grep '^b' | sed 's/.* //' | tr '\n' ',' | sed 's/.$//')"
if [[ "$md_dev" != "" ]]; then
	set -f
	md_disks=(${md_dev//,/ })
	set +f

	for (( a=0;a<${#md_disks[@]};a++ )); do			#will print devices that are stopped
		if [[ "${md_disks[${a}]}" == *"/dev/"* ]]; then
			mdadm --stop "${md_disks[${a}]}"
		fi
	done
fi


cd /root/epc_setup
/root/epc_setup/epc_setup
exit 0

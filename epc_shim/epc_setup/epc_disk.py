
#author : Benjamin Burk
#details : EPC Setup 'epc_disk.py' routine. WIPES ALL FILE SIGNATURES, PARTITIONS DRIVES, HANDLES FAKE RAID (MDADM), FORMATS DEVICES, CONFIGURES AND ENABLES SWAP, AND WRITES ALL SETUP INFORMATION TO DISK
#date : Updated 13 MAR 2019
#copyright : BURKTECH 2019

import json
import os
import sys
import re
import math
import string
import shlex
import parted
from subprocess import Popen, PIPE, STDOUT

try:
    from subprocess import DEVNULL
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

#called by 'epc_setup.py' (main). CONFIGURES DISKS BASED ON INPUT
#install_settings, dictionary representing selection from install menu
#devices, list of device strings to use from menu ... i.e (/dev/sdX, /dev/sdY)
def setup_disks(install_settings, devices):

    if len(devices) < 1:
        print("\033[31;1mERROR! NOT ENOUGH DRIVES PROVIDED FOR DISK SETUP: {0}\t{1}\033[0m".format(len(devices), devices))
        sys.exit(1)

    #menu spacing
    print("\n")

    #open drives json, grabbing drives from choice
    with open("drives.json") as j:
        drive_data = json.load(j)
    j.close()

    #initialize values for drive partitioning
    #I expect these to be in MiB units, from config.json
    swap_sz=install_settings["SWAP"]
    espfs_sz=install_settings["ESP"]
    bootfs_sz=install_settings["BOOT"]

    temp=-1
    disk_dict = {}                                                                      #construction of consolidated disc dictionary
    disk_dict["DISK"] = {}
    disk_dict["MDARRAY"] = {}
    disk_dict["INFO"] = {}
    for i in range(0, len(devices)):
        disk_dict["DISK"][devices[i]] = {}
        disk_dict["DISK"][devices[i]]["PARTITION"] = {}
        disk_dict["DISK"][devices[i]]["P_SECTOR_SIZE"] = 0
        disk_dict["DISK"][devices[i]]["L_SECTOR_SIZE"] = 0
        disk_dict["DISK"][devices[i]]["P_SECTOR_COUNT"] = 0
        disk_dict["DISK"][devices[i]]["L_SECTOR_COUNT"] = 0

    #DECISION MAKING FOR DISK BOUNDARIES (SECTOR SIZES)
    for k0 in drive_data.keys():
        for i in range(0, len(devices)):
            if drive_data[k0] == devices[i]:
                if temp == -1:          #initial set
                    temp = drive_data[k0]["P_SECTOR_SIZE"]
                if drive_data[k0]["P_SECTOR_SIZE"] != temp:
                    print("\033[33;1mFOUND SELECTED DRIVES HAVE DIFFERENT PHYSICAL SECTOR SIZES: {0}, {1}\033[0m".format(drive_data[k0]["P_SECTOR_SIZE"], temp))
                    print("\033[33;1mUSING LOGICAL SECTOR SIZE AS BOUNDARY\033[0m")
                    temp=-2
                    break
                else:
                    continue

    #ADDITION OF DEVICE FIELDS TO DICTIONARY
    for k0 in drive_data.keys():
        for i in range(0, len(devices)):
            if devices[i] == k0:
                disk_dict["DISK"][devices[i]]["P_SECTOR_SIZE"] = drive_data[k0]["P_SECTOR_SIZE"]
                disk_dict["DISK"][devices[i]]["P_SECTOR_COUNT"] = drive_data[k0]["P_SECTOR_COUNT"]
                disk_dict["DISK"][devices[i]]["L_SECTOR_SIZE"] = drive_data[k0]["L_SECTOR_SIZE"]
                disk_dict["DISK"][devices[i]]["L_SECTOR_COUNT"] = drive_data[k0]["L_SECTOR_COUNT"]

    #ERROR CHECKING FOR CODE INTEGRITY.
    if temp == -2:
        lsize_agg = 0
        for a, b in enumerate(drive_data.keys()):
            lsize_agg+=int(drive_data[b]["L_SECTOR_SIZE"])
            if not lsize_agg % drive_data[b]["L_SECTOR_SIZE"] == 0:
                #throw an error, showing the incongruity of logical sector sizes among recognized drives
                print("\033[31;1mERROR! FOUND DRIVES WITH DIFFERENT LOGICAL SECTOR SIZES! {0}, {1} !\033[0m".format((lsize_agg-int(drive_data[b]["L_SECTOR_SIZE"])/a), drive_data[b]["L_SECTOR_SIZE"]))
                sys.exit(1)

    #removed EFI RAID guard 02.02.2019
    #if "ESP" in install_settings["RAID_PART_OPT"] or "EFI" in install_settings["RAID_PART_OPT"] or "BOOT" in install_settings["RAID_PART_OPT"]:
        #print("\033[31;1mERROR! WILL NOT CREATE RAID DEVICE OF ESP/EFI OR BOOT PARTITIONS. RISK OF CORRUPTION. EXITING!\033[0m")
        #sys.exit(1)

    #FINDING SECTOR SIZE AND COUNT TO USE
    used_sectorc = sys.maxsize                                                      #set value to max, easy value to compare drive sector counts with. looking for lowest number
    used_sector_sz = 0
    for k0 in disk_dict["DISK"].keys():
        if temp > 0:
            used_sector_sz = disk_dict["DISK"][k0]["P_SECTOR_SIZE"]
            if disk_dict["DISK"][k0]["P_SECTOR_COUNT"] < used_sectorc:
                used_sectorc = disk_dict["DISK"][k0]["P_SECTOR_COUNT"]
        else:
            used_sector_sz = disk_dict["DISK"][k0]["L_SECTOR_SIZE"]
            if disk_dict["DISK"][k0]["L_SECTOR_COUNT"] < used_sectorc:
                used_sectorc = disk_dict["DISK"][k0]["L_SECTOR_COUNT"]

    print("{0} BYTE SECTOR SIZE CHOSEN...".format(used_sector_sz))
    print("CALCULATING PARTITION TABLE...")
    #VARIABLES LEFT IN SECTOR UNITS
    sda1_start = 1023                   #if logical sector size is used, then start at 1024 sectors
    if (used_sector_sz > 1024):         #if physical sector size is used, then start at 2048 sectors
        sda1_start = 2047

    sda1_scount = (espfs_sz*1048576)/used_sector_sz         #ESP (boot) partition calculation for number of sectors
    sda1_end = sda1_start+sda1_scount
    sda2_start = sda1_end+1
    sda2_scount = (bootfs_sz*1048576)/used_sector_sz        #BOOT (boot) partition calculation for number of sectors
    sda2_end = sda2_start+sda2_scount
    swap_start = sda2_end+1
    swap_scount = (swap_sz*1048576)/used_sector_sz          #SWAP partition calculation for number of sectors
    swap_end = swap_start+swap_scount
    sda3_start = swap_end+1
    sda3_scount = used_sectorc-sda3_start-((100*1048576)/used_sector_sz)            #ROOT partition calculation for number of sectors, 100MiB unallocated, in the case that another drive, possibly a different manufacturer is used to rebuild in the future

    #WIPING ALL *SELECTED* DEVICE SIGNATURES with wipefs, including partitions and device... i.e. /dev/sda .... sda1, sda2
    #pools selected disks for wiping/reformat and reads '/proc/partitions' comparing and searching for a matching device name
    #reverse-iterates through file, due to the format of '/proc/partitions'.... i.e. need to process /dev/sda1 before /dev/sda
    #example:
    #       /dev/sda    <- Wiped third
    #       /dev/sda1   <- Wiped second
    #       /dev/sda2   <- Wiped first

    print("WIPING DRIVE SIGNATURES...")
    for i in range(0, len(devices)):
        for line in reversed(list(open('/proc/partitions'))):
            if devices[i].replace("/dev/","") in line:
                #line is split from the end, using index '1' as the first " "(space) from the end. index '1' will be the device name
                proc = Popen(["mdadm", "--zero-superblock", "/dev/"+line.rsplit(' ',1)[1].replace("\n","")], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
                md_zo, md_zer = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mdadm needs root privileges!\033[0m")
                    sys.exit(1)

                #checking return code for spawned process, if error, don't print message
                if proc.returncode == 0:
                    print("\033[33;1mmdadm: {0} superblock zeroed!\033[0m".format("/dev/"+line.rsplit(' ',1)[1].replace("\n","")))

                proc = Popen(["wipefs", "-a", "/dev/"+line.rsplit(' ',1)[1].replace("\n","")], stdin=PIPE, stdout=DEVNULL, stderr=STDOUT)
                wfs_o, wfs_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error, print customized error message
                if proc.returncode != 0 and wfs_o != None:
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, wfs_o.decode('utf-8')))                  #print program-specific error
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! wipefs needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
                if proc.returncode != 0 and wfs_o == None:
                    print("\033[31;1mERROR! wipefs: {0} unable to read superblock. Is device mounted, swap, or a multidisc array?\033[0m".format("/dev/"+line.rsplit(' ',1)[1].replace("\n","")))                 #print sysadmin error
                    sys.exit(1)

                #print status message
                print("\033[33;1mwipefs: {0} filesystem signatures removed!\033[0m".format("/dev/"+line.rsplit(' ',1)[1].replace("\n","")))


    #NAMES FOR PARTITIONS BELOW ARE SEARCHED FOR BY MDADM UTILITY. 'DB.JSON' CONTAINS FIELDS SPECIFYING INFORMATION FOR MDADM, SPECIFIC TO THE LABELS SET UP BELOW
    #IF THE LABELS ARE CHANGED, CHANGE THE FIELDS IN 'DB.JSON' AND THE CODE HANDLING MDADM
    #SWAP ALLOCATION MADE ON ALL SELECTED DRIVES, THAT WAY RETROACTIVE SWAP RAID 1 IS CONFIGURABLE, AND SWAP RAID 5 OR ANY WITH PARITY, IS CONFIGURED

    print("PARTITIONING SELECTED DRIVES...")
    for i in range(0, len(devices)):
        proc = Popen(["parted", "--script", devices[i], """ \
        mklabel gpt \
        mkpart primary fat32 {0}s {1}s \
        name 1 esp{2} \
        set 1 esp on \
        mkpart primary xfs {3}s {4}s \
        name 2 boot{5} \
        mkpart primary linux-swap {6}s {7}s \
        name 3 swap{8} \
        mkpart primary xfs {9}s {10}s \
        name 4 root{11} \

        """.format(sda1_start, sda1_end, i, sda2_start, sda2_end, i, swap_start, swap_end, i, sda3_start, (sda3_scount+sda3_start), i)], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
        part_o, part_er = proc.communicate()
        #wait for return before continue
        while proc.returncode is None:
            proc.poll()

        # checking return code for spawned process, if error, print customized error message
        if proc.returncode != 0:
            print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, part_o.decode('utf-8').replace("Error","parted")))
            if os.geteuid() != 0:
                print("\033[31;1mERROR! parted needs root privileges!\033[0m")
                sys.exit(1)
            sys.exit(1)

    #check if swap was chosen to be part of raid operation, if so enable flag in parted
    if "EFI" in install_settings['RAID_PART_OPT'] or "ESP" in install_settings['RAID_PART_OPT']:
        try:
            if int(install_settings['RAID_PART_OPT']['EFI']) >= 0:
                for i in range(0, len(devices)):
                    proc = Popen(["parted", "--script", devices[i], """ \
                            set 1 raid on \

                    """], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                    part_o, part_er = proc.communicate()
                    #wait for return before continue
                    while proc.returncode is None:
                        proc.poll()

                    # checking return code for spawned process, if error, print customized error message
                    if proc.returncode != 0:
                        print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, part_o.decode('utf-8').replace("Error", "parted")))
                        if os.geteuid() != 0:
                            print("\033[31;1mERROR! parted needs root privileges!\033[0m")
                            sys.exit(1)
                        sys.exit(1)
        except KeyError:
            if int(install_settings['RAID_PART_OPT']['ESP']) >= 0:
                for i in range(0, len(devices)):
                    proc = Popen(["parted", "--script", devices[i], """ \
                            set 1 raid on \

                    """], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                    part_o, part_er = proc.communicate()
                    #wait for return before continue
                    while proc.returncode is None:
                        proc.poll()

                    #checking return code for spawned process, if error, print customized error message
                    if proc.returncode != 0:
                        print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, part_o.decode('utf-8').replace("Error", "parted")))
                        if os.geteuid() != 0:
                            print("\033[31;1mERROR! parted needs root privileges!\033[0m")
                            sys.exit(1)
                        sys.exit(1)

    #check if esp/efi was chosen to be part of raid operation, if so enable flag in parted
    if "BOOT" in install_settings['RAID_PART_OPT'] and int(install_settings['RAID_PART_OPT']['BOOT']) >= 0:
        for i in range(0, len(devices)):
            proc = Popen(["parted", "--script", devices[i], """ \
                    set 2 raid on \

            """], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            part_o, part_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, part_o.decode('utf-8').replace("Error","parted")))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! parted needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)

    #check if boot was chosen to be part of raid operation, if so enable flag in parted
    if "SWAP" in install_settings['RAID_PART_OPT'] and int(install_settings['RAID_PART_OPT']['SWAP']) >= 0:
        for i in range(0, len(devices)):
            proc = Popen(["parted", "--script", devices[i], """ \
                    set 3 raid on \

            """], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            part_o, part_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            #checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, part_o.decode('utf-8').replace("Error","parted")))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! parted needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)

    #check if swap was chosen to be part of raid operation, if so enable flag in parted
    if "ROOT" in install_settings['RAID_PART_OPT'] and int(install_settings['RAID_PART_OPT']['ROOT']) >= 0:
        for i in range(0, len(devices)):                                #swap is the last partition, configured above as '3'
            proc = Popen(["parted", "--script", devices[i], """ \
                    set 4 raid on \

            """], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            part_o, part_er = proc.communicate()
            #wait for return before continue
            while proc.returncode is None:
                proc.poll()

            # checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, part_o.decode('utf-8').replace("Error","parted")))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! parted needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)

    print("INITIALIZING MDADM ARRAYS...")
    for a, b in enumerate(disk_dict["DISK"].keys()):
        md_pos = 0
        for i in range(0, len(parted.newDisk(parted.getDevice(devices[a])).partitions)):                                                                            #operation begin
            disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)] = {}
            disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)]["PART_GEOM"] = (parted.newDisk(parted.getDevice(devices[a])).partitions[i].geometry.start, parted.newDisk(parted.getDevice(devices[a])).partitions[i].geometry.end, parted.newDisk(parted.getDevice(devices[a])).partitions[i].geometry.length)
            disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)]["NAME"] = re.sub("[0-9]", "", parted.newDisk(parted.getDevice(devices[a])).partitions[i].name).upper()
            disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)]["MDNUM"] = -1  #set to no raid for initialization

            if disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)]["NAME"] in install_settings["RAID_PART_OPT"] and int(install_settings["RAID_PART_OPT"][disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)]["NAME"]]) >= 0:
                disk_dict["DISK"][b]["PARTITION"][b + "{0}".format(i + 1)]["MDNUM"] = md_pos
                md_pos+=1

    #THE FOLLOWING TWO LISTS ARE USED AS CHECKLISTS FOR MKFS. THE DATA WILL BE IN THE 'MASTER.JSON'. HOWEVER THE VALUES WILL BE REMOVED AS THE PARTITIONS ARE CREATED
    disk_dict["INFO"]["MD_PLIST"] = []           #creation of aggregate list for all md devices. an aggregate list of all partitions involved in a md array
    disk_dict["INFO"]["IND_PLIST"] = []          #creation of aggregate list for all scsi devices. an aggregate list of all partitions not involved in a md array
    #INITIALIZE LIST FOR MDARRAY IN DICT, ACCORDING TO INSTALLATION DEFINITION
    for i0, k0 in enumerate(install_settings["RAID_PART_OPT"].keys()):
        if int(install_settings["RAID_PART_OPT"][k0]) >= 0:                     #only add if value indicates specified for raid
            disk_dict["MDARRAY"]["/dev/md{0}".format(i0)] = {}
            disk_dict["MDARRAY"]["/dev/md{0}".format(i0)]["DEVLIST"] = []
            disk_dict["MDARRAY"]["/dev/md{0}".format(i0)]["NAME"] = k0.upper()                                      #should already be in 'UPPER' format. double checking...
            disk_dict["MDARRAY"]["/dev/md{0}".format(i0)]["LEVEL"] = int(install_settings["RAID_PART_OPT"][k0])     #conversion of raid type to int. double checking...

    #CONSTRUCT MDARRY LIST IN DICT
    for k0 in disk_dict["MDARRAY"].keys():
        for k1 in disk_dict["DISK"].keys():
            for k2 in disk_dict["DISK"][k1]["PARTITION"].keys():
                if disk_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == disk_dict["MDARRAY"][k0]["NAME"] and int(install_settings["RAID_PART_OPT"][disk_dict["MDARRAY"][k0]["NAME"]]) >= 0:
                    disk_dict["MDARRAY"][k0]["DEVLIST"].append(k2)
                    disk_dict["INFO"]["MD_PLIST"].append(k2)

    for k0 in disk_dict["DISK"].keys():
        for k1 in disk_dict["DISK"][k0]["PARTITION"].keys():
            if not k1 in disk_dict["INFO"]["MD_PLIST"]:
                disk_dict["INFO"]["IND_PLIST"].append(k1)


    #write disc dictionary to file
    with open('master.json', 'w') as m:
        json.dump(disk_dict, m)
    m.close()

    for k0 in disk_dict["MDARRAY"].keys():
        if len(disk_dict["MDARRAY"][k0]["DEVLIST"]) > 1:
           
            #removed EFI RAID guard 02.02.2019 
            #if disk_dict["MDARRAY"][k0]["NAME"] == "EFI" or disk_dict["MDARRAY"][k0]["NAME"] == "ESP" or disk_dict["MDARRAY"][k0]["NAME"] == "BOOT":
                #print("\033[31;1mERROR! WILL NOT CREATE RAID ARRAY DESIGNATED FOR ESP/EFI OR BOOT PARTITIONS. RISK OF CORRUPTION. EXITING!\033[0m")
                #sys.exit(1)

            #EFI RAID Accomodation added 02.02.2019. Will use older superblock format, which is written to the end of filesystem. EFI is picky about location, so I have to accomodate
            if disk_dict["MDARRAY"][k0]["NAME"] == "EFI" or disk_dict["MDARRAY"][k0]["NAME"] == "ESP":
                proc = Popen(shlex.split("""mdadm --create {0} -f --metadata=1.0 --name={1} --level={2} --raid-devices={3} {4}""".format(k0, disk_dict["MDARRAY"][k0]["NAME"].lower()+k0.replace("/dev/",""), disk_dict["MDARRAY"][k0]["LEVEL"], len(devices), ' '.join(disk_dict["MDARRAY"][k0]["DEVLIST"]))), stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            else:
                proc = Popen(shlex.split("""mdadm --create {0} -f --metadata=1.2 --name={1} --level={2} --raid-devices={3} {4}""".format(k0, disk_dict["MDARRAY"][k0]["NAME"].lower()+k0.replace("/dev/",""), disk_dict["MDARRAY"][k0]["LEVEL"], len(devices), ' '.join(disk_dict["MDARRAY"][k0]["DEVLIST"]))), stdin=PIPE, stdout=PIPE, stderr=STDOUT)

            md_o, md_e = proc.communicate()
            # wait for return before continue
            while proc.returncode is None:
                proc.poll()

            # checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:        #multiple error codes possible
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, md_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mdadm needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
            #print success message from mdadm
            print("\033[32;1m{0}\033[0m".format(md_o.decode('utf-8').rsplit('\n',1)[0]))

    #FORMAT DISKS AND MDADM ARRAYS
    print("FORMATTING DISKS...")
    for k1 in disk_dict["DISK"].keys():
        for k2 in disk_dict["DISK"][k1]["PARTITION"].keys():
            if k2 in disk_dict["INFO"]["IND_PLIST"] and not k2 in disk_dict["INFO"]["MD_PLIST"] and disk_dict["DISK"][k1]["PARTITION"][k2]["NAME"] != "SWAP":       #found individual partition to format
                if disk_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "ESP" or disk_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "EFI":
                    mkfs_cmd = "mkfs.fat -F32 " + k2
                else:
                    mkfs_cmd = "mkfs.xfs -f " + k2

                proc = Popen(shlex.split(mkfs_cmd), stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                mkfs_o, mkfs_er = proc.communicate()
                #wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                #checking return code for spawned process, if error, print customized error message
                if proc.returncode != 0:
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mkfs_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mkfs needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
                disk_dict["INFO"]["IND_PLIST"].remove(k2)                               #marking partition as setup
                print("\033[32;1mformatted device {0}\033[0m".format(k2))               #print success message

    for k0 in disk_dict["MDARRAY"].keys():
        for k1 in disk_dict["DISK"].keys():
            for k2 in disk_dict["DISK"][k1]["PARTITION"].keys():
                if k2 in disk_dict["INFO"]["MD_PLIST"] and k2 in disk_dict["MDARRAY"][k0]["DEVLIST"] and disk_dict["MDARRAY"][k0]["NAME"] != "SWAP":             #found mdadm array to format
                    #removed EFI RAID guard 02.02.2019
                    #if disk_dict["MDARRAY"][k0]["NAME"] == "EFI" or disk_dict["MDARRAY"][k0]["NAME"] == "ESP" or disk_dict["MDARRAY"][k0]["NAME"] == "BOOT":
                        #print("\033[31;1mERROR! WILL NOT FORMAT RAID DESIGNATED FOR ESP/EFI OR BOOT PARTITIONS. RISK OF CORRUPTION. EXITING!\033[0m")
                        #sys.exit(1)

                    #EFI RAID Accomodation added 02.02.2019

                    if disk_dict["MDARRAY"][k0]["NAME"] == "EFI" or disk_dict["MDARRAY"][k0]["NAME"] == "ESP":
                        mkfs_cmd = "mkfs.fat -F32 " + k0
                    else:
                        mkfs_cmd = "mkfs.xfs -f " + k0
                    
                    proc = Popen(shlex.split(mkfs_cmd), stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                    mkfs_o, mkfs_er = proc.communicate()
                    #wait for return before continue
                    while proc.returncode is None:
                        proc.poll()

                    #checking return code for spawned process, if error, print customized error message
                    if proc.returncode != 0:
                        print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mkfs_o.decode('utf-8')))
                        if os.geteuid() != 0:
                            print("\033[31;1mERROR! mkfs needs root privileges!\033[0m")
                            sys.exit(1)
                        sys.exit(1)
                    for k3 in disk_dict["MDARRAY"][k0]["DEVLIST"]:
                        if k3 in disk_dict["INFO"]["MD_PLIST"]:
                            disk_dict["INFO"]["MD_PLIST"].remove(k3)                         #marking md array as setup
                    print("\033[32;1mformatted device {0}\033[0m".format(k0))                #print success message

    with open('master.json', 'r') as t:
        temp_db = json.load(t)
    t.close()

    print("CONFIGURING/ENABLING SWAP...")
    swap_set = 0
    for k0 in disk_dict["MDARRAY"].keys():
        if disk_dict["MDARRAY"][k0]["NAME"] == "SWAP" and swap_set == 0:    #configure swap as raid
            proc = Popen(["mkswap", k0], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            mkswp_o, mkswp_er = proc.communicate()
            # wait for return before continue
            while proc.returncode is None:
                proc.poll()

            # checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mkswp_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! mkswap needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
            proc = Popen(["swapon", k0], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
            swpon_o, swpon_er = proc.communicate()
            # wait for return before continue
            while proc.returncode is None:
                proc.poll()

            # checking return code for spawned process, if error, print customized error message
            if proc.returncode != 0:
                print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, swpon_o.decode('utf-8')))
                if os.geteuid() != 0:
                    print("\033[31;1mERROR! swapon needs root privileges!\033[0m")
                    sys.exit(1)
                sys.exit(1)
            swap_set = 1
            print("\033[32;1mswap created and enabled on {0}\033[0m".format(k0))            #print success message
            temp_db["INFO"]["SWAP_EN"] = k0
            with open('master.json', 'w') as y:                                             #update db with enabled swap field
                json.dump(temp_db, y)
            y.close()

    for k1 in disk_dict["DISK"].keys():
        for k2 in disk_dict["DISK"][k1]["PARTITION"].keys():
            if disk_dict["DISK"][k1]["PARTITION"][k2]["NAME"] == "SWAP" and swap_set == 0:  #configure swap as no raid
                proc = Popen(["mkswap", k2], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                mkswp_o, mkswp_er = proc.communicate()
                # wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                # checking return code for spawned process, if error, print customized error message
                if proc.returncode != 0:
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, mkswp_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! mkswap needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)

                proc = Popen(["swapon", k2], stdin=PIPE, stdout=PIPE, stderr=STDOUT)
                swpon_o, swpon_er = proc.communicate()
                # wait for return before continue
                while proc.returncode is None:
                    proc.poll()

                # checking return code for spawned process, if error, print customized error message
                if proc.returncode != 0:
                    print("\033[31;1mERROR! code {0}\n{1}\033[0m".format(proc.returncode, swpon_o.decode('utf-8')))
                    if os.geteuid() != 0:
                        print("\033[31;1mERROR! swapon needs root privileges!\033[0m")
                        sys.exit(1)
                    sys.exit(1)
                swap_set = 1
                print("\033[32;1mswap created and enabled on {0}\033[0m".format(k2))            #print success message
                temp_db["INFO"]["SWAP_EN"] = k2
                with open('master.json', 'w') as z:                                             #update db with enabled swap field
                    json.dump(temp_db, z)
                z.close()

    return 0

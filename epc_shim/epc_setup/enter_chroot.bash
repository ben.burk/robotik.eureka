#!/bin/bash

#author : Benjamin Burk
#details : EPC Setup ENTER_CHROOT script. CATALOGS ALL CREATED FILESYSTEMS TO DB, BUILDS MDADM CONF, SETS UP AND ENTERS CHROOT
#date : 13 MARCH 2019
#copyright : BURKTECH 2019

#THIS SCRIPT IS CALLED FROM 'epc_setup'
#'chroot.bash' is placed into the chroot of the installed system, 'root_mnt', and executed

root_mnt="/mnt"
boot_mnt="/mnt/boot"
esp_mnt="/mnt/boot/efi"

if [ $(id -u) -ne 0 ]; then										#check for effective user
	echo -e "\033[1;31mERROR! finalization script must be called as root\033[0m"
	exit 1
fi
grep -qs "$root_mnt " /proc/mounts
if [[ "$?" != "0" ]]; then								#check for mounted filesystem
	echo -e "\033[1;31mERROR! finalization script cannot operate on $root_mnt. Is filesystem mounted?\033[0m"
	exit 1
fi
grep -qs "$boot_mnt " /proc/mounts
if [[ "$?" != "0" ]]; then
	echo -e "\033[1;31mERROR! finalization script cannot operate on $boot_mnt. Is filesystem mounted?\033[0m"
	exit 1
fi
grep -qs "$efi_mnt " /proc/mounts
if [[ "$?" != "0" ]]; then
	echo -e "\033[1;31mERROR! finalization script cannot operate on $esp_mnt. Is filesystem mounted?\033[0m"
	exit 1
fi

args=("$@")
args_ele=${#args[@]}

#preliminary error check for argument number
if [[ $# -lt 2 ]] || [[ $(($#%2)) -ne 0 ]]; then
	echo -e "\033[1;31mERROR! finalization script must be called with at least 2 arguments\033[0m"
	echo -e "\033[1;31mERROR! finalization script must be called with arguments in pairs (i.e multiples of 2)\033[0m"
	printf "\033[1;31mARGS:\t\033[0m"
	for (( i=0;i<$args_ele;i++ )); do
		printf "\033[1;31m ${args[${i}]}\033[0m"
	done
	printf "\n"
	exit 1
fi

#preliminary error check for formatting
for (( i=0;i<$args_ele;i++ )); do
	if [[ "${args[${i}]}" = *"/"* ]] && [ $(($i%2)) -eq 0 ]; then
		echo -e "\033[1;31mERROR! finalization script arguments invalid\033[0m"
		echo -e "\033[1;31mExample: $0 name1 device1 name2 device2\033[0m"
		echo -e "\033[1;31mname format cannot contain '/' character\033[0m"
		echo -e "\033[1;31mdevice format must contain '/' character\033[0m"
		printf "\033[1;31mARGS:\t\033[0m"
		for (( i=0;i<$args_ele;i++ )); do
			printf "\033[1;31m ${args[${i}]}\033[0m"
		done
		printf "\n"
		exit 1
	fi
	if [[ "${args[${i}]}" != *"/"* ]] && [ $(($i%2)) -ne 0 ]; then
		echo -e "\033[1;31mERROR! finalization script arguments invalid\033[0m"
		echo -e "\033[1;31mExample: $0 name1 device1 name2 device2\033[0m"
		echo -e "\033[1;31mname format cannot contain '/' character\033[0m"
		echo -e "\033[1;31mdevice format must contain '/' character\033[0m"
		printf "\033[1;31mARGS:\t\033[0m"
		for (( i=0;i<$args_ele;i++ )); do
			printf "\033[1;31m ${args[${i}]}\033[0m"
		done
		printf "\n"
		exit 1
	fi
done

dev_c_arr=()
c_arr_ind=0
md_n=$(cat /proc/partitions | grep md | wc -l)
sd_n=$(cat /proc/partitions | grep sd | wc -l)

echo "PARSING FS UUIDS..."
#configure commands to run lsblk
for (( j=1;j<$args_ele;j+=2 )); do
	is_op=0
	for (( k=0;k<$md_n;k++ )); do					#md used as selector
		if [[ "${args[${j}]}" = "/dev/"$(cat /proc/partitions | grep md | head "-$((k+1))" | tail -1 | sed 's/.* //') ]] && [ $is_op -eq 0 ]; then	#add md UUID to array
			dev_c_arr[$c_arr_ind]="${args[$((j-1))]}"

			if [[ "$(cat /proc/swaps | grep -v "Filename" | sed 's/ .*//')" != *"${args[${j}]}"* ]] && [[ "$(echo "${args[$((j-1))]}" | awk '{print toupper($0)}')" = "SWAP" ]]; then		#error handling
				echo -e "\033[1;31mERROR! ${args[${j}]} device/partition not found configured as swap\033[0m"												#if mdarray was erroneously marked as swap, and is not populated, throw error and exit
				exit 1
			fi
			if [[ "$(mdadm --detail "${args[${j}]}" | grep "Total Devices" | sed 's/.*://' | sed 's/.*\ //')" != "$(lsblk -o NAME,UUID | grep "$(echo "${args[${j}]}" | sed 's/^\(\/dev\/\)*//')" | wc -l)" ]] && [ $(lsblk -o NAME,UUID | grep "$(echo "${args[${j}]}" | sed 's/^\(\/dev\/\)*//')" | wc -l) -gt 0 ]; then
				#found possibility of degraded raid/disks during setup. continue with warning. construction of disks could possibly cause this
				echo -e "\033[1;33mWARNING! ${args[${j}]} array in degraded condition! MDARRAY UUID FOUND! Recommend verifying RAID status after sync. Continuing...\033[0m"
			fi
			if [[ "$(mdadm --detail "${args[${j}]}" | grep "Total Devices" | sed 's/.*://' | sed 's/.*\ //')" != "$(lsblk -o NAME,UUID | grep "$(echo "${args[${j}]}" | sed 's/^\(\/dev\/\)*//')" | wc -l)" ]] && [ $(lsblk -o NAME,UUID | grep "$(echo "${args[${j}]}" | sed 's/^\(\/dev\/\)*//')" | wc -l) -eq 0 ]; then
				#found inoperability of drive. unable to find mdarray UUID. exit with error message
				echo -e "\033[1;31mERROR! ${args[${j}]} array in degraded condition! System unable to find UUID\033[0m"
				exit 1
			fi

			dev_c_arr[$((c_arr_ind+1))]="$(lsblk -o NAME,UUID | grep "$(echo "${args[${j}]}" | sed 's/^\(\/dev\/\)*//')" | sed 's/.* //' | sort -u)"
			dev_c_arr[$((c_arr_ind+2))]="${args[${j}]}"
			c_arr_ind=$((c_arr_ind+3))
			is_op=1
		fi
	done
	for (( l=0;l<$sd_n;l++ )); do					#sd used as selector
		if [[ "${args[${j}]}" = "/dev/"$(cat /proc/partitions | grep sd | head "-$((l+1))" | tail -1 | sed 's/.* //') ]] && [ $is_op -eq 0 ]; then	#add sd UUID to array
			dev_c_arr[$c_arr_ind]="${args[$((j-1))]}"

			if [[ "$(cat /proc/swaps | grep -v "Filename" | sed 's/ .*//')" != *"${args[${j}]}"* ]] && [[ "$(echo "${args[$((j-1))]}" | awk '{print toupper($0)}')" = "SWAP" ]]; then		#error handling
				echo -e "\033[1;31mERROR! ${args[${j}]} partition not found configured as swap\033[0m"												#if partition was erroneously marked as swap, and is not populated, throw error and exit
				exit 1
			fi

			dev_c_arr[$((c_arr_ind+1))]="$(lsblk -o NAME,UUID | grep "$(echo "${args[${j}]}" | sed 's/^\(\/dev\/\)*//')" | sed 's/.* //')"

			if [[ "${dev_c_arr[$((c_arr_ind+1))]}" = "" ]]; then																	#error handling
				echo -e "\033[1;31mERROR! UUID could not be found for ${args[${j}]} partition\033[0m"												#if partition UUID cannot be found, throw error and exit
				exit 1
			fi

			dev_c_arr[$((c_arr_ind+2))]="${args[${j}]}"
			c_arr_ind=$((c_arr_ind+3))
			is_op=1
		fi
	done
done

uuid_file="/root/epc_setup/partuuid.json"
if [ -f $uuid_file ]; then		#delete if file exists. starting from clean slate
	rm -f $uuid_file
fi

echo "{" >> $uuid_file			#construction of fs uuid database
for (( i=2;i<$c_arr_ind;i+=3 )); do
	if [ $((i+3)) -gt $c_arr_ind ]; then
		delim=""
	else
		delim=","
	fi
	echo -e "\"p$(((i+1)/3))\":{\"NAME\":\"${dev_c_arr[$((i-2))]}\",\"UUID\":\"${dev_c_arr[$((i-1))]}\",\"DEVICE\":\"${dev_c_arr[${i}]}\"}$delim" >> $uuid_file
done
echo "}" >> $uuid_file

#CONSTRUCTION OF MDADM CONF FILE. SOME DISTRIBUTIONS OF LINUX HAVE THE CONFIGURATION FILE IN DIFFERENT PLACES. RUN BEFORE CHROOT, TO ENSURE THE ARRAYS ARE SEEN
#I.E. DEBIAN IS IN '/ETC/MDADM/MDADM.CONF'. CENTOS '/ETC/MDADM.CONF'. THE PLACEMENT OF THE CONFIGURATION FILE WILL BE DONE IN 'CHROOT.SH'
#NO NEED TO CHECK IF MDARRAYS ARE RUNNING, IT IS DONE AT THE BEGINNING
#writing mdadm.conf header
echo "# mdadm configuration file" > mdadm_file
echo "#" >> mdadm_file
echo "# mdadm will function properly without the use of a configuration file," >> mdadm_file
echo "# but this file is useful for keeping track of arrays and member disks." >> mdadm_file
echo "# In general, a mdadm.conf file is created, and updated, after arrays" >> mdadm_file
echo "# are created. This is the opposite behavior of /etc/raidtab which is" >> mdadm_file
echo "# created prior to array construction." >> mdadm_file
echo "MAILADDR root" >> mdadm_file
mdadm --detail --scan >> mdadm_file

echo "BACKING UP DISK GPT TABLES..."
master_file="/root/epc_setup/master.json"
for (( i=0;i<$(cat "$master_file" | jq '.DISK | length');i++ )); do
	sgdisk --backup=/root/epc_setup/"$(jq -r ".DISK | keys[$i]" $master_file | sed 's/^\(\/dev\/\)*//')".table "$(jq -r ".DISK | keys[$i]" $master_file)" >/dev/null 2>&1
done

mkdir -p "$root_mnt/dev"
mkdir -p "$root_mnt/dev/pts"
mkdir -p "$root_mnt/proc"
mkdir -p "$root_mnt/run"
mkdir -p "$root_mnt/sys"
mkdir -p "$root_mnt/tmp"
mount --bind /dev "$root_mnt/dev" &&
mount -t devpts devpts "$root_mnt/dev/pts" &&
mount --bind /proc "$root_mnt/proc" &&
mount --bind /sys "$root_mnt/sys" &&
mount --bind /tmp "$root_mnt/tmp" &&
cp /root/epc_setup/partuuid.json "$root_mnt"
cp /root/epc_setup/master.json "$root_mnt"					#included as backup. information for fs repair, if necessary in the future
cp /root/epc_setup/mdadm_file "$root_mnt"
cp /root/epc_setup/chroot.bash "$root_mnt"
cp /root/epc_setup/*.table "$root_mnt"
echo "ENTERING CHROOT..."
chroot "$root_mnt" ./chroot.bash

#return from chroot

echo -e "\033[1;32mSUCCESS! EUREKA (EPC) SYSTEM INSTALLED!\033[0m"
umount "$esp_mnt" ; umount "$boot_mnt" ; rm -f "$root_mnt/chroot.bash" ; umount -lf "$root_mnt" ; rm -f mdadm_file
echo -e "\033[1;33mMDADM WILL NOW SYNC DISKS (IF APPLICABLE)... SYSTEM WILL REBOOT WHEN FINISHED...\033[0m"
sleep 3

#mdadm wait to reboot until raid synced
while [[ ! -z "$(cat /proc/mdstat | grep "resync")" ]]; do
	clear
	echo -e "\033[1;33mMDADM SYNC IN PROGRESS... SYSTEM WILL REBOOT WHEN FINISHED...\033[0m"
	cat /proc/mdstat
	sleep 30
done

clear
echo -e "\033[1;32mREBOOTING...\033[0m"
echo "" ; echo ""
echo -e "\033[1;31mTHANK YOU FOR INSTALLING EPC (EUREKA)!\033[0m"
echo -e "\033[1;31mBROUGHT TO YOU BY:\033[0m" ; echo "" ; echo "" ; echo ""; echo ""
cat /root/epc_setup/robotik_logo
( sleep 60 ; reboot ) &	#reboot
exit 0

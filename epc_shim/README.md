Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

NOTE: SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS

	debian_live_bootstrap:		live distro of linux to chainload the EPC deployment process	
	epc_setup:			conditioning code for disks before/during tarball (single filesystem) deployment. THIS CODE DOES NOT MODIFY EPC/OCULUS.
	epc_provision:			production provisioning scripts.

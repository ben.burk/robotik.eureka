Benjamin Burk

Projects during employment at Robotik Technology
06/2018 - 04/2019

NOTE : SECTIONS HAVE BEEN REDACTED WHERE NECESSARY TO PROTECT TRADEMARKS

	epc_grab_depend.bash:			grabs dependencies for system. builds and installs required components. configures needed services
	epc_provision.bash:			provisions system for production. enters required data to db.
	epc_db_provision.sql.template:		template file to configure system services
	epc_add_automation.sql.template:	template file to add automation	


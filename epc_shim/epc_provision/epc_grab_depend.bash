#!/bin/bash

#author : Benjamin Burk
#date : FEB 25 2019
#details : EPC setup script. GRABS ALL DEPENDENCIES NEEDED FOR EPC AND OCULUS. INSTALLS BASE CODE, CONFIGURES WEB INTERFACE.

#06.29.2019 UPDATE.	Moved VNC Server activation to provision script. Will be prepared/configured when a hostname is specified
#SECTIONS HAVE BEEN REDACTED TO PROTECT TRADEMARKS

BUILD_PREFIX="/home/eAdmin"
DEPEND_LOG="/root/eureka_depend.log"

#remove all but latest 2 kernels
sudo package-cleanup -y --oldkernels --count=2 | sudo tee "$DEPEND_LOG"

#extract main snapshot
cd $BUILD_PREFIX
sudo unzip sandbox.zip | sudo tee -a "$DEPEND_LOG" >/dev/null 2>&1

#extract EPC tarballs
sudo tar -C "$BUILD_PREFIX/sandbox/" -xvf "$BUILD_PREFIX/sandbox/********.tar" | sudo tee -a "$DEPEND_LOG" >/dev/null 2>&1
sudo tar -C "$BUILD_PREFIX/sandbox/" -xvf "$BUILD_PREFIX/sandbox/********.tar" | sudo tee -a "$DEPEND_LOG" >/dev/null 2>&1
sudo tar -C "$BUILD_PREFIX/sandbox/" -xvf "$BUILD_PREFIX/sandbox/********.tar" | sudo tee -a "$DEPEND_LOG" >/dev/null 2>&1

#update yum (get up to date list of packages in repo)
sudo yum -y update | sudo tee -a "$DEPEND_LOG"

#start dependency script
cd $BUILD_PREFIX/sandbox/********
sudo bash eureka_build.bash | sudo tee -a "$DEPEND_LOG"
cd $BUILD_PREFIX/sandbox

sudo python3 -m pip install scikit-image scipy mysql-connector | sudo tee -a "$DEPEND_LOG"

#grab mysql connector, extract and install with python3
sudo wget -O "$BUILD_PREFIX/sandbox/mysql-connector-python-8.0.12.tar.gz" "https://dev.mysql.com/get/Downloads/Connector-Python/mysql-connector-python-8.0.12.tar.gz" | sudo tee -a "$DEPEND_LOG"
sudo tar -C "$BUILD_PREFIX/sandbox" -xzvf "$BUILD_PREFIX/sandbox/mysql-connector-python-8.0.12.tar.gz" | sudo tee -a "$DEPEND_LOG"
sudo chown -R eAdmin:eureka "$BUILD_PREFIX/sandbox/mysql-connector-python-8.0.12"
cd "$BUILD_PREFIX/sandbox/mysql-connector-python-8.0.12"
sudo python3 setup.py install | sudo tee -a "$DEPEND_LOG"
cd "$BUILD_PREFIX/sandbox"
sudo rm -rf "$BUILD_PREFIX/sandbox/mysql-connector-python-8.0.12"

#grab driver for Kyocera printer and install
sudo mkdir -p /usr/share/cups/model/Kyocera
sudo wget -O "$BUILD_PREFIX/sandbox/Linux_Ecosys_M-P6x3xcdn.zip" "https://cdn.kyostatics.net/dlc/eu/driver/all/linux_ecosys_m-p6x3xcdn.-downloadcenteritem-Single-File.downloadcenteritem.tmp/Linux_Ecosys_M-P6x3xcdn.zip" | sudo tee -a "$DEPEND_LOG"
sudo unzip "$BUILD_PREFIX/sandbox/Linux_Ecosys_M-P6x3xcdn.zip" -d "$BUILD_PREFIX/sandbox/" | sudo tee -a "$DEPEND_LOG" >/dev/null 2>&1
sudo cp -r $BUILD_PREFIX/sandbox/Linux/* /usr/share/cups/model/Kyocera
sudo chown -R root:root /usr/share/cups/model/Kyocera
#cleanup
sudo rm -rf "$BUILD_PREFIX/sandbox/Linux"

#make and install EPC base code
cd $BUILD_PREFIX/sandbox/********
sudo make install | sudo tee -a "$DEPEND_LOG" >/dev/null 2>&1

#init db (initialize and create database)
cd /opt/********
sudo bash eureka_db_setup.bash -c | sudo tee -a "$DEPEND_LOG"
cd $BUILD_PREFIX

#make and install Oculus base code
cd $BUILD_PREFIX/sandbox/********
sudo make install | sudo tee -a "$DEPEND_LOG"

#install apache root conf (Eureka)
sudo cp $BUILD_PREFIX/sandbox/********/eureka_httpd_36.conf /etc/httpd/conf.d/
sudo chown root:root /etc/httpd/conf.d/eureka_httpd_36.conf
sudo systemctl start httpd.service
sudo systemctl enable httpd.service

#enable Oculus in web code
cd /opt/********
sudo cp settings_local.py.default settings_local.py
sudo chown eService:apache settings_local.py
sudo sed -i 's/OCULUS_INSTALLED\ =\ False/OCULUS_INSTALLED\ =\ True/' settings_local.py
sudo sed -i 's/OCULUS_INSTALLED\ =\ False/OCULUS_INSTALLED\ =\ True/' settings.py

###REDACTED###
###REDACTED###

#enable all Eureka services at startup
sudo systemctl enable eureka_db_helper.service | sudo tee -a "$DEPEND_LOG"
sudo systemctl enable eureka_interface.service | sudo tee -a "$DEPEND_LOG"
sudo systemctl enable eureka_impression.service | sudo tee -a "$DEPEND_LOG"
sudo systemctl enable eureka_printing.service | sudo tee -a "$DEPEND_LOG"
sudo systemctl enable oculus_acquire.service | sudo tee -a "$DEPEND_LOG"
sudo systemctl enable oculus_evaluation.service | sudo tee -a "$DEPEND_LOG"

#start all Eureka services
sudo systemctl start eureka_db_helper.service
sudo systemctl start eureka_interface.service
sudo systemctl start eureka_impression.service
sudo systemctl start eureka_printing.service
sudo systemctl start oculus_acquire.service
sudo systemctl start oculus_evaluation.service
sudo systemctl daemon-reload	#<--- reload for current systemd context

echo -e "\033[1;32mEPC/OCULUS CODEBASE AND DEPENDENCIES LOADED SUCCESSFULLY!\033[0m" | sudo tee -a "$DEPEND_LOG"
echo -e "\033[1;33mREMEMBER TO RUN DB PERSONALITY SCRIPT, AND INSTALL OPENVPN KEYS...\033[0m" | sudo tee -a "$DEPEND_LOG"

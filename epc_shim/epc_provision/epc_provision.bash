#!/bin/bash

#author : Benjamin Burk
#date : MAR 16 2019
#details : EPC provision script. Sets up autofs mounts, configures DB fields, etc

#06.29.2019 UPDATE.	Fixed VNC server setup. Remove any existing Xauthority file, or stale PID/LOGS that may exist. System will create upon login on boot.
#SECTIONS HAVE BEEN REDACTED TO PROTECT TRADEMARKS

PROV_PREFIX="/home/eAdmin"

# Test an IP address for validity:
# Usage:
#      valid_ip IP_ADDRESS
#      if [[ $? -eq 0 ]]; then echo good; else echo bad; fi
#   OR
#      if valid_ip IP_ADDRESS; then echo good; else echo bad; fi
#
function valid_ip()
{
    local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

hostn=$(cat /etc/hostname)
printf 'Enter Hostname: '
read host_name
if [ -z "$host_name" ]; then
	echo "Skipped setting hostname..."
else	
	sudo sed -i s/$hostn/$host_name/g /etc/hosts
	sudo sed -i s/$hostn/$host_name/g /etc/hostname
	
	#start and enable VNC service
	#delete Xauthority file and any PID/LOG files that remain.
	
	#stop service if it is active
	if [[ "$(sudo systemctl is-active vncserver@:1.service)" == "active" ]]; then
		sudo systemctl stop vncserver@:1.service
	fi

	sudo rm -f "$PROV_PREFIX"/.Xauthority
	find "$PROV_PREFIX"/.vnc/ -maxdepth 1 -type f \( -name "*.log" -o -name "*.pid" \) -delete
	
	sudo systemctl enable vncserver@:1.service
	sudo systemctl daemon-reload	#<----- reload for current systemd context

	#start service if it is inactive
	if [[ "$(sudo systemctl is-active vncserver@:1.service)" == "inactive" ]]; then
		sudo systemctl start vncserver@:1.service
	fi
fi	
printf 'Enter ATMS IP Address: '
read atms_ip
if ! valid_ip $atms_ip; then printf 'ATMS IP Address Invalid!\n'; exit 1; fi
printf 'Enter Number of BAPs: '
read bap_num

reg='^[0-9]+$'
if ! [[ $bap_num =~ $reg ]]; then
	printf '%s is not a valid number!\n' '$bap_num'
	exit 1
fi

#remove any existing config files
if [ -f /etc/auto.ap ]; then
	sudo rm -f /etc/auto.ap
fi
if [ -f /etc/auto.atms ]; then
	sudo rm -f /etc/auto.atms
fi

#set values in autofs. ATMS needs to be moved a separate loop for multiple ATMS functionality.
sudo cp /etc/auto.atms.template /etc/auto.atms
sudo sed -i 's/Z.Z.Z.Z/'"$atms_ip"'/g' /etc/auto.atms

if [ -f ./epc_db_provision.sql ]; then
	rm -f ./epc_db_provision.sql
fi

cp ./epc_db_provision.sql.template ./epc_db_provision.sql

#set values in DB
sed -i 's/X.X.X.X/'"$atms_ip"'/g' ./epc_db_provision.sql
psql ******** -v "ON_ERROR_STOP=1" -f ./epc_db_provision.sql
ret="$?"	#grab return code to check for error.

if [[ "$ret" == "0" ]]; then
	for (( i=0;i<$bap_num;i++ )); do
		printf 'Enter #%s BAP IP Address: ' "$((i+1))"
		read bap_ip
		if ! valid_ip $bap_ip; then printf 'BAP IP Address Invalid!\n'; exit 1; fi

		if [ -f ./epc_add_automation.sql ]; then
			rm -f ./epc_add_automation.sql
		fi
	
		sudo bash -c "cat /etc/auto.ap.template >> /etc/auto.ap"
		sudo sed -i 's/YYYY/'"$((i+1))"'/g' /etc/auto.ap
		sudo sed -i 's/Y.Y.Y.Y/'"$bap_ip"'/g' /etc/auto.ap

		cp ./epc_add_automation.sql.template ./epc_add_automation.sql
		sed -i 's/YYYY/'"$((i+1))"'/g' ./epc_add_automation.sql
		sed -i 's/X.X.X.X/'"$atms_ip"'/g' ./epc_add_automation.sql
		psql ******** -v "ON_ERROR_STOP=1" -f ./epc_add_automation.sql
		ret1="$?"
		
		if [[ "$ret1" != "0" ]]; then
			echo -e "\033[1;31mERROR! Unable to add new automation!\033[0m"
		fi
	done
else
	for (( i=0;i<$bap_num;i++ )); do
		printf 'Enter #%s BAP IP Address: ' "$((i+1))"
		read bap_ip
		if ! valid_ip $bap_ip; then printf 'BAP IP Address Invalid!\n'; exit 1; fi
		
		if [ -f ./epc_add_automation.sql ]; then
			rm -f ./epc_add_automation.sql
		fi
		
		sudo bash -c "cat /etc/auto.ap.template >> /etc/auto.ap"
		sudo sed -i 's/YYYY/'"$((i+1))"'/g' /etc/auto.ap
		sudo sed -i 's/Y.Y.Y.Y/'"$bap_ip"'/g' /etc/auto.ap
		
		cp ./epc_add_automation.sql.template ./epc_add_automation.sql
		sed -i d ./epc_add_automation.sql
		echo "UPDATE equipment.automation SET oculus_attr = jsonb_set(oculus_attr, '{host}', '\"X.X.X.X\"') WHERE tag = 'AUTOMATION_BP_YYYY';" >> ./epc_add_automation.sql
		echo "UPDATE equipment.automation SET oculus_attr = jsonb_set(oculus_attr, '{watch_dir}', '\"/mnt/ap/bapYYYY\"') WHERE tag = 'AUTOMATION_BP_YYYY';" >> ./epc_add_automation.sql
		sed -i 's/YYYY/'"$((i+1))"'/g' ./epc_add_automation.sql
		sed -i 's/X.X.X.X/'"$atms_ip"'/g' ./epc_add_automation.sql
		psql ******** -v "ON_ERROR_STOP=1" -f ./epc_add_automation.sql
		ret2="$?"
		
		if [[ "$ret2" != "0" ]]; then
	
			if [ -f ./epc_add_automation.sql ]; then
				rm -f ./epc_add_automation.sql
			fi
	
			cp ./epc_add_automation.sql.template ./epc_add_automation.sql
			sed -i 's/YYYY/'"$((i+1))"'/g' ./epc_add_automation.sql
			sed -i 's/X.X.X.X/'"$atms_ip"'/g' ./epc_add_automation.sql
			psql ******** -v "ON_ERROR_STOP=1" -f ./epc_add_automation.sql
			ret3="$?"
			
			if [[ "$ret3" != "0" ]]; then
				echo -e "\033[1;31mERROR! Unable to add new automation!\033[0m"
			fi
		fi
	done
fi


sudo chown root:root /etc/auto.ap ; sudo chmod 644 /etc/auto.ap
sudo chown root:root /etc/auto.atms ; sudo chmod 644 /etc/auto.atms
sudo systemctl start autofs.service
sudo systemctl enable autofs.service

if [ ! -L /opt/******** ]; then
	sudo ln -s /opt/******** /opt/********
	sudo chown -h eService:apache /opt/********
fi

rm -f ./epc_db_provision.sql
rm -f ./epc_add_automation.sql
echo -e "\033[1;33mREMEMBER TO INSTALL OPENVPN KEYS...\033[0m"
echo -e "\033[1;33mREMEMBER TO PLACE CalibImage.raw and EmptyImages...\033[0m"
echo -e "\033[1;33mREMEMBER TO GENERATE MASK FILES...\033[0m"
echo -e "\033[1;33mSYSTEM REBOOT IN 15 SECONDS...\033[0m"
( sleep 15 ; sudo reboot ) & #reboot
